<?php

namespace App\Http\Requests\Inventory\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'name' => 'required|max:20|unique:products,name|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'code' => 'required|max:20|unique:products,code|alpha_num',
            'description' => 'nullable|max:50|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'category_id' => 'required|exists:categories,id|regex:/^[0-9]+$/',
            'measurement' => [
                'required',
                Rule::in(['kg', 'ud', 'l']),
            ],
            'min_total_amount' => 'nullable|regex:/^[0-9]+$/',
            'total_quantity' => 'nullable|regex:/^[0-9]+$/',
            'sale_price' => 'nullable|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'purchase_price' => 'nullable|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];

    }
    public function messages()
    {
        return [
            'name.required' => 'El nombre es requerido',
            'name.max' => 'El número máximo de caracteres permitidos en el nombre son 20',
            'name.unique' => 'El nombre ya se encuentra registrado',
            'name.regex' => 'La combinación de caracteres del nombre no es permitida',
            'code.max' => 'El número máximo de caracteres permitidos en el codigo son 20',
            'code.required' => 'El código es requerido',
            'code.unique' => 'El código ya se encuentra registrado',
            'code.regex' => 'La combinación de caracteres del codigo no es permitida',
            'category_id.required' => 'La categoria es requerida',
            'category_id.integer' => 'El id de la categoria debe ser entera',
            'measurement.required' => 'La unidad de medida es requerida',
            'min_total_amount.integer' => 'El stock minimo debe ser entero',
            'total_quantity.integer' => 'El stock debe ser entero',
            'sale_price.regex' => 'El precio de venta debe ser numero real',
            'purchase_price.regex' => 'El precio de compra debe ser numero real',

        ];

    }
}

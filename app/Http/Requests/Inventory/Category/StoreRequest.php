<?php

namespace App\Http\Requests\Inventory\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [     
            'name' => 'required|unique:categories,name|max:20|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
        ];

    }
    public function messages()
    {
        return [
            'name.unique' => 'El nombre ya se encuentra registrado',
            'name.required' => 'El nombre es requerido',
            'name.max' => 'El número máximo de caracteres permitidos en el nombre es 20',
            'name.regex' => 'La combinación de caracteres en el nombre no es permitida',
        ];

    }
}

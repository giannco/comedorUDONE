<?php

namespace App\Http\Requests\Inventory\Provider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'type' => [
                'required',
                Rule::in(['j', 'g', 'v']),
            ],
            'rif' => 'required|unique:providers,rif|regex:/^[0-9]+$/',
            'name' => 'required|max:20|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'address' => 'nullable|max:50|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'phone' => 'nullable|regex:/^[0-9]+$/',
        ];

    }
    public function messages()
    {
        return [
            'type.required' => 'El tipo es requerido',
            'type.max' => 'El tipo solo puede tener un caracter(j, g ó v)',
            'rif.required' => 'El rif es requerido',
            'rif.unique' => 'El rif ya se encuentra registrado',
            'name.required' => 'El nombre es requerido',

        ];

    }
}

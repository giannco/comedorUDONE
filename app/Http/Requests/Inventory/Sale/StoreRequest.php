<?php

namespace App\Http\Requests\Inventory\Sale;

use App\Models\Inventory\Product;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'campus_id' => 'required|exists:campus,id|regex:/^[0-9]+$/',
            'products' => 'required|array',
            'products.*.id' => 'required|exists:products,id|regex:/^[0-9]+$/',
            'products.*.sale_price' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'sale_date' => 'required',
        ];

        foreach ($this->products as $index => $product) {

            $stock = Product::find($product['id'])['total_quantity'];
            $rules["products.{$index}.total_quantity"] = "required|integer|between:1,{$stock}";

        }

        return $rules;
    }

    public function messages()
    {
        return [
            'products.required' => 'Los productos son requerido',
            'campus_id.required' => 'El campus es requerido',
            'campus_id.exists' => 'El campus debe existir',

            'products.*.total_quantity.between' => 'La cantidad no puede ser mayor al stock',

            'sale_date.required' => 'La fecha de venta es requerida',
        ];
    }
}

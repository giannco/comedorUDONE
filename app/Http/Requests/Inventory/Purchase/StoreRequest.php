<?php

namespace App\Http\Requests\Inventory\Purchase;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'provider_id' => 'required|exists:providers,id|regex:/^[0-9]+$/',
            'products' => 'required|array',
            'products.*.id' => 'required|exists:products,id|regex:/^[0-9]+$/',
            'products.*.total_quantity' => 'required|integer|min:1',
            'products.*.purchase_price' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'purchase_date' => 'required',

        ];
    }
    public function messages()
    {
        return [
            'products.required' => 'Los productos son requerido',

            'provider_id.required' => 'El proveedor es requerido',
            'provider_id.exists' => 'El proveedor debe existir',

            'purchase_date.required' => 'La fecha de compra es requerida',
        ];

    }
}

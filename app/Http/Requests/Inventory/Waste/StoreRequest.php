<?php

namespace App\Http\Requests\Inventory\Waste;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products' => 'required|array',
            'products.*.id' => 'required|exists:products,id|regex:/^[0-9]+$/',
            'products.*.total_quantity' => 'required|min:1|regex:/^[0-9]+$/',
            'waste_date' => 'required',
            'reason' => [
                'required',
                Rule::in(['dañado', 'merma', 'vencido']),
            ],
        ];

    }

    public function messages()
    {
        return [
            //
        ];

    }
}

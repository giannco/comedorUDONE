<?php

namespace App\Http\Requests\Comensal;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'ic' => 'required|unique:comensales,ic|max:15|regex:/^[0-9]+$/',
            'type_id' => 'required|exists:comensal_types,id|regex:/^[0-9]+$/',
            'first_name' => 'required|min:2|max:150|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'last_name' => 'required|min:2|max:150|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'sex' => [
                'required',
                Rule::in(['f', 'm']),
            ],
            'career_id' => 'nullable|exists:careers,id|regex:/^[0-9]+$/',
        ];
    }
    public function messages()
    {
        return [
            'ic.required' => 'La cedula es requerida',
            'ic.unique' => 'La cedula ya se encuentra registrada',
            'first_name.required' => 'El nombre es requerido',
            'last_name.required' => 'El apellido es requerido',
            'sex.required' => 'El sexo es requerido',
            'type_id.required' => 'El tipo de comensal es requerido',
            'type_id.exists' => 'El tipo de comensal debe existir',

        ];
    }
}

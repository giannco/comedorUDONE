<?php

namespace App\Http\Requests\Comensal;

use Illuminate\Foundation\Http\FormRequest;

class ImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|file|mimes:xlsx,xls,csv',
            'type_id' => 'required|exists:comensal_types,id',
        ];
    }

    public function messages()
    {

        return [
            'file.required' => 'El archivo es requerido',
            'type_id.required' => 'El tipo de usuario es obligatorio',
            'type_id.exists' => 'El tipo de comensal debe existir',

        ];
    }
}

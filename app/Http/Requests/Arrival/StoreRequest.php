<?php

namespace App\Http\Requests\Arrival;

use App\Models\ComensalType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'comensal_ic' => 'required|exists:comensales,ic|max:15',
            'comensal_ic' => [
                'required',
                'regex:/^[0-9]+$/',
                Rule::exists('comensales', 'ic')->where(function ($query) {

                    //Selecciona los tipos de comensal activos.
                    $typeActives = ComensalType::where('deleted_at', null)->pluck('id');

                    Log::info($typeActives);

                    //chequea que el comensal y el tipo de comensal esten activos.
                    $query->where('deleted_at', null)->whereIn('type_id', $typeActives);
                }),
            ],
            'turn_id' => 'required|exists:turns,id|regex:/^[0-9]+$/',
        ];
    }

    public function messages()
    {
        return [
            'comensal_ic.required' => 'La cédula es requerida',
            'comensal_ic.exists' => 'La cédula no se encuentra registrada',
            'turn_id.required' => 'El turno es requerido',
            'turn_id.exists' => 'El turno no se encuentra registrada',
        ];
    }

}

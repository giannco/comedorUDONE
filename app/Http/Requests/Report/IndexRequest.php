<?php

namespace App\Http\Requests\Report;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|date',
            'end_date' => 'required|after_or_equal:start_date|date',
            'type_report' => 'required|numeric',
            //'turn_id' => 'required|exists:turns,id',
        ];
    }

    public function messages()
    {
        return [
            'start_date.required' => 'La fecha de inicio es requerida',
            'start_date.date' => 'Formato de fecha incorrecta',
            'end_date.required' => 'La fecha fin es requerido',
            'end_date.date' => 'Formato de fecha incorrecta',
            'end_date.after_or_equal' => 'La fecha fin, debe ser igual o mayor a la fecha de inicio',
            //'type.required' => 'El tipo de comensal es requerida',
            'turn_id.required' => 'El turno es requerido',
            'turn_id.exists' => 'El turno debe existir',
        ];
    }

}

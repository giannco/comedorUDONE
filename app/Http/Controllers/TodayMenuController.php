<?php

namespace App\Http\Controllers;

use App\Models\TodayMenu;
use Illuminate\Http\Request;

class TodayMenuController extends Controller
{
    public function index()
    {

        $today_menu = TodayMenu::get()->map(function ($menu) {
            return [
                'name' => $menu->turn->name,
                'description' => $menu->description,
            ];
        });

        return response()->json($today_menu);

    }
    public function create()
    {
        return view('todaymenu.create');
    }
    public function store(Request $request)
    {
        if ($request->expectsJson()) {
            TodayMenu::create($request->all());
            return response()->json(['success' => 'success'], 201);

        }
    }
}

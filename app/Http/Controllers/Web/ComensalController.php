<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Comensal\ImportRequest;
use App\Http\Requests\Comensal\StoreRequest;
use App\Models\Career;
use App\Models\Comensal;
use App\Models\ComensalType;
use App\Models\Student;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ComensalController extends Controller
{
    public function index(Request $request)
    {
        if ($request->expectsJson()) {

            $comensals = Comensal::with('type')->findComensal($request->search)->withTrashed()->orderBy('deleted_at')->paginate(10);

            //Log::info($request->search);

            $comensals = [
                'data' => $comensals->map(function ($comensal) {

                    return [
                        'ic' => $comensal->ic,
                        'type' => $comensal->type->name,
                        'type_id' => $comensal->type->id,
                        'first_name' => $comensal->first_name,
                        'last_name' => $comensal->last_name,
                        'sex' => $comensal->sex,
                        'photo' => $comensal->photo,
                        'career_id' => $comensal->student->career->id ?? 'N/A',
                        'created_at' => $comensal->created_at,
                        'deleted_at' => $comensal->deleted_at,
                    ];
                }),
                'meta' => [
                    'current_page' => $comensals->currentPage(),
                    'from' => $comensals->firstItem(),
                    'last_page' => $comensals->lastPage(),
                    'next_page_url' => $comensals->nextPageUrl(),
                    'path' => $request->url(),
                    'per_page' => $comensals->perPage(),
                    'prev_page_url' => $comensals->previousPageUrl(),
                    'to' => $comensals->lastItem(),
                    'total' => $comensals->total(),
                ],
            ];

            return response()->json($comensals);
        }

        return view('errors.404');
    }

    public function import(ImportRequest $request)
    {
        if ($request->isMethod('post')) {

            DB::beginTransaction();

            try {

                $listNewComensales = [];
                $listNewCareers = [];
                $listNewStudents = [];
                $listToggleActiveComensales = [];

                //expresiones regulares
                $regex_integer = "/^[0-9]+$/";
                $regex_string = "/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/";
                $regex_sex = "/^(?:m|M|masculino|Masculino|f|F|femenino|Femenino)$/";

                //temporal path del archivo excel
                $tmp_path = $request->file('file')->getRealPath();
                $results = Excel::load($tmp_path)->get();

                //trozo
                $chunks = $results->chunk(250);
                $chunks->toArray();

                //cabezera de excel
                $headerRow = $results->first()->keys()->toArray();

                //Verifica las cabezeras del excel
                $boolArray = (!in_array('cedula', $headerRow) || !in_array('nombres', $headerRow) || !in_array('apellidos', $headerRow) || !in_array('especialidad', $headerRow) || !in_array('sexo', $headerRow));

                //Valida que sean maximo 5 columnas y que esten las cabezeras
                if (count($headerRow) != 5 || $boolArray) {
                    return response()->json(['errors' => ['file' => ['Formato de archivo excel inválido']]], 422);
                }

                foreach ($chunks as $rows) {
                    foreach ($rows as $row) {

                        //reglas para las columnas del excel
                        $ruleIc = preg_match($regex_integer, $row->cedula);
                        $ruleFirstName = preg_match($regex_string, $row->nombres);
                        $ruleLastName = preg_match($regex_string, $row->apellidos);
                        $ruleCareer = preg_match($regex_string, $row->especialidad);
                        $ruleSex = preg_match($regex_string, $row->sexo);

                        if ($ruleIc && $ruleFirstName && $ruleLastName && $ruleCareer && $ruleSex) {

                            $existsCedula = Comensal::withTrashed()->where('ic', $row->cedula)->exists();

                            array_push($listToggleActiveComensales, $row->cedula);

                            if (!$existsCedula) {

                                array_push($listNewComensales, [
                                    'ic' => intval($row->cedula),
                                    'first_name' => $row->nombres,
                                    'last_name' => $row->apellidos,
                                    'type_id' => $request->type_id,
                                    'sex' => strtolower($row->sexo[0]),
                                ]);
                            }

                            $existsStudent = Student::where('comensal_ic', $row->cedula)->exists();
                            $career = Career::where('name', $row->especialidad)->first();

                            if (!$existsStudent) {
                                array_push($listNewStudents, [
                                    'comensal_ic' => $row->cedula,
                                    'career_id' => $career->id,
                                ]);
                            } else {
                                Student::where('comensal_ic', $row->cedula)->update([
                                    'career_id' => $career->id,
                                ]);

                            }
                        }
                    }
                }

                //inserta los nuevos comensales
                if (!empty($listNewComensales)) {
                    Comensal::insert($listNewComensales);
                }

                //inserta los nuevos carreras
                if (!empty($listNewCareers)) {
                    Career::insert($listNewCareers);
                }

                //inserta los nuevos estudiantes
                if (!empty($listNewStudents)) {
                    Student::insert($listNewStudents);
                }

                //Desactiva los comensales que no se encuentra en el excel
                if (!empty($listToggleActiveComensales)) {
                    Comensal::whereNotIn('ic', $listToggleActiveComensales)->delete();
                }

                //Se guarda la transacción
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();

                // something went wrong
                //return response()->json(['error' => $e], 500);
                return response()->json(['errors' => ['error' => ['error en la carga del archivo']]], 500);
            }

        }

        return redirect()->route('configuration');
    }

    public function create()
    {
        if (Auth::user()->hasRole('admin')) {
            return view('comensal.create');
        }

        return redirect()->route('home');

    }

    public function store(StoreRequest $request)
    {

        DB::beginTransaction();

        try {

            //Comensal::create($request->all());
            $type_name = ComensalType::where('id', $request->type_id)->first()->name;

            Comensal::create([
                'ic' => $request->ic,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'type_id' => $request->type_id,
                'sex' => $request->sex,
            ]);

            if ($type_name == 'estudiante') {

                Student::create([
                    'comensal_ic' => $request->ic,
                    'career_id' => $request->career_id,
                ]);
            }

            //Se guarda la transacción
            DB::commit();

            return response()->json(['success' => 'success'], 201);

        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            //return response()->json(['error' => $e], 500);
            return response()->json(['errors' => ['error' => [$e]]], 500);
        }

    }

    public function show($id)
    {
        return response()->json(Comensal::where('id', $id)->first());
    }

    public function edit($id)
    {
        Comensal::findOrFail($id);
        return view('comensal.edit')->with(compact('id'));
    }

    public function update(Request $request, $ic)
    {

        DB::beginTransaction();

        try {

            $type_name = ComensalType::where('id', $request->type_id)->first()->name;

            Comensal::where('ic', $ic)->update([
                'ic' => $request->ic,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'type_id' => $request->type_id,
                'sex' => $request->sex,

            ]);

            if ($type_name == 'estudiante') {

                $existsStudent = Student::where('comensal_ic', $request->ic)->exists();

                if (!$existsStudent) {

                    Student::create([
                        'comensal_ic' => $request->ic,
                        'career_id' => $request->career_id,
                    ]);

                } else {

                    Student::where('comensal_ic', $request->ic)->update([
                        'career_id' => $request->career_id,
                    ]);
                }
            }

            //Se guarda la transacción
            DB::commit();

            return response()->json(['success' => 'success'], 201);

        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            //return response()->json(['error' => $e], 500);
            return response()->json(['errors' => ['error' => [$e]]], 500);
        }
    }

    public function activate($id)
    {

        Comensal::withTrashed()->where('ic', $id)->restore();

        return response()->json(['success' => 'success'], 201);
    }

    public function destroy($ic)
    {

        Comensal::where('ic', $ic)->delete();

        return response()->json(['success' => 'success'], 201);
    }
}

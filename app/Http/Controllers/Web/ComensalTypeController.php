<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ComensalType;
use Illuminate\Http\Request;

class ComensalTypeController extends Controller
{
    public function index(Request $request)
    {
        $type_comensal[] = [];

        if ($request->expectsJson()) {
            $type_comensal['data'] = ComensalType::withTrashed()->get()->map(function ($type) {
                return [
                    'id' => $type->id,
                    'name' => $type->name,
                    'description' => $type->description,
                    'deleted_at' => $type->deleted_at,
                ];
            });

            return response()->json($type_comensal);
        }

        return view('errors.404');
    }
    public function activate($id)
    {

        ComensalType::withTrashed()->where('id', $id)->restore();

        return response()->json(['success' => 'success'], 201);
    }

    public function destroy($id)
    {

        ComensalType::findOrFail($id)->delete();

        return response()->json(['success' => 'success'], 201);
    }
}

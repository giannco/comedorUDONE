<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Report\IndexRequest;
use App\Models\Arrival;
use App\Models\Career;
use App\Models\Inventory\Product;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;

//
class ReportController extends Controller
{
    public function index()
    {
        return view('reports.index');
    }

    public function pdf(IndexRequest $request)
    {

        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $turn_id = $request->turn_id;
        $sex = $request->sex;
        $typeReport = $request->type_report;
        $numReport = 0;

        $option = $request->option;

        $dateNow = Carbon::now()->format('d-m-Y');

        $statistics = function ($arrivals = [], $index) {
            return $arrivals['data']->unique($index)->map(function ($data) use ($arrivals, $index) {

                return [
                    'name' => $data[$index],
                    'total_f' => $arrivals['data']->where($index, $data[$index])->where('sex', 'f')->count(),
                    'total_m' => $arrivals['data']->where($index, $data[$index])->where('sex', 'm')->count(),
                    'total' => $arrivals['data']->where($index, $data[$index])->count(),

                ];
            });
        };

        if ($typeReport == '0' || $typeReport == '1' || $typeReport == '2' || $typeReport == '3') {

            $arrivals['date_now'] = $dateNow;

            $arrivals = Arrival::with('comensal', 'turn')
                ->whereDate('created_at', '>=', $startDate)
                ->whereDate('created_at', '<=', $endDate)
                ->orderBy('created_at', 'asc')->get();

            $arrivals = [
                'data' => $arrivals->map(function ($arrival) {
                    return [
                        'ic' => $arrival->comensal_ic,
                        'sex' => $arrival->comensal->sex,
                        'career' => $arrival->comensal->type->name == 'estudiante' ? $arrival->comensal->student->career->name : null,
                        'type' => $arrival->comensal->type->name,
                        'first_name' => $arrival->comensal->first_name,
                        'last_name' => $arrival->comensal->last_name,
                        'turn_id' => $arrival->turn->id,
                        'turn_name' => $arrival->turn->name,
                        'created_at' => $arrival->created_at->format('d-m-Y H:i:s'),
                    ];
                }),
            ];

            $arrivals['meta'] = [
                'careers' => $statistics($arrivals, 'career'),
                'types' => $statistics($arrivals, 'type'),
                'turns' => $statistics($arrivals, 'turn_name'),
                'general' => [
                    'total_f' => $arrivals['data']->where('sex', 'f')->count(),
                    'total_m' => $arrivals['data']->where('sex', 'm')->count(),
                    'total' => $arrivals['data']->count(),
                ],

            ];

            if ($option == '2') {

                return response()->json($arrivals);
            }

            if ($typeReport == '0') {

                return PDF::loadView('reports.files.general-pdf', [
                    'arrivals' => $arrivals,
                    'dateNow' => $dateNow,
                ])->download('reporte_general_' . $dateNow . '.pdf');

            }

            if ($typeReport == '1') {

                return PDF::loadView('reports.files.career-pdf', [
                    'arrivals' => $arrivals,
                    'dateNow' => $dateNow,
                ])->download('reporte_career_' . $dateNow . '.pdf');

            }

            if ($typeReport == '3') {

                return PDF::loadView('reports.files.turn-pdf', [
                    'arrivals' => $arrivals,
                    'dateNow' => $dateNow,
                ])->download('reporte_turn_' . $dateNow . '.pdf');

            }

            //tipo de reporte 3
            return PDF::loadView('reports.files.comensal-pdf', [
                'arrivals' => $arrivals,
                'dateNow' => $dateNow,
            ])->download('reporte_de_comensales_' . $dateNow . '.pdf');

        }

        if ($typeReport == '4' || $typeReport == '5' || $typeReport == '6' || $typeReport == '7') {

            if ($typeReport == '4') {

                $numReport = 4;

                $products = Product::with('category')->where('total_quantity', '>', 0)->get();

                $products = [
                    'data' => $products->map(function ($product) {
                        return [
                            'name' => $product->name,
                            'category_name' => $product->category->name,
                            'total_quantity' => $product->total_quantity,
                        ];
                    }),
                ];

            }
            if ($typeReport == '5') {

                $numReport = 5;

                $products = Product::whereHas('purchases', function ($query) {
                    return $query;
                })->where('total_quantity', '>', 0)->get();

                $products = [
                    'data' => $products->map(function ($product) use ($startDate, $endDate) {
                        return [
                            'name' => $product->name,
                            'total_quantity_sum' => $product->wastes->where('purchase_date', '>=', $startDate)->where('purchase_date', '<=', $endDate)->where('status', 'aprobada')->sum('pivot.total_quantity'),
                        ];
                    }),
                ];

            }

            if ($typeReport == '6') {
                $numReport = 6;

                $products = Product::whereHas('sales', function ($query) {
                    return $query;
                })->where('total_quantity', '>', 0)->get();

                $products = [
                    'data' => $products->map(function ($product) use ($startDate, $endDate) {
                        return [
                            'name' => $product->name,
                            'total_quantity_sum' => $product->wastes->where('sale_date', '>=', $startDate)->where('sale_date', '<=', $endDate)->where('status', 'aprobada')->sum('pivot.total_quantity'),
                        ];
                    }),
                ];

            }

            if ($typeReport == '7') {

                $numReport = 7;

                $products = Product::whereHas('wastes', function ($query) {
                    return $query;
                })->get();

                $products = [
                    'data' => $products->map(function ($product) use ($startDate, $endDate) {
                        return [
                            'name' => $product->name,
                            'total_quantity_sum' => $product->wastes->where('waste_date', '>=', $startDate)->where('waste_date', '<=', $endDate)->where('status', 'aprobada')->sum('pivot.total_quantity'),

                        ];
                    }),
                ];

            }

            $products['date_now'] = $dateNow;
            if ($option == '2') {

                return response()->json($products);
            }

            return PDF::loadView('reports.files.products-pdf', [
                'products' => $products,
                'num' => $numReport,
                'dateNow' => $dateNow,
            ])->download('reporte_productos_' . $dateNow . '.pdf');

        }

    }

}

<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Turn\StoreRequest;
use App\Http\Requests\Turn\UpdateRequest;
use App\Models\Turn;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TurnController extends Controller
{
    public function index(Request $request)
    {
        if ($request->expectsJson()) {

            $time_now = Carbon::now()->toTimeString();

            $turns = Turn::findTurn($request->search)
                ->orderBy('created_at', 'DESC')
                ->when($request,
                    function ($query, $request) use ($time_now) {
                        return $request->paginate ? $query->withTrashed()->paginate(10) : $query->where('start_hour', '<=', $time_now)->where('end_hour', '>=', $time_now)->first();
                    });

            if ($request->paginate) {

                $turns = [
                    'data' => $turns->map(function ($turn) {
                        return [
                            'id' => $turn->id,
                            'name' => $turn->name,
                            'description' => $turn->description,
                            'start_hour' => $turn->start_hour,
                            'end_hour' => $turn->end_hour,
                            'deleted_at' => $turn->deleted_at,
                        ];
                    }),
                    'meta' => $request->paginate ? [
                        'current_page' => $turns->currentPage(),
                        'from' => $turns->firstItem(),
                        'last_page' => $turns->lastPage(),
                        'next_page_url' => $turns->nextPageUrl(),
                        'path' => $request->url(),
                        'per_page' => $turns->perPage(),
                        'prev_page_url' => $turns->previousPageUrl(),
                        'to' => $turns->lastItem(),
                        'total' => $turns->total(),
                    ] : [],
                ];

            }

            return response()->json($turns);
        }

        return view('errors.404');

    }

    public function create()
    {
        return view('turn.create');
    }

    public function store(StoreRequest $request)
    {

        Turn::create($request->all());
        return response()->json(['success' => 'success'], 201);
    }

    public function show($id)
    {
        return response()->json(Turn::where('id', $id)->first());
    }

    public function edit($id)
    {
        Turn::findOrFail($id);
        return view('turn.edit')->with(compact('id'));
    }

    public function update(UpdateRequest $request, $id)
    {
        Turn::findOrFail($id)->update($request->all());

        return response()->json(['success' => 'success'], 201);
    }

    public function activate($id)
    {

        Turn::withTrashed()->where('id', $id)->restore();

        return response()->json(['success' => 'success'], 201);
    }

    public function destroy($id)
    {

        Turn::findOrFail($id)->delete();

        return response()->json(['success' => 'success'], 201);
    }
}

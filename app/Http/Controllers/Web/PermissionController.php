<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
//use App\Models\Permission;
use App\Http\Requests\Permission\UpdateRequest;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index(Request $request)
    {

        if ($request->expectsJson()) {

            $permissions = Permission::where('description', "LIKE", "%$request->search%")
                ->orderBy('description', 'ASC')
                ->when($request,
                    function ($query, $request) {
                        return $request->paginate ? $query->paginate(10) : $query->get();
                    });

            $permissions = [
                'data' => $permissions->map(function ($permission) {
                    return [
                        'id' => $permission->id,
                        'description' => $permission->description,
                        'roles' => $permission->roles->where('name', '!=', 'admin')->pluck('name'),

                    ];
                }),
                'meta' => $request->paginate ? [
                    'current_page' => $permissions->currentPage(),
                    'from' => $permissions->firstItem(),
                    'last_page' => $permissions->lastPage(),
                    'next_page_url' => $permissions->nextPageUrl(),
                    'path' => $request->url(),
                    'per_page' => $permissions->perPage(),
                    'prev_page_url' => $permissions->previousPageUrl(),
                    'to' => $permissions->lastItem(),
                    'total' => $permissions->total(),
                ] : [],
            ];

            return response()->json($permissions);
        }

        return view('errors.404');
    }

    public function update(UpdateRequest $request)
    {

        $role = Role::where('name', $request->role)->first();

        $request->checked ? $role->givePermissionTo(Permission::find($request->id)->id) : $role->revokePermissionTo(Permission::find($request->id)->id);

    }
}

<?php

namespace App\Http\Controllers\Web\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Inventory\Product;
use App\Http\Requests\Inventory\Product\StoreRequest;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->expectsJson()) {

            $products = Product::with('category')
                ->findProduct($request->search)
                ->withTrashed()
                ->orderBy('created_at', 'DESC')
                ->when($request,
                function ($query, $request) {
                    return $request->sale ? $query->where('total_quantity','>',0) : $query;
                })
                ->when($request,
                    function ($query, $request) {
                        return $request->paginate ? $query->paginate(10) : $query->get();
                    });
                    

            $products = [
                'data' => $products->map(function ($product) {
                    return [
                        'id' => $product->id,
                        'name' => $product->name,
                        'category_id' => $product->category_id,
                        'category_name' => $product->category->name,
                        'code' => $product->code,
                        'min_total_amount' => $product->min_total_amount,
                        'measurement' => $product->measurement,
                        'sale_price' => $product->sale_price,
                        'purchase_price' => $product->purchase_price,
                        'stock' => $product->total_quantity,
                        'deleted_at' => $product->deleted_at,
                    ];
                }),
                'meta' => $request->paginate ? [
                    'current_page' => $products->currentPage(),
                    'from' => $products->firstItem(),
                    'last_page' => $products->lastPage(),
                    'next_page_url' => $products->nextPageUrl(),
                    'path' => $request->url(),
                    'per_page' => $products->perPage(),
                    'prev_page_url' => $products->previousPageUrl(),
                    'to' => $products->lastItem(),
                    'total' => $products->total(),
                ] : [],
            ];

            return response()->json($products);
        }

        return view('errors.404');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Product::create($request->all());
        return response()->json(['success' => 'success'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Product::findOrFail($id)->update($request->all());

        return response()->json(['success' => 'success'], 201);
    }

    public function activate($id)
    {
        Product::withTrashed()->where('id', $id)->restore();

        return response()->json(['success' => 'success'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id', $id)->delete();

        return response()->json(['success' => 'success'], 201);
    }
}

<?php

namespace App\Http\Controllers\Web\Inventory;

use App\Http\Controllers\Controller;
use App\Http\Requests\Inventory\Waste\StoreRequest;
use App\Models\Inventory\Waste;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WasteController extends Controller
{
    public function index(Request $request)
    {

        if ($request->expectsJson()) {

            $wastes = Waste::findWaste($request->search)
                ->orderBy('created_at', 'DESC')
                ->paginate(10);

            $wastes = [
                'data' => $wastes->map(function ($waste) {
                    return [
                        'id' => $waste->id,
                        'reason' => $waste->reason,
                        'description' => $waste->description,
                        'status' => $waste->status,
                        'active' => $waste->deleted_at ? true : false,
                        'products' => $waste->products->map(function ($product) {
                            return [
                                'name' => $product->name,
                                'measurement' => $product->measurement,
                                'total_quantity' => $product->pivot->total_quantity,
                            ];
                        }),
                    ];
                }),
                'meta' => [
                    'current_page' => $wastes->currentPage(),
                    'from' => $wastes->firstItem(),
                    'last_page' => $wastes->lastPage(),
                    'next_page_url' => $wastes->nextPageUrl(),
                    'path' => $request->url(),
                    'per_page' => $wastes->perPage(),
                    'prev_page_url' => $wastes->previousPageUrl(),
                    'to' => $wastes->lastItem(),
                    'total' => $wastes->total(),
                ],
            ];

            return response()->json($wastes);
        }

        return view('errors.404');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

        //Comienza la transaccion
        DB::beginTransaction();

        try {

            $products = $request->products;

            $array = [];

            foreach ($products as $product) {
                $array = $array + [
                    $product['id'] =>
                    [
                        'total_quantity' => $product['total_quantity'],
                    ],
                ];
            }

            $waste = Waste::create([
                'reason' => $request->reason,
                'waste_date' => Carbon::parse($request->waste_date),
            ]);

            $waste->products()->attach($array);

            //Se guarda la transacción
            DB::commit();

            return response()->json(['success' => 'success'], 201);

        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Comienza la transaccion
        DB::beginTransaction();

        try {

            $waste = Waste::findOrFail($id);
            $waste->update(['status' => $request->status]);

            //Se guarda la transacción
            DB::commit();

            return response()->json(['success' => 'success'], 201);

        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            return response()->json(['error' => $e], 500);
        }
    }
}

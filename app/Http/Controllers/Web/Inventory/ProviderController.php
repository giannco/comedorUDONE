<?php

namespace App\Http\Controllers\Web\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Inventory\Provider;
use Illuminate\Http\Request;
use App\Http\Requests\Inventory\Provider\StoreRequest;


class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->expectsJson()) {

            $providers = Provider::findProvider($request->search)
                ->withTrashed()
                ->orderBy('created_at', 'DESC')
                ->paginate(10);

            $providers = [
                'data' => $providers->map(function ($provider) {
                    return [
                        'id' => $provider->id,
                        'type' => $provider->type,
                        'rif' => $provider->rif,
                        'name' => $provider->name,
                        'address' => $provider->address,
                        'phone' => $provider->phone,
                        'deleted_at' => $provider->deleted_at,
                    ];
                }),
                'meta' => [
                    'current_page' => $providers->currentPage(),
                    'from' => $providers->firstItem(),
                    'last_page' => $providers->lastPage(),
                    'next_page_url' => $providers->nextPageUrl(),
                    'path' => $request->url(),
                    'per_page' => $providers->perPage(),
                    'prev_page_url' => $providers->previousPageUrl(),
                    'to' => $providers->lastItem(),
                    'total' => $providers->total(),
                ],
            ];

            return response()->json($providers);
        }

        return view('errors.404');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

        Provider::create($request->all());
        return response()->json(['success' => 'success'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Provider::findOrFail($id)->update($request->all());

        return response()->json(['success' => 'success'], 201);

    }
    
    public function activate($id)
    {

        Provider::withTrashed()->where('id', $id)->restore();

        return response()->json(['success' => 'success'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Provider::where('id', $id)->delete();

        return response()->json(['success' => 'success'], 201);
    }
}

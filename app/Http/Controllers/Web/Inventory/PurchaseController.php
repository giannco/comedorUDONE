<?php

namespace App\Http\Controllers\Web\Inventory;

use App\Http\Controllers\Controller;
use App\Http\Requests\Inventory\Purchase\StoreRequest;
use App\Models\Inventory\Purchase;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->expectsJson()) {

            $purchases = Purchase::with('provider')->findPurchase($request->search)->orderBy('created_at', 'DESC')->paginate(10);

            $purchases = [
                'data' => $purchases->map(function ($purchase) {
                    return [
                        'id' => $purchase->id,
                        'purchase_date' => $purchase->purchase_date,
                        'provider_code' => $purchase->provider->rif,
                        'provider_name' => $purchase->provider->name,
                        'status' => $purchase->status,
                        'total_cost' => $purchase->total_cost,
                        'description' => $purchase->description,
                        'products' => $purchase->products->map(function ($product) {
                            return [
                                'name' => $product->name,
                                'measurement' => $product->measurement,
                                'purchase_price' => $product->pivot->purchase_price,
                                'total_quantity' => $product->pivot->total_quantity,
                            ];
                        }),
                    ];
                }),
                'meta' => [
                    'current_page' => $purchases->currentPage(),
                    'from' => $purchases->firstItem(),
                    'last_page' => $purchases->lastPage(),
                    'next_page_url' => $purchases->nextPageUrl(),
                    'path' => $request->url(),
                    'per_page' => $purchases->perPage(),
                    'prev_page_url' => $purchases->previousPageUrl(),
                    'to' => $purchases->lastItem(),
                    'total' => $purchases->total(),
                ],
            ];

            return response()->json($purchases);
        }

        return view('errors.404');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

        //Comienza la transaccion
        DB::beginTransaction();

        try {

            $products = $request->products;

            $array = [];

            foreach ($products as $product) {
                $array = $array + [
                    $product['id'] =>
                    [
                        'total_quantity' => $product['total_quantity'],
                        'purchase_price' => $product['purchase_price'],
                    ],
                ];
            }

            $purchase = Purchase::create([
                'provider_id' => $request->provider_id,
                'purchase_date' => Carbon::parse($request->purchase_date),
                'total_cost' => 0,
            ]);

            $purchase->products()->attach($array);

            //Se guarda la transacción
            DB::commit();

            return response()->json(['success' => 'success'], 201);

        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            return response()->json(['error' => $e], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Comienza la transaccion
        DB::beginTransaction();

        try {

            $purchase = Purchase::findOrFail($id);
            $purchase->update(['status' => $request->status]);

            if ($request->status == 'aprobada') {
                $purchase->products()->get()->each(function ($product) {
                    $product->update([
                        'purchase_price' => $product->purchase_price + $product->pivot->purchase_price,
                        'total_quantity' => $product->total_quantity + $product->pivot->total_quantity,
                    ]);
                });
            }

            //Se guarda la transacción
            DB::commit();

            return response()->json(['success' => 'success'], 201);

        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

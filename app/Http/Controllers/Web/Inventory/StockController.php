<?php

namespace App\Http\Controllers\Web\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Inventory\Purchase;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->expectsJson()) {
            //Se agrupa por productos y se obtiene la primera compra de cada producto donde las unidades son mayores a 0
            $stock['data'] = Purchase::with('product')->get()->groupBy('product_id')->map(function ($purchase) {
                return $purchase->map(function ($purchase) {
                    return [
                        'id' => $purchase->id,
                        'created_at' => $purchase->created_at->toW3cString(),
                        'product_id' => $purchase->product->id,
                        'product_code' => $purchase->product->code,
                        'product_name' => $purchase->product->name,
                        'units' => $purchase->units,
                        'sold_units' => $purchase->sold_units,
                        'price_unit' => $purchase->price_unit,
                        'total_cost' => $purchase->total_cost,
                        'description' => $purchase->description,
                        'measurement' => $purchase->measurement,
                    ];
                })->where('units', '>', '0')->first();
            });

            return response()->json($stock);
        }

        return view('errors.404');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Web\Inventory;

use App\Http\Controllers\Controller;
use App\Models\Inventory\Category;
use App\Http\Requests\Inventory\Category\StoreRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->expectsJson()) {

            $categories = Category::findCategory($request->search)
                ->orderBy('created_at', 'DESC')
                ->withTrashed()
                ->when($request,
                    function ($query, $request) {
                        return $request->paginate ? $query->paginate(10) : $query->get();
                    });

            $categories = [
                'data' => $categories->map(function ($category) {
                    return [
                        'id' => $category->id,
                        'name' => $category->name,
                        'description' => $category->description,
                        'deleted_at' => $category->deleted_at,
                    ];
                }),
                'meta' => $request->paginate ? [
                    'current_page' => $categories->currentPage(),
                    'from' => $categories->firstItem(),
                    'last_page' => $categories->lastPage(),
                    'next_page_url' => $categories->nextPageUrl(),
                    'path' => $request->url(),
                    'per_page' => $categories->perPage(),
                    'prev_page_url' => $categories->previousPageUrl(),
                    'to' => $categories->lastItem(),
                    'total' => $categories->total(),
                ] : [],
            ];

            return response()->json($categories);
        }

        return view('errors.404');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Category::create($request->all());
        return response()->json(['success' => 'success'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
        Category::findOrFail($id)->update($request->all());

        return response()->json(['success' => 'success'], 201);
    }

    public function activate($id)
    {
        Category::withTrashed()->where('id', $id)->restore();

        return response()->json(['success' => 'success'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::where('id', $id)->delete();

        return response()->json(['success' => 'success'], 201);
    }
    
}

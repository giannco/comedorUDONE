<?php

namespace App\Http\Controllers\Web\Inventory;

use App\Http\Controllers\Controller;
use App\Http\Requests\Inventory\Sale\StoreRequest;
use App\Models\Inventory\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//use Illuminate\Support\Facades\Log;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {

            $sales = Sale::findSale($request->search)->orderBy('created_at', 'DESC')->paginate(10);

            $sales = [
                'data' => $sales->map(function ($sale) {
                    return [
                        'id' => $sale->id,
                        'campus_id' => $sale->campus->id,
                        'campus_name' => $sale->campus->name,
                        'status' => $sale->status,
                        'total_cost' => $sale->total_cost,
                        'description' => $sale->description,
                        'sale_date' => $sale->sale_date,
                        'products' => $sale->products->map(function ($product) {
                            return [
                                'name' => $product->name,
                                'measurement' => $product->measurement,
                                'sale_price' => $product->pivot->sale_price,
                                'total_quantity' => $product->pivot->total_quantity,
                            ];
                        }),
                    ];
                }),
                'meta' => [
                    'current_page' => $sales->currentPage(),
                    'from' => $sales->firstItem(),
                    'last_page' => $sales->lastPage(),
                    'next_page_url' => $sales->nextPageUrl(),
                    'path' => $request->url(),
                    'per_page' => $sales->perPage(),
                    'prev_page_url' => $sales->previousPageUrl(),
                    'to' => $sales->lastItem(),
                    'total' => $sales->total(),
                ],
            ];

            return response()->json($sales);

        }

        return view('errors.404');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

        //Comienza la transaccion
        DB::beginTransaction();

        try {

            $products = $request->products;

            $array = [];

            foreach ($products as $product) {
                $array = $array + [
                    $product['id'] =>
                    [
                        'total_quantity' => $product['total_quantity'],
                        'sale_price' => $product['sale_price'],
                    ],
                ];
            }

            $sale = Sale::create([
                'campus_id' => $request->campus_id,
                'sale_date' => Carbon::parse($request->sale_date),
                'total_cost' => 0,
            ]);

            $sale->products()->attach($array);

            //Se guarda la transacción
            DB::commit();

            return response()->json(['success' => 'success'], 201);

        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Comienza la transaccion
        DB::beginTransaction();

        try {

            $sale = Sale::findOrFail($id);

            $sale->update(['status' => $request->status]);

            if ($request->status == 'aprobada') {
                $sale->products()->get()->each(function ($product) use ($sale) {
                    if ($product->total_quantity >= $product->pivot->total_quantity) {
                        $product->update([
                            'purchase_price' => $product->purchase_price - $product->pivot->purchase_price,
                            'total_quantity' => $product->total_quantity - $product->pivot->total_quantity,
                        ]);
                    } else {
                        $sale->products()->detach($product->id);
                    }
                });
            }

            //Se guarda la transacción
            DB::commit();

            return response()->json(['success' => 'success'], 201);

        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            return response()->json(['error' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class ConfigurationController extends Controller
{
    public function show()
    {
        return view('configuration.index');
    }

}

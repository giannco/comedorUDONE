<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Career;

class CareerController extends Controller
{
    public function index() {
 
    	return response()->json(Career::get());
    }
}

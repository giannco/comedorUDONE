<?php

namespace App\Http\Controllers\Web;

use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {

       // return view('admin.users.index')->with(['userLogged' => Auth::user()->remember_token]);
    
    	$user = User::all();

        return response()->json($user);
    }
}

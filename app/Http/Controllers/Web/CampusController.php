<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Campus;

class CampusController extends Controller
{
    public function index(){
        return response()->json(Campus::get());
    }
    
}

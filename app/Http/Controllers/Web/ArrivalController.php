<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Arrival\StoreRequest;
use App\Models\Arrival;
use App\Models\Turn;
//use App\Models\Comensal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArrivalController extends Controller
{

    public function index(Request $request)
    {

        if ($request->expectsJson()) {
            $arrivals['data'] = Arrival::with('comensal', 'turn')->where('created_at', 'like', '%' . Carbon::now()->toDateString() . '%')->where('turn_id', $request->turn_id)->orderBy('created_at', 'desc')->get()->map(function ($arrival) {
                return [
                    'ic' => $arrival->comensal_ic,
                    'turn_name' => $arrival->turn->name,
                    'type' => $arrival->comensal->type->name,
                    'first_name' => $arrival->comensal->first_name,
                    'last_name' => $arrival->comensal->last_name,
                    'sex' => $arrival->comensal->sex,
                    'created_at' => $arrival->created_at->toW3cString(),
                ];
            });

            $arrivals['graph'] = $arrivals['data']->unique('type')->map(function ($map) use ($arrivals) {
                return [
                    'name' => $map['type'],
                    'total' => $arrivals['data']->where('type', $map['type'])->count(),
                ];
            });

            return response()->json($arrivals);
        }
        return view('errors.404');
    }

    public function store(StoreRequest $request)
    {
        //valida que la hora del servidor este en el rango del turno
        if (!Turn::existsTurnWithHour($request)->exists()) {
            return response()->json(['errors' => ['turn' => ['Turno no habilitado']]], 422);
        } else {
            //valida que el comensal no este aun registrado en el turno
            if (Arrival::alreadyExists($request)->exists()) {
                return response()->json(['errors' => ['comensal' => ['El comensal ya comio']]], 422);
            }
        }

        //crea la llegada del comensal
        Arrival::create($request->all());
        return response()->json(['success' => 'success'], 201);

    }
}

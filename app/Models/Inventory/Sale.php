<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'total_cost', 'description', 'campus_id', 'status', 'sale_date',
    ];

    protected $table = 'sales';

    public function campus()
    {
        return $this->belongsTo('App\Models\Campus', 'campus_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Inventory\Product', 'sale_details', 'sale_id', 'product_id')->withPivot('product_id', 'total_quantity', 'sale_price');
    }

    public function scopeFindSale($query, $search)
    {
        return $query->whereHas('campus', function ($q) use ($search) {
            $q->where('name', "LIKE", "%$search%");

        })->OrWhereRaw("DATE_FORMAT(sale_date, '%d/%m/%Y') LIKE '%{$search}%'")
            ->OrWhere('status', "LIKE", "%$search%");

    }

}

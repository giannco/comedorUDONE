<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'total_cost', 'description', 'provider_id', 'status', 'purchase_date',
    ];

    protected $table = 'purchases';

    public function provider()
    {
        return $this->belongsTo('App\Models\Inventory\Provider', 'provider_id', 'id')->withTrashed();
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Inventory\Product', 'purchase_details', 'purchase_id', 'product_id')->withPivot('product_id', 'total_quantity', 'purchase_price');

    }

    public function scopeFindPurchase($query, $search)
    {
        return $query->whereHas('provider', function ($q) use ($search) {
            $q->where('name', "LIKE", "%$search%");

        })->OrWhereRaw("DATE_FORMAT(purchase_date, '%d/%m/%Y') LIKE '%{$search}%'")
            ->OrWhere('status', "LIKE", "%$search%");

    }
}

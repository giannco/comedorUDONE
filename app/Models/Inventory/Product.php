<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'code', 'description', 'category_id', 'measurement', 'sale_price', 'purchase_price', 'min_total_amount', 'total_quantity',
    ];

    protected $table = 'products';

    public function category()
    {
        return $this->belongsTo('App\Models\Inventory\Category', 'category_id', 'id')->withTrashed();
    }

    public function purchases()
    {
        return $this->belongsToMany('App\Models\Inventory\Purchase', 'purchase_details', 'product_id', 'purchase_id')->withPivot('purchase_id', 'total_quantity', 'purchase_price');
    }

    public function sales()
    {
        return $this->belongsToMany('App\Models\Inventory\Sale', 'sale_details')->withPivot('sale_id', 'total_quantity', 'sale_price'); //ventas-productos
    }
    public function wastes()
    {
        return $this->belongsToMany('App\Models\Inventory\Waste', 'waste_details', 'product_id', 'waste_id')->withPivot('waste_id', 'total_quantity');
    }

    public function scopeFindProduct($query, $search)
    {

        return $query->whereHas('category', function ($q) use ($search) {
            $q->where('name', "LIKE", "%$search%");

        })->OrWhere('code', "LIKE", "%$search%")
            ->OrWhere('name', "LIKE", "%$search%");

    }
}

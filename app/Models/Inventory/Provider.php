<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'type', 'rif', 'name', 'address', 'phone',
    ];

    protected $table = 'providers';

    public function purchases()
    {
        return $this->hasMany('App\Models\Inventory\Purchase', 'provider_id', 'id')->withTrashed();
    }

    public function scopeFindProvider($query, $search)
    {

        return $query->where('rif', "LIKE", "%$search%")->OrWhere('name', "LIKE", "%$search%");

    }

}

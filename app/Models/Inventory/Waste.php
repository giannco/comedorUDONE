<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Waste extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'reason', 'description', 'product_id', 'status', 'waste_date',
    ];

    protected $table = 'waste';

    public function products()
    {
        return $this->belongsToMany('App\Models\Inventory\Product', 'waste_details', 'waste_id', 'product_id')->withPivot('product_id', 'total_quantity');
    }

    public function scopeFindWaste($query, $search)
    {
        return $query->whereHas('products', function ($q) use ($search) {
            $q->where('name', "LIKE", "%$search%")
                ->OrWhere('code', "LIKE", "%$search%");
        })->OrWhere('reason', "LIKE", "%$search%");

    }

}

<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'description',
    ];

    protected $table = 'categories';

    public function products()
    {
        return $this->hasMany('App\Models\Inventory\Product', 'category_id', 'id');
    }

    public function scopeFindCategory($query, $search)
    {

        return $query->where('name', "LIKE", "%$search%");

    }
}

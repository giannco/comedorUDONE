<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campus extends Model
{
    protected $fillable = [
        'name', 'description',
    ];
    
    protected $table = 'campus';

    public function sale()
    {
        return $this->hasOne('App\Models\Inventory\Sale' , 'campus_id', 'id');
    }
}

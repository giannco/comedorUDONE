<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comensal extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'ic', 'first_name', 'last_name', 'photo', 'type_id', 'career', 'sex',
    ];

    protected $table = 'comensales';

    public function arrivals()
    {
        return $this->hasMany('App\Models\Arrival', 'comensal_ic', 'ic');
    }

    public function student()
    {
        return $this->hasOne('App\Models\Student', 'comensal_ic', 'ic');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\ComensalType', 'type_id', 'id')->withTrashed();
    }

    public function scopeFindComensal($query, $search)
    {
        if (trim($search) != "") {

            $query->where('ic', "LIKE", "%$search%")
                ->OrWhere('first_name', "LIKE", "%$search%")
                ->OrWhere('last_name', "LIKE", "%$search%");
        }
    }
}

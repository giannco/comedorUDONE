<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComensalType extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name', 'description',
    ];

    protected $table = 'comensal_types';

    public function comensals()
    {
        return $this->hasMany('App\Models\Comensal', 'type_id', 'id');
    }

}

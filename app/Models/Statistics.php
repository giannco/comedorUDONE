<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Statistics extends Model
{
    public static function getCareersByArrivals($arrivals=[],$var){

    	return $arrivals['data']->unique($var)->map(function($map) use ($arrivals,$var){

            return [
                '{$var}'=>$map[$var],
                'statistics' => [
                    'total_f'=>$arrivals['data']->where($var,$map[$var])->where('sex','F')->count(),
                    'total_m' => $arrivals['data']->where($var,$map[$var])->where('sex','M')->count(),
                    'total' => $arrivals['data']->where($var,$map[$var])->count(),
                ],
            ];
        });
       
    }
}

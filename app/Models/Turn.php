<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Turn extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name', 'description', 'start_hour', 'end_hour',
    ];

    protected $table = 'turns';

    public function arrivals()
    {
        return $this->hasMany('App\Models\Arrival', 'turn_id', 'id');
    }

    public function scopeExistsTurnWithHour($query, $request)
    {
        $current_date = Carbon::now()->toTimeString();

        return $query->where('id', $request->turn_id)->where('start_hour', '<=', $current_date)->where('end_hour', '>=', $current_date)->first();
    }

    public function scopeFindTurn($query, $search)
    {

        return $query->where('name', "LIKE", "%$search%");

    }
}

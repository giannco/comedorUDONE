<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arrival extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'comensal_ic', 'turn_id',
    ];

    public function comensal()
    {
        return $this->belongsTo('App\Models\Comensal', 'comensal_ic', 'ic')->latest()->withTrashed();
    }
    public function turn()
    {
        //Con withTrashed se trae los turnos desactivados.
        return $this->belongsTo('App\Models\Turn', 'turn_id', 'id')->withTrashed();
    }
    public function scopeAlreadyExists($query, $request)
    {

        $current_date = Carbon::now()->toDateString();

        return $query->where('comensal_ic', $request->comensal_ic)->where('created_at', 'like', "%$current_date%")->where('turn_id', $request->turn_id)->first();
    }
}

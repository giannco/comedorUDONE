<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TodayMenu extends Model
{
    protected $fillable = [
        'turn_id', 'description',
    ];

    protected $table = 'today_menu';

    public function turn()
    {
        return $this->belongsTo('App\Models\Turn', 'turn_id', 'id');
    }
}

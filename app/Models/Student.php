<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'comensal_ic', 'career_id',
    ];

    protected $table = 'students';

    public function comensal()
    {
        return $this->belongsTo('App\Models\Comensal', 'comensal_ic', 'ic');
    }

    public function career()
    {
        return $this->belongsTo('App\Models\Career', 'career_id', 'id');
    }
}
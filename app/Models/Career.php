<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{

	//use SoftDeletes;

    protected $fillable = [
        'name', 'description',
    ];

    protected $table = 'careers';

    public function students()
    {
        return $this->hasMany('App\Models\Student', 'career_id', 'id');
    }
    
}
<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'v1', 'as' => '.v1'], function () {

    Route::group(['prefix' => 'today-menu', 'as' => '.today-menu'], function () {

        Route::get('/', ['as' => '.index', 'uses' => 'TodayMenuController@index']);
        Route::post('/', ['as' => '.store']);
        Route::put('/{id}', ['as' => '.update']);
        Route::delete('/{id}', ['as' => '.delete']);

    });

    Route::group(['prefix' => 'login', 'as' => '.login'], function () {
        //Route::get('/', ['uses' => 'Auth\LoginController@showLoginForm']);
        //Route::post('/', ['uses' => 'Auth\LoginController@login']);
        //Route::get('/register', ['as' => '.register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
        //Route::post('/register', ['as' => '.register', 'uses' => 'Auth\RegisterController@register']);
        //Route::post('/logout', ['as' => '.logout', 'uses' => 'Auth\LoginController@logout']);
    });

});

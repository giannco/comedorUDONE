<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
})->name('/');

//Auth::routes();

//Rutas de inicio de sesion
Route::group(['prefix' => 'login', 'as' => 'login', 'middleware' => 'web'], function () {
    Route::get('/', ['uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('/', ['uses' => 'Auth\LoginController@login']);
});

Route::group(['prefix' => 'register', 'as' => 'register', 'middleware' => ['web', 'auth', 'role:admin']], function () {
    Route::get('/', ['uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('/', ['uses' => 'Auth\RegisterController@register']);

});

Route::group(['prefix' => 'password', 'as' => 'password', 'middleware' => 'web'], function () {
    //Route::get('/reset', ['as' => '.request', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    //Route::post('/reset', ['as' => '.request', 'uses' => 'Auth\ResetPasswordController@reset']);
    //Route::post('/email', ['as' => '.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
});

Route::group(['prefix' => 'logout', 'as' => 'logout', 'middleware' => 'web'], function () {
    Route::post('/', ['uses' => 'Auth\LoginController@logout']);
});

/*  Rutas para modulo de reportes*/

//Rutas de módulo de reporte
Route::group(['prefix' => 'reportes', 'as' => 'reports', 'middleware' => ['auth']], function () {
    Route::get('/', ['uses' => 'Web\ReportController@index']);
    Route::post('/export', ['as' => '.pdf', 'uses' => 'Web\ReportController@pdf']);
});

/*  Rutas para módulo de configuración*/

//Rutas de configuracion vista
Route::group(['prefix' => 'configuracion', 'as' => 'configuration', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/', ['uses' => 'Web\ConfigurationController@show']);
});

//Rutas de usuarios
Route::group(['prefix' => 'users', 'as' => 'users', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\UserController@index']);
});

//Rutas de roles
Route::group(['prefix' => 'roles', 'as' => 'roles', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\RoleController@index']);
});

//Rutas de permisos
Route::group(['prefix' => 'permissions', 'as' => 'permissions', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\PermissionController@index']);
    Route::put('/', ['as' => '.update', 'uses' => 'Web\PermissionController@update']);
});

/*  Rutas para módulo de control de acceso*/

//Ruta vista para el módulo control de acceso
Route::group(['prefix' => 'control-acceso', 'as' => 'control-access', 'middleware' => ['auth']], function () {
    Route::get('/', ['uses' => 'Web\ControlAccessController@show'])->middleware('permission:control-access');
});

//Rutas de menu del dia
Route::group(['prefix' => 'menu', 'as' => 'today-menu', 'middleware' => ['auth', 'role:admin']], function () {
    //Route::get('/', ['as' => '.index', 'uses' => 'TodayMenuController@index']);
    Route::get('/', ['uses' => 'TodayMenuController@create']);
    Route::post('/', ['as' => '.store', 'uses' => 'TodayMenuController@store']);
});

//Rutas de comensales
Route::group(['prefix' => 'comensales', 'as' => 'comensales', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\ComensalController@index'])->middleware('permission:comensales.index');
    //Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\ComensalController@edit']);
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\ComensalController@show']);
    Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\ComensalController@update'])->middleware('permission:comensales.update');
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\ComensalController@create']);
    Route::post('/', ['as' => '.store', 'uses' => 'Web\ComensalController@store'])->middleware('permission:comensales.store');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\ComensalController@destroy'])->middleware('permission:comensales.delete');
    Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\ComensalController@activate'])->middleware('permission:comensales.activate');
    Route::post('/import', ['as' => '.import', 'uses' => 'Web\ComensalController@import'])->middleware('permission:comensales.import');
});

//Rutas de tipos de comensales
Route::group(['prefix' => 'comensal-type', 'as' => 'comensal-type', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\ComensalTypeController@index'])->middleware('permission:comensal-type.index');
    Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\ComensalTypeController@activate'])->middleware('permission:comensal-type.activate');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\ComensalTypeController@destroy'])->middleware('permission:comensal-type.delete');
});

//Ruta de registro de llegadas
Route::get('/home', 'Web\HomeController@index')->name('home')->middleware('permission:home');

Route::group(['prefix' => 'arrivals', 'as' => 'arrivals', 'middleware' => ['auth']], function () {
    Route::get('/', ['uses' => 'Web\ArrivalController@index'])->middleware('permission:arrivals.index');
    Route::post('/', ['uses' => 'Web\ArrivalController@store'])->middleware('permission:arrivals.post');
});

//Rutas de turnos
Route::group(['prefix' => 'turns', 'as' => 'turns', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\TurnController@index'])->middleware('permission:turns.index');
    //Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\TurnController@edit'])->middleware('permission:turns.edit');
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\TurnController@show'])->middleware('permission:turns.show');
    Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\TurnController@update'])->middleware('permission:turns.update');
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\TurnController@create'])->middleware('permission:turns.create');
    Route::post('/', ['as' => '.store', 'uses' => 'Web\TurnController@store'])->middleware('permission:turns.store');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\TurnController@destroy'])->middleware('permission:turns.delete');
    Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\TurnController@activate'])->middleware('permission:turns.activate');
});

//Rutas de carreras
Route::group(['prefix' => 'careers', 'as' => 'careers', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\CareerController@index'])->middleware('permission:careers.index');
});

//Rutas de campus
Route::group(['prefix' => 'campus', 'as' => 'campus', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\CampusController@index'])->middleware('permission:campus.index');
});

/*  Rutas para inventario*/

//Rutas vista de módulo de inventario
Route::group(['prefix' => 'inventario', 'as' => 'inventory', 'middleware' => ['auth']], function () {
    Route::get('/', ['uses' => 'Web\InventoryController@index'])->middleware('permission:inventory');
});

//proveedores
Route::group(['prefix' => 'providers', 'as' => 'providers', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\Inventory\ProviderController@index'])->middleware('permission:providers.index');
    //Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\Inventory\ProviderController@edit'])->middleware('permission:providers.edit');
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\Inventory\ProviderController@show'])->middleware('permission:providers.show');
    Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\Inventory\ProviderController@update'])->middleware('permission:providers.update');
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\Inventory\ProviderController@create'])->middleware('permission:providers.create');
    Route::post('/', ['as' => '.store', 'uses' => 'Web\Inventory\ProviderController@store'])->middleware('permission:providers.store');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\Inventory\ProviderController@destroy'])->middleware('permission:providers.delete');
    Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\Inventory\ProviderController@activate'])->middleware('permission:providers.activate');
});

//compras-entrada
Route::group(['prefix' => 'purchases', 'as' => 'purchases', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\Inventory\PurchaseController@index'])->middleware('permission:purchases.index');
    //Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\Inventory\PurchaseController@edit'])->middleware('permission:purchases.edit');
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\Inventory\PurchaseController@show'])->middleware('permission:purchases.show');
    Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\Inventory\PurchaseController@update'])->middleware('permission:purchases.update');
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\Inventory\PurchaseController@create'])->middleware('permission:purchases.create');
    Route::post('/', ['as' => '.store', 'uses' => 'Web\Inventory\PurchaseController@store'])->middleware('permission:purchases.store');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\Inventory\PurchaseController@destroy'])->middleware('permission:purchases.delete');
    // Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\CategoryController@activate']);
});

//ventas-salida
Route::group(['prefix' => 'sales', 'as' => 'sales', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\Inventory\SaleController@index'])->middleware('permission:sales.index');
    //Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\Inventory\SaleController@edit'])->middleware('permission:sales.edit');
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\Inventory\SaleController@show'])->middleware('permission:sales.show');
    Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\Inventory\SaleController@update'])->middleware('permission:sales.update');
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\Inventory\SaleController@create'])->middleware('permission:sales.create');
    Route::post('/', ['as' => '.store', 'uses' => 'Web\Inventory\SaleController@store'])->middleware('permission:sales.store');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\Inventory\SaleController@destroy'])->middleware('permission:sales.delete');
    // Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\CategoryController@activate']);
});

//categorias
Route::group(['prefix' => 'categories', 'as' => 'categories', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\Inventory\CategoryController@index'])->middleware('permission:categories.index');
    // Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\Inventory\CategoryController@edit'])->middleware('permission:categories.edit');
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\Inventory\CategoryController@show'])->middleware('permission:categories.show');
    Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\Inventory\CategoryController@update'])->middleware('permission:categories.update');
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\Inventory\CategoryController@create'])->middleware('permission:categories.create');
    Route::post('/', ['as' => '.store', 'uses' => 'Web\Inventory\CategoryController@store'])->middleware('permission:categories.store');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\Inventory\CategoryController@destroy'])->middleware('permission:categories.delete');
    Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\Inventory\CategoryController@activate'])->middleware('permission:categories.activate');
});

//productos
Route::group(['prefix' => 'products', 'as' => 'products', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\Inventory\ProductController@index'])->middleware('permission:products.index');
    //Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\Inventory\ProductController@edit'])->middleware('permission:products.edit');
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\Inventory\ProductController@show'])->middleware('permission:products.show');
    Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\Inventory\ProductController@update'])->middleware('permission:products.update');
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\Inventory\ProductController@create'])->middleware('permission:products.create');
    Route::post('/', ['as' => '.store', 'uses' => 'Web\Inventory\ProductController@store'])->middleware('permission:products.store');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\Inventory\ProductController@destroy'])->middleware('permission:products.delete');
    Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\Inventory\ProductController@activate'])->middleware('permission:products.activate');
});

//desperdicios
Route::group(['prefix' => 'waste', 'as' => 'waste', 'middleware' => ['auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\Inventory\WasteController@index'])->middleware('permission:waste.index');
    //Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\Inventory\WasteController@edit'])->middleware('permission:waste.edit');
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\Inventory\WasteController@show'])->middleware('permission:waste.show');
    Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\Inventory\WasteController@update'])->middleware('permission:waste.update');
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\Inventory\WasteController@create'])->middleware('permission:waste.create');
    Route::post('/', ['as' => '.store', 'uses' => 'Web\Inventory\WasteController@store'])->middleware('permission:waste.store');
    Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\Inventory\WasteController@destroy'])->middleware('permission:waste.delete');
    Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\Inventory\WasteController@activate'])->middleware('permission:waste.activate');
});

//stock
Route::group(['prefix' => 'stock', 'as' => 'stock', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'Web\Inventory\StockController@index']);
    //Route::get('{id}/edit', ['as' => '.edit', 'uses' => 'Web\Inventory\StockController@edit']);
    //Route::get('{id}/show', ['as' => '.show', 'uses' => 'Web\Inventory\StockController@show']);
    //Route::put('/{id}', ['as' => '.update', 'uses' => 'Web\Inventory\StockController@update']);
    //Route::get('create', ['as' => '.create', 'uses' => 'Web\Inventory\StockController@create']);
    //Route::post('/', ['as' => '.store', 'uses' => 'Web\Inventory\StockController@store']);
    //Route::delete('/{id}', ['as' => '.delete', 'uses' => 'Web\Inventory\StockController@destroy']);
    // Route::put('{id}/activate', ['as' => '.activate', 'uses' => 'Web\CategoryController@activate']);
});

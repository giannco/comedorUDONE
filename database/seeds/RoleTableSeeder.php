<?php
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'administrador';
        $role->guard_name = 'web';
        $role->save();

        $role = new Role();
        $role->name = 'nutricionista';
        $role->description = 'nutricionista';
        $role->guard_name = 'web';
        $role->save();

        $role = new Role();
        $role->name = 'almacen';
        $role->description = 'jefe de almacen';
        $role->guard_name = 'web';
        $role->save();

        $role = new Role();
        $role->name = 'operador';
        $role->description = 'registro de llegadas';
        $role->guard_name = 'web';
        $role->save();
    }
}

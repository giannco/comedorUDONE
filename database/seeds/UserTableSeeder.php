<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@udo.ne.com';
        $user->password = bcrypt('secret');
        $user->assignRole('admin');
        $user->save();

        $user = new User();
        $user->name = 'operador';
        $user->email = 'operador@udo.ne.com';
        $user->password = bcrypt('secret');
        $user->assignRole('operador');
        $user->save();

        $user = new User();
        $user->name = 'nutricionista';
        $user->email = 'nutricionista@udo.ne.com';
        $user->password = bcrypt('secret');
        $user->assignRole('nutricionista');
        $user->save();

        $user = new User();
        $user->name = 'almacen';
        $user->email = 'almacen@udo.ne.com';
        $user->password = bcrypt('secret');
        $user->assignRole('almacen');
        $user->save();

    }
}

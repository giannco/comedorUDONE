<?php

use App\Models\Career;
use App\Models\ComensalType;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ComensalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        //factory('App\Models\Comensal', 49)->create();

        $faker = Faker::create();

        $comensalType = ComensalType::get();

        for ($i = 0; $i < 20; $i++) {

            $comensalType->each(function ($ct) use ($faker) {
                $randSex = array_rand(['m', 'f']) ? 'm' : 'f';
                $carrer = Career::get()->random(1);
                $comensal_ic = $faker->ean8;

                DB::table('comensales')->insert([
                    'ic' => $comensal_ic,
                    'first_name' => $faker->firstNameMale,
                    'sex' => $randSex,
                    'last_name' => $faker->lastName,
                    'type_id' => $ct->id,
                    'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                ]);

                if ($ct->name == 'estudiante') {
                    DB::table('students')->insert([
                        'comensal_ic' => $comensal_ic,
                        'career_id' => $carrer[0]->id,
                    ]);
                }

            });

        }
    }
}

<?php

use Illuminate\Database\Seeder;

class CareerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('careers')->insert([
            [
                "name" => "licenciatura en informatica",
                "description" => "",
            ],
            [
                "name" => "licenciatura en educación integral",
                "description" => "",
            ],
            [
                "name" => "licenciatura en administración",
                "description" => "",
            ],
            [
                "name" => "lic. edu. menc. ingles",
                "description" => "",
            ],
            [
                "name" => "lic. en enfermería",
                "description" => "",
            ],
            [
                "name" => "técnico superior en administración de empresas turísticas",
                "description" => "",
            ],
            [
                "name" => "licenciatura en hotelería",
                "description" => "",
            ],
            [
                "name" => "licenciatura en contaduría pública",
                "description" => "",
            ],
            [
                "name" => "licenciatura en turismo",
                "description" => "",
            ],
            [
                "name" => "licenciatura en tecnología de alimentos",
                "description" => "",
            ],
            [
                "name" => "técnico superior en administración de empresas hoteleras",
                "description" => "",
            ],
            [
                "name" => "licenciatura en estadística",
                "description" => "",
            ],
            [
                "name" => "licenciatura en biología marina",
                "description" => "",
            ],
            [
                "name" => "licenciatura en acuacultura",
                "description" => "",
            ],

        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('permissions')->insert([
            [
                "name" => "control-access",
                "description" => "Módulo control de acceso",
                "guard_name" => "web",
            ],
            [
                "name" => "home",
                "description" => "Módulo home",
                "guard_name" => "web",
            ],
            [
                "name" => "arrivals.index",
                "description" => "Ver llegadas",
                "guard_name" => "web",
            ],
            [
                "name" => "arrivals.post",
                "description" => "Registar llegadas",
                "guard_name" => "web",
            ],
            [
                "name" => "careers.index",
                "description" => "Ver carrerras",
                "guard_name" => "web",
            ],
            [
                "name" => "campus.index",
                "description" => "Ver campus",
                "guard_name" => "web",
            ],
            [
                "name" => "comensales.index",
                "description" => "Ver comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "comensales.store",
                "description" => "Registar comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "comensales.update",
                "description" => "Actualizar comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "comensales.delete",
                "description" => "Eliminar comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "comensales.activate",
                "description" => "Activar comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "comensales.import",
                "description" => "Importar comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "comensal-type.activate",
                "description" => "Activar tipo de comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "comensal-type.index",
                "description" => "Ver tipo de comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "comensal-type.delete",
                "description" => "Eliminar tipo de comensales",
                "guard_name" => "web",
            ],
            [
                "name" => "turns.index",
                "description" => "Ver turnos",
                "guard_name" => "web",
            ],
            [
                "name" => "turns.store",
                "description" => "Registrar turnos",
                "guard_name" => "web",
            ],
            [
                "name" => "turns.update",
                "description" => "Editar turnos",
                "guard_name" => "web",
            ],
            [
                "name" => "turns.delete",
                "description" => "Eliminar turnos",
                "guard_name" => "web",
            ],
            [
                "name" => "turns.activate",
                "description" => "Activar turnos",
                "guard_name" => "web",
            ],
            [
                "name" => "inventory",
                "description" => "Módulo de inventario",
                "guard_name" => "web",
            ],
            [
                "name" => "sales.index",
                "description" => "Lista de ventas",
                "guard_name" => "web",
            ],
            [
                "name" => "sales.store",
                "description" => "Registrar ventas",
                "guard_name" => "web",
            ],

            [
                "name" => "sales.update",
                "description" => "Editar ventas",
                "guard_name" => "web",
            ],
            [
                "name" => "sales.delete",
                "description" => "Eliminar ventas",
                "guard_name" => "web",
            ],
            [
                "name" => "categories.index",
                "description" => "Ver categorias",
                "guard_name" => "web",
            ],
            [
                "name" => "categories.store",
                "description" => "Registrar categorias",
                "guard_name" => "web",
            ],
            [
                "name" => "categories.update",
                "description" => "Editar categorias",
                "guard_name" => "web",
            ],
            [
                "name" => "categories.delete",
                "description" => "Eliminar categorias",
                "guard_name" => "web",
            ],
            [
                "name" => "categories.activate",
                "description" => "Activar categorias",
                "guard_name" => "web",
            ],
            [
                "name" => "purchases.index",
                "description" => "Ver compras",
                "guard_name" => "web",
            ],
            [
                "name" => "purchases.store",
                "description" => "Registrar compras",
                "guard_name" => "web",
            ],
            [
                "name" => "purchases.delete",
                "description" => "Eliminar compras",
                "guard_name" => "web",
            ],
            [
                "name" => "purchases.update",
                "description" => "Editar compras",
                "guard_name" => "web",
            ],
            [
                "name" => "providers.index",
                "description" => "Ver proveedores",
                "guard_name" => "web",
            ],
            [
                "name" => "providers.store",
                "description" => "Registrar proveedores",
                "guard_name" => "web",
            ],
            [
                "name" => "providers.update",
                "description" => "Editar proveedores",
                "guard_name" => "web",
            ],
            [
                "name" => "providers.delete",
                "description" => "Eliminar proveedores",
                "guard_name" => "web",
            ],
            [
                "name" => "providers.activate",
                "description" => "Activar proveedores",
                "guard_name" => "web",
            ],
            [
                "name" => "products.index",
                "description" => "Ver productos",
                "guard_name" => "web",
            ],
            [
                "name" => "products.store",
                "description" => "Registrar productos",
                "guard_name" => "web",
            ],
            [
                "name" => "products.update",
                "description" => "Editar productos",
                "guard_name" => "web",
            ],
            [
                "name" => "products.delete",
                "description" => "Eliminar productos",
                "guard_name" => "web",
            ],
            [
                "name" => "products.activate",
                "description" => "Activar productos",
                "guard_name" => "web",
            ],
            [
                "name" => "waste.index",
                "description" => "Ver desperdicios",
                "guard_name" => "web",
            ],
            [
                "name" => "waste.store",
                "description" => "Registrar desperdicios",
                "guard_name" => "web",
            ],
            [
                "name" => "waste.update",
                "description" => "Editar desperdicios",
                "guard_name" => "web",
            ],
            [
                "name" => "waste.delete",
                "description" => "Eliminar desperdicios",
                "guard_name" => "web",
            ],
            [
                "name" => "waste.activate",
                "description" => "Activar desperdicios",
                "guard_name" => "web",
            ],
            [
                "name" => "users.index",
                "description" => "Ver usuarios",
                "guard_name" => "web",
            ],
            [
                "name" => "users.store",
                "description" => "Registrar usuarios",
                "guard_name" => "web",
            ],
            [
                "name" => "roles.index",
                "description" => "Ver roles",
                "guard_name" => "web",
            ],
            [
                "name" => "stock.index",
                "description" => "Ver stock",
                "guard_name" => "web",
            ],
            [
                "name" => "reports",
                "description" => "Módulo de reportes",
                "guard_name" => "web",
            ],
            [
                "name" => "reports.pdf",
                "description" => "Exportar reportes formato pdf",
                "guard_name" => "web",
            ],
        ]);
    }
}

<?php

use App\Models\Turn;
use Illuminate\Database\Seeder;

class TurnTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $turn = new Turn();
        $turn->name = 'desayuno';
        $turn->description = 'desayuno';
        $turn->start_hour = '08:30:00';
        $turn->end_hour = '10:00:00';

        $turn->save();

        $turn = new Turn();
        $turn->name = 'almuerzo';
        $turn->description = 'almuerzo';
        $turn->start_hour = '11:30:00';
        $turn->end_hour = '01:00:00';

        $turn->save();

        $turn = new Turn();
        $turn->name = 'cena';
        $turn->description = 'cena';
        $turn->start_hour = '02:30:00';
        $turn->end_hour = '04:00:00';

        $turn->save();
    }
}

<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                "name" => "frutas",
                "description" => "Frutas",

            ],
            [
                "name" => "bulbos",
                "description" => "Verduras tipo bulbo",

            ],
            [
                "name" => "cereales",
                "description" => "Cereales",

            ],
            [
                "name" => "legumbres",
                "description" => "Legumbres",

            ],
            [
                "name" => "tubérculos",
                "description" => "Verduras tipo tubérculos",

            ],
            [
                "name" => "frutos secos",
                "description" => "Frutos secos",

            ],
            [
                "name" => "carne",
                "description" => "Carne",

            ],
            [
                "name" => "pescado",
                "description" => "Pescado",

            ],
            [
                "name" => "huevo",
                "description" => "Huevo",

            ],
            [
                "name" => "leche",
                "description" => "Leche",

            ],
            [
                "name" => "productos lácteos",
                "description" => "Productos lácteos",

            ],
            [
                "name" => "aceites",
                "description" => "Aceites",

            ],
            [
                "name" => "grasas",
                "description" => "Grasas",

            ],
            [
                "name" => "mantequilla",
                "description" => "Mantequilla",

            ],
            [
                "name" => "Bebidas",
                "description" => "Bebidas",

            ],

        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // La creación de datos de roles debe ejecutarse primero
        $this->call(RoleTableSeeder::class);

        //inserta permisios por defecto
        $this->call(PermissionsTableSeeder::class);

        // Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);

        // Creando tipos de comensales
        $this->call(ComensalTypesTableSeeder::class);

        //Insertando carreras por defecto
        $this->call(CareerTableSeeder::class);

        // Creando tipos de comensales
        $this->call(ComensalTableSeeder::class);

        // Creando turno por defecto
        $this->call(TurnTableSeeder::class);

        //Insertando proveedores por defecto
        $this->call(ProvidersTableSeeder::class);

        //inserta Categorias por defecto
        $this->call(CategoryTableSeeder::class);

        //inserta productos por defecto
        $this->call(ProductTableSeeder::class);

        //inserta productos por defecto
        $this->call(CampusTableSeeder::class);

        //Asignar permisos al rol admin
        $this->call(PermissionRoleAdminSeeder::class);

        //Creando llegadas por defecto
        $this->call(ArrivalsTableSeeder::class);

    }
}

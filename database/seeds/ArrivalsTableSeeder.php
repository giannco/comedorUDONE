<?php

use App\Models\Comensal;
use App\Models\Turn;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ArrivalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $turns = Turn::get();

        $comensales = Comensal::get();

        $faker = Faker::create();

        $turns->each(function ($turn) use ($comensales, $faker) {

            $comensales->each(function ($comensal) use ($turn, $faker) {

                DB::table('arrivals')->insert([
                    'comensal_ic' => $comensal->ic,
                    'turn_id' => $turn->id,
                    'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),

                ]);
            });

        });
    }
}

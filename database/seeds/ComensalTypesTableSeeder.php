<?php
use App\Models\ComensalType;
use Illuminate\Database\Seeder;

class ComensalTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new ComensalType();
        $role->name = 'estudiante';
        $role->description = 'Personal estudiantil';
        $role->save();

        $role = new ComensalType();
        $role->name = 'docente';
        $role->description = 'Personal docente';
        $role->save();

        $role = new ComensalType();
        $role->name = 'administrativo';
        $role->description = 'Personal administrativo';
        $role->save();

        $role = new ComensalType();
        $role->name = 'obrero';
        $role->description = 'Personal obrero';
        $role->save();
    }
}

<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionRoleAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //asignacion de permiso a roles por defecto
        $role = Role::where('name', 'admin')->first();

        Permission::get()->each(function ($permission) use ($role) {

            //print($p->id);
            $role->givePermissionTo($permission->id);

        });
    }
}

<?php

use Illuminate\Database\Seeder;

class CampusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campus')->insert([
            [
    			"name" => "GUATAMARE",
    			"description" => "",
    		],
    		[
    			"name" => "BOCA DE RÍO",
    			"description" => "",
    		],
        ]);
    }
}

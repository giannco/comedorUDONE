<?php

use Illuminate\Database\Seeder;

class ProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('providers')->insert([
            [
                'type' => 'j',
                'rif' => '301370139',
                'name' => 'Pepsi-Cola Venezuela c.a',
                'address' => 'Esq. 4ta Transversal Edif Centro Empresarial Polar',
                'phone' => '',

            ],
            [
                'type' => 'j',
                'rif' => '408486059',
                'name' => 'DistMarg C.A',
                'address' => 'Calle Tacarigua',
                'phone' => '04163365598',

            ],
            [
                'type' => 'j',
                'rif' => '306469605',
                'name' => 'Inversiones Malepa',
                'address' => 'Conejeros',
                'phone' => '02954004715',

            ],
            [
                'type' => 'j',
                'rif' => '000413126',
                'name' => 'Alimentos Polar',
                'address' => 'San Juan Bautista',
                'phone' => '02122023111',
            ],
            [
                'type' => 'j',
                'rif' => '306742654',
                'name' => 'Diarca s.a',
                'address' => 'San Juan Bautista',
                'phone' => '02952744023',
            ],
            [
                'type' => 'j',
                'rif' => '409086496',
                'name' => 'Incarven',
                'address' => 'El cardon',
                'phone' => '02952348779',
            ],

        ]);
    }
}

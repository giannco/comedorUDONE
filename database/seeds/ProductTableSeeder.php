<?php

use App\Models\Inventory\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $category = Category::get(['id']);

        $faker = Faker::create();

        DB::table('products')->insert([
            [
                "name" => "cambur",
                "description" => "Fruta",
                "category_id" => $category[0]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "mandarina",
                "description" => "Fruta",
                "category_id" => $category[0]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "naranja",
                "description" => "Fruta",
                "category_id" => $category[0]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "patilla",
                "description" => "Fruta",
                "category_id" => $category[0]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "lechosa",
                "description" => "Fruta",
                "category_id" => $category[0]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "melon",
                "description" => "Fruta",
                "category_id" => $category[0]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "parchita",
                "description" => "Fruta",
                "category_id" => $category[0]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "platano",
                "description" => "Fruta",
                "category_id" => $category[0]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "ají",
                "description" => "Verdura tipo bulbo",
                "category_id" => $category[1]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "tomate",
                "description" => "Verdura tipo bulbo",
                "category_id" => $category[1]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "cebolla",
                "description" => "Verdura tipo bulbo",
                "category_id" => $category[1]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "arroz",
                "description" => "cereal",
                "category_id" => $category[2]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "maiz",
                "description" => "cereal",
                "category_id" => $category[2]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "avena",
                "description" => "cereal",
                "category_id" => $category[2]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "trigo",
                "description" => "cereal",
                "category_id" => $category[2]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "harina de maiz precocida",
                "description" => "cereal",
                "category_id" => $category[2]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "harina de trigo",
                "description" => "cereal",
                "category_id" => $category[2]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "cebada",
                "description" => "cereal",
                "category_id" => $category[2]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "lenteja",
                "description" => "Legumbre",
                "category_id" => $category[3]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "caraota",
                "description" => "Legumbre",
                "category_id" => $category[3]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "frijol",
                "description" => "Legumbre",
                "category_id" => $category[3]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "arveja",
                "description" => "Legumbre",
                "category_id" => $category[3]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "papa",
                "description" => "Verdura tipo tubérculo",
                "category_id" => $category[4]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "zanahoria",
                "description" => "Verdura tipo tubérculo",
                "category_id" => $category[4]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "yuca",
                "description" => "Verdura tipo tubérculo",
                "category_id" => $category[4]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "auyama",
                "description" => "Verdura",
                "category_id" => $category[4]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "ocumo chino",
                "description" => "Verdura tipo tubérculo",
                "category_id" => $category[4]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "batata",
                "description" => "Verdura tipo tubérculo",
                "category_id" => $category[4]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "ñame",
                "description" => "Verdura tipo tubérculo",
                "category_id" => $category[4]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "carne roja",
                "description" => "carne de res",
                "category_id" => $category[6]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],
            [
                "name" => "pollo beneficiado",
                "description" => "carne blanca",
                "category_id" => $category[6]->id,
                "code" => $faker->randomLetter . $faker->unique()->numberBetween(1, 9999),

            ],

        ]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('campus_id')->unsigned();
            $table->enum('status', ['aprobada', 'cancelada', 'en proceso'])->default('en proceso');
            $table->integer('total_cost')->nullable()->default(0);
            $table->string('description')->nullable();
            $table->date('sale_date')->required();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('campus_id')
                ->references('id')->on('campus')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

        });
        //ventas-productos
        Schema::create('sale_details', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('sale_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('total_quantity')->required()->default(0);
            $table->double('sale_price')->nullable()->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('sale_id')
                ->references('id')->on('sales')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_details');
        Schema::dropIfExists('sales');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArrivalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arrivals', function (Blueprint $table) {
            $table->string('comensal_ic');
            $table->integer('turn_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('comensal_ic')
                ->references('ic')->on('comensales')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('turn_id')
                ->references('id')->on('turns')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arrivals');
    }
}

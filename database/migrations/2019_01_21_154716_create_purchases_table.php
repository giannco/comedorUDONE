<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('provider_id')->unsigned();
            $table->enum('status', ['aprobada', 'cancelada', 'en proceso'])->default('en proceso');
            $table->double('total_cost')->nullable()->default(0);
            $table->string('description')->nullable();
            $table->date('purchase_date')->required();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('provider_id')
                ->references('id')->on('providers')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

        });
        //compras-productos
        Schema::create('purchase_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('total_quantity')->required()->default(0); //cantidad
            $table->double('purchase_price')->nullable()->default(0); //precio de compra

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('purchase_id')
                ->references('id')->on('purchases')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_details');
        Schema::dropIfExists('purchases');

    }
}

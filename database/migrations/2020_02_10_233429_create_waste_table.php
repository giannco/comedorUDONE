<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('reason', ['dañado', 'merma', 'vencido'])->default('merma');
            $table->enum('status', ['aprobada', 'cancelada', 'en proceso'])->default('en proceso');
            $table->string('description')->nullable();
            $table->date('waste_date')->required();

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('waste_details', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('waste_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('total_quantity')->required()->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('waste_id')
                ->references('id')->on('waste')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste');
    }
}

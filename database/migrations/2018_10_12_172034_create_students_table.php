<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('students', function (Blueprint $table) {

            $table->string('comensal_ic');
            $table->integer('career_id')->unsigned();
            $table->primary(['comensal_ic', 'career_id']);
            //$table->primary('comensal_ic');
            $table->timestamps();

            $table->foreign('comensal_ic')
                ->references('ic')->on('comensales')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('career_id')
                ->references('id')->on('careers')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}

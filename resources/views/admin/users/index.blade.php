@extends('layouts.app')
@section('js')
<script>

    var vm =  new Vue({
            el: '.content',
            data: {

                users:[]

            },
            created: function () {
                 setTimeout(function() {
                    vm.userSearch()

                 },250)
            },
            methods: {

                 userSearch: function(data) {
                    let _vm = this

                    $.ajax({
                        url: "{{route('api.v1.users.index')}}",
                        data: data,
                        method: 'get',
                        dataType: 'json',
                        contentType: 'application/json',
                        headers: {
                            'Authorization':'',
                        },
                        success: function(data) {
                            _vm.users = data

                        },
                        error: function() {
                            console.log('error')
                        },
                        complete: function() {
                            //_vm.loading = false
                        },
                    })
                },

            }
        });
</script>

@stop
@section('content')
    <div class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card ">
                        <div class="card-header ">
                            <h4 class="card-title">Email Statistics</h4>
                            <p class="card-category">Last Campaign Performance</p>

                        </div>
                        <div class="card-body ">
                            <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>
                        </div>
                        <div class="card-footer ">
                            <div class="legend">
                                <i class="fa fa-circle text-info"></i> Open
                                <i class="fa fa-circle text-danger"></i> Bounce
                                <i class="fa fa-circle text-warning"></i> Unsubscribe
                            </div>
                            <hr>
                            <div class="stats">
                                <i class="fa fa-clock-o"></i> Campaign sent 2 days ago
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card ">
                        <div class="card-header ">
                            <h4 class="card-title">Users Behavior</h4>
                            <p class="card-category">24 Hours performance</p>
                        </div>
                        <div class="card-body ">
                            <div id="chartHours" class="ct-chart"></div>
                        </div>
                        <div class="card-footer ">
                            <div class="legend">
                                <i class="fa fa-circle text-info"></i> Open
                                <i class="fa fa-circle text-danger"></i> Click
                                <i class="fa fa-circle text-warning"></i> Click Second Time
                            </div>
                            <hr>
                            <div class="stats">
                                <i class="fa fa-history"></i> Updated 3 minutes ago
                            </div>
                        </div>
                    </div>
                </div>
            </div><!---END ROW-->

        </div><!--END CONTAINER FLUID-->
    </div><!--END CONTENT-->
    @endsection

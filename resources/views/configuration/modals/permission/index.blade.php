<div class="modal fade" id="showPermissionModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog" style="max-width: 50%;">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body" v-if="!loadingPermissions">
        <div>
          <span style="font-size:20px;"><b style="color: #87CB16;">Permisos</b></span>
        </div>
        <br/>
        <input type="text" name="findPermission" class="form-control" v-model="permission.search" placeholder="Buscar por nombre" v-on:keyup.enter="searchData(permission.search,getPermissions,'loadingPermissions')" style="font-size:12px;text-transform:uppercase">
        <br/>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> acción </th>
              <th> operador </th>
              <th> nutricionista </th>
              <th> almacenista </th>
            </tr>
          </thead>
          <tbody v-if="permissions.length > 0" >
            <tr v-for="(permission, index) in permissions">
              <td width="40%" style="font-size:12px;">@{{permission.description}}</td>
              <td width="20%"><input  @click.prevent ="updatePermission(permission.id,'operador',permission.checked_operator,index)" v-model="permission.checked_operator" type=checkbox  :value="index"></td></td>
              <td width="20%"> <input  @click.prevent ="updatePermission(permission.id,'almacen',permission.checked_ow,index)" v-model="permission.checked_ow" type=checkbox  :value="index"></td>
              <td width="20%"><input  @click.prevent ="updatePermission(permission.id,'nutricionista',permission.checked_nutritionist,index)" v-model="permission.checked_nutritionist" type=checkbox  :value="index"></td></td>
            </tr>
          </tbody>
        </table>
        <div v-if="pagination.last_page > 0">
              <paginate
                  :page-count="pagination.last_page"
                  :page-range="3"
                  :click-handler="setPagePermission"
                  :prev-text="'ant'"
                  :next-text="'sig'"
                  :container-class="'pagination'"
                  :page-class = "'page-item'"
                  :page-link-class = "'page-link'"
                  :prev-class = "'page-item'"
                  :prev-link-class = "'page-link'"
                  :next-class = "'page-item'"
                  :next-link-class = "'page-link'"
                  :initial-page="pagination.current_page -1">
              </paginate>
            </div>
      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

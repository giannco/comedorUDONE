<div class="modal fade" id="showRolesModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body" v-if="!loadingRoles">
        <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Roles</b></span>
        </div>
        <br/>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> Nombre </th>
              <th> Descripción </th>
              <th> Operación </th>
            </tr>
          </thead>
          <tbody v-if="roles.length > 0" >
            <tr v-for="role in roles">
              <td style="font-size:12px;">@{{role.name}}</td>
              <td style="font-size:12px;">@{{role.description}}</td>
              <td><a v-if="role.deleted_at == null" @click.prevent = "deleteRole(role.id)" href="#" data-toggle="tooltip" title="Desactivar" style="margin-left: 25px;"><i  class="fa fa-check-square-o" style="font-size:25px;color: #87CB16"></i></a><a v-else @click.prevent = "activateRole(role.id)" href="#"  data-toggle="tooltip" title="Activar" style="margin-left: 25px;"><i  class="fa fa-square-o" style="font-size:25px;color: #87CB16"></i></a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

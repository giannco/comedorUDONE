<div class="modal fade" id="createUserModal" role="dialog">
  <div class="modal-dialog">
    
      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">&times;</a>    
      </div>
      <div class="modal-body">
        @include('configuration.modals.user.partials.form')
      </div>  
    </div> 
  </div>
</div>
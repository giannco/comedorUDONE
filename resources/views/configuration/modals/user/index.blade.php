<div class="modal fade" id="showUserModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
        <!--<a href="#" @click.prevent="clearUser()" class="btn btn-success" data-toggle="modal" data-target="#createUserModal">Crear Usuario</a>-->
      </div>
      <div class="modal-body" v-if="!loadingUsers">
        <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Usuarios</b></span>
        </div>
        <br/>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> nombre </th>
              <th> correo electrónico </th>
              <th> operación </th>
            </tr>
          </thead>
          <tbody v-if="users.length > 0"  >
            <tr v-for="user in users">
              <td style="font-size:12px;">@{{user.name}}</td>
              <td style="font-size:12px;">@{{user.email}}</td>
              <td><a v-if="user.deleted_at == null" @click.prevent = "deleteUser(user.id)" href="#" data-toggle="tooltip" title="Desactivar" style="margin-left: 25px;"><i  class="fa fa-check-square-o" style="font-size:25px;color: #87CB16"></i></a><a v-else @click.prevent = "activateUser(user.id)" href="#"  data-toggle="tooltip" title="Activar" style="margin-left: 25px;"><i  class="fa fa-square-o" style="font-size:25px;color:green"></i></a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

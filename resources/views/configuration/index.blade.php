@extends('layouts.app')
@section('css')
<style>
  [v-cloak] {display: none}

  .loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
    margin-left: 35%;
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }

</style>
@endsection
@section('js')

<script src="js/vuejs-paginate.js"></script>
<script>

    var vm =  new Vue({
            el: '.content',
            data: {
                users:[],
                roles: [],
                permission: {
                  'search' : '',
                },
                permissions :[],
                pagination: {},
                error : false,
                errors : [],
                dataModal:[],
                loadingUsers : true,
                loadingRoles : true,
                loadingPermissions: true,
                checkflag: false,
            },

            created: function () {

              this.getUsers()
              this.getRoles()
              this.getPermissions()

            },
            mounted: function(){

              this.loadModalWithError()

            },
            methods: {

              getUsers: function(data) {

                let _vm = this

                this.loadingUsers = true

                axios({
                  method: 'get',
                  url:"{{route('users.index')}}",
                })
                .then(function (response) {
                  //success

                  _vm.users = response.data
                })
                .catch(function (error) {
                  //error
                  console.log('error',error)
                })
                .then(function () {
                  //completado
                  _vm.loadingUsers = false
                })
              },
              loadModalWithError:function(){

                if (this.error){
                  $(window).on('load',function(){
                      $('#createUserModal').modal('show')
                  })

                }

              },

              /* FIN USUARIOS METODOS*/

              //-----------Roles-----------------------

              getRoles: function(data) {

                let _vm = this

                this.loadingRoles = true

                axios({
                  method: 'get',
                  url:"{{route('roles.index')}}",
                })
                .then(function (response) {
                  //success

                  _vm.roles = response.data
                })
                .catch(function (error) {
                  //error
                  console.log('error',error)
                })
                .then(function () {
                  //completado
                  _vm.loadingRoles = false
                })
              },

              //---------------Permisos---------------------------

              getPermissions: function(data,page,search) {

                let _vm = this

                this.loadingPermissions = true

                let uri = "{{route('permissions.index')}}"

                if (page != "" && page != undefined){
                  uri = uri + "?page=" + page
                }
                if (uri.indexOf("?") != -1){
                  uri = uri + "&paginate=1"
                }else{
                  uri = uri + "?paginate=1"
                }
                if (search != "" && search != undefined){
                  uri = uri + "&search=" + search
                }

                axios({
                  method: 'get',
                  url: uri,
                })
                .then(function (response) {
                  //success

                  _vm.permissions = []

                  response.data.data.forEach(function(permission) {

                    let checked_admin = false
                    let checked_operator = false
                    let checked_ow = false
                    let checked_nutritionist = false

                    permission.roles.forEach(function(role){

                      switch (role) {
                        case 'admin':
                          checked_admin = true
                        break;
                        case 'operador':
                          checked_operator = true
                        break;
                        case 'almacen':
                          checked_ow = true
                        break;
                        case 'nutricionista':
                          checked_nutritionist = true
                        break;
                      }
                    })

                    _vm.permissions.push({
                      id: permission.id,
                      description : permission.description,
                      checked_admin : checked_admin,
                      checked_operator: checked_operator,
                      checked_ow : checked_ow,
                      checked_nutritionist : checked_nutritionist,

                    })


                  })

                   _vm.pagination = response.data.meta
                })
                .catch(function (error) {
                  //error
                  console.log('error',error)
                })
                .then(function () {
                  //completado
                  _vm.loadingPermissions = false
                })
              },
              updatePermission: function(id,role,checked,index){

                let uri = "{{route('permissions.update')}}"
                let method = "put"
                const params = {
                  id: id,
                  role : role,
                  checked : !checked,
                }

                switch (role) {
                  case 'admin':
                    vm.permissions[index].checked_admin = !checked
                  break;
                  case 'operador':
                    vm.permissions[index].checked_operator = !checked
                  break;
                  case 'almacen':
                    vm.permissions[index].checked_ow = !checked
                  break;
                  case 'nutricionista':
                    vm.permissions[index].checked_nutritionist = !checked
                  break;
                }

                axios({
                  method: method,
                  url: uri ,
                  data:params
                })
                  .then(function (response) {
                    //success
                    let msg = ""

                    !checked ? msg = "permiso habilitado" : msg = "permiso deshabilitado"

                    $.notify(msg,{
                      position:'top center',className:'success',arrowShow: false})

                    $(id).modal('hide')
                  })
                  .catch(function (error) {
                    //error
                    //console.log('error',error)
                    let msg = ''
                    //Muestra los msj de error dependiendo de la propiedad obtenida

                    msg = Object.values(error.response.data.errors)[0][0]

                    $.notify(msg,{
                      position:'top center',className:'error',arrowShow: false})
                  })
                  .then(function () {
                    //completado
                    //Vue.set(obj,'loading', false)

                  })
              },

              setPagePermission:function(page){

                let _vm = this

                _vm.loadingPermissions = true

                _vm.getPermissions(null,page,null)
              },


              searchData: function(search,getData,loadingName){
                let _vm = this

                Vue.set(vm,loadingName, true)

                vm.getPermissions("",1,search)

              },

              //------------------------------------------
              momentTime: function(time) {
                return moment(time).format('h:mm:ss A')
                },
            },
              components: {
                paginate: VuejsPaginate
            }
        });
</script>

@endsection

@section('content')
<div class="content"  v-cloak>
	 <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="m-b-md" style="align-items: center; display: flex;justify-content: center;">
          <span style="color: #636b6f;  font-family: 'Raleway', sans-serif;font-weight: 100;font-size: 42px;">Configuración</span><br>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <a href="#" data-toggle="modal" data-target="#showUserModal">
        <div class="link-box">
          <center><i style="font-size:30px;color:white;opacity: .86" class="fa fa-users" aria-hidden="true"></i></center>
          <center><span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Usuarios</span></center>
        </div>
      </a>

      <a href="#" data-toggle="modal" data-target="#showRolesModal">
        <div class="link-box">
          <center><i style="font-size:30px;color:white;opacity: .86" class="fa fa-unlock" aria-hidden="true"></i></center>
          <center><span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Roles</span></center>
        </div>
      </a>

      <a href="#" data-toggle="modal" data-target="#showPermissionModal">
        <div class="link-box">
          <center><i style="font-size:30px;color:white;opacity: .86" class="fa fa-unlock" aria-hidden="true"></i></center>
          <center><span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Permisos</span></center>
        </div>
      </a>

      @include('configuration.modals.user.index')

      @include('configuration.modals.user.create')

      @include('configuration.modals.role.index')

      @include('configuration.modals.permission.index')


    </div><!--END ROW-->

  </div><!--END CONTAINER-FLUID-->

</div>

@endsection

@extends('layouts.app')
@section('css')
<style>
  [v-cloak] {display: none}
  .loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 150px;
    height: 150px;
    animation: spin 2s linear infinite;
    margin: auto;
  }

  .pillButton {
    background-color: #ddd;
    border: none;
    color: black;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 16px;
}

.pillButton:hover {
    background-color: #f1f1f1;
}

</style>
@endsection
@section('js')
<script>

    var vm =  new Vue({
        el: '.content',
        data: {
            arrivals:[],
            latest_arrival : [],
            ic : '',
            turn : {
              'id': '',
              'name' : ''
            },
            errors : [],
            photo : false,
            loading : true,
            loadingSubmit : true,
            loadingTurn : true,
            loadingArrival : true,
            chartData: [],
            chartDataBySex: [],
            hora:'{{time()}}',
            pillButton: 1
        },

        created: function () {
            this.turnsearch()
        },

        methods: {

            getServerTime: function() {
                return $.ajax({async: false}).getResponseHeader( 'Date' );
            },

            momentTime: function(time) {
                return moment(time).format('h:mm:ss A')
            },

            arrivalsearch: function(data) {

              vm.loadingArrival = true

              axios({
                  method: 'get',
                  url: "{{route('arrivals')}}" + '?turn_id='+ vm.turn.id,
              })
              .then(function (response) {
                  //success
                  vm.chartData = [] //inicializa la grafica
                  vm.chartDataBySex = []
                  vm.pushChartDataByComensals(response.data.data)
                  vm.pushChartDataBySex(response.data.data)
                  vm.arrivals = response.data.data
                  vm.latest_arrival = response.data.data[0] //obtener el ultimo registro
              })
              .catch(function (error) {
                  //error
                  console.log('error',error)
                  })
                  .then(function () {
                      //completado
                      vm.loadingArrival = false
                  })
            },

            turnsearch: function(data) {

              this.loadingTurn = true

              axios({
                  method: 'get',
                  url: "{{route('turns.index')}}",
              })
              .then(function (response) {
                  //success
                  //saber si el objeto está vacio
                  if (Object.keys(response.data).length !== 0){
                    vm.turn.id = response.data.id
                    vm.turn.name = response.data.name
                  }

                  vm.arrivalsearch()
              })
              .catch(function (error) {
                  //error
                  console.log('error',error)
              })
              .then(function () {
                  //completado
                  this.loadingTurn = false
              })
            },

            submit: function() {

              this.loadingSubmit = true

                const params = {
                    comensal_ic : vm.ic,
                    turn_id : vm.turn.id,
                }

                axios({
                    method: 'post',
                    url: "{{route('arrivals')}}",
                    data: params,
                })
                .then(function (response) {
                  //success
                  vm.arrivalsearch()
                  vm.ic = ''
                  })
                  .catch(function (error) {
                    //error
                    msg = Object.values(error.response.data.errors)[0][0]
                    $.notify(msg,{
                      position:'top center',className:'error',arrowShow: false
                     })
                    })
                    .then(function () {
                        //completado
                        vm.loading = false
                    })
            },

            pushChartDataByComensals:function(data){

                let _vm = this
                var estudiante = 0;
                var docente = 0;
                var administrativo = 0;
                var obrero = 0;

                for (i=0;i<data.length;i++){
                    switch(data[i].type){
                      case 'estudiante':
                        estudiante = estudiante + 1
                        break
                      case 'docente':
                        docente = docente + 1
                        break
                      case 'administrativo':
                        administrativo = administrativo + 1
                        break
                      case 'obrero':
                        obrero = obrero + 1
                        break
                    }
                }
                _vm.chartData.push(['Estudiantes', estudiante])
                _vm.chartData.push(['Profesores',docente])
                _vm.chartData.push(['Administrativos',administrativo])
                _vm.chartData.push(['Obreros',obrero])
            },

            pushChartDataBySex:function(data){

                let _vm = this
                var m = 0;
                var f = 0;

                for (i=0;i<data.length;i++){
                    switch(data[i].sex){
                      case 'm':
                        m = m + 1
                        break
                      case 'f':
                        f = f + 1
                        break
                    }
                }
                _vm.chartDataBySex.push(['Masculino',m])
                _vm.chartDataBySex.push(['Femenino',f])

            },

            /*
              @param(type)="1,2,3,etc"
            */
            changeStatusPillButton:function(type){
                type == 1 ? this.pillButton = 1 : this.pillButton = 2
            },

            getFilterByRowTable:function(){

                var $rows = $('#table tbody tr');

                $('#search').keyup(function() {

                    var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
                        reg = RegExp(val, 'i'),
                        text;

                    $rows.show().filter(function() {
                        text = $(this).text().replace(/\s+/g, ' ');
                        return !reg.test(text);
                    }).hide();
                });

            },

            }
        });
</script>

@stop

@section('content')
<div class="content"  v-cloak>
  <div class="container-fluid" v-if="turn.id">
    <div class="row">
      <div class="col-md-12">
        <div class="m-b-md" style="align-items: center; display: flex;justify-content: center;">
          <span style="color: #636b6f;  font-family: 'Raleway', sans-serif;font-weight: 100;font-size: 42px;">Registro de llegadas</span><br>
        </div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-md-10">
        <span><b>Hora:</b> {{\Carbon\Carbon::now()->toTimeString()}}</span><br/>
        <span><b>Turno: </b> @{{turn.name.toLowerCase()}}</span>
      </div>
      <div class="col-md-2">
        <span><a href="#" data-toggle="modal" data-target="#showGraphModal" style="color: #008bff;">Ver gráfica</a></span>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <div class="card" style="height: 450px;">
          <div class="card-header">
            <input type="text" class="form-control " id="ic" placeholder="Ingresar cedula" minlength="6" maxlength="15" @keyup.enter = "submit();" v-model="ic" onkeypress = 'return ((event.keyCode == 9 || event.keyCode == 8 || (event.charCode >= 48 && event.charCode <= 57)))'>
          </div>
          <div class="card-body ">
            <div class="card card-user" v-if="arrivals.length > 0">
              <div class="card-body">
                <p class="description">
                  <span><b>Nombre:</b></span><span style="text-transform:uppercase"> @{{latest_arrival.first_name}} </span>
                </p>
                <p class="description">
                  <span><b>Apellido:</b></span><span style="text-transform:uppercase"> @{{latest_arrival.last_name}}</span>
                </p>
                <p class="description">
                  <span><b>Sexo:</b></span><span style="text-transform:uppercase"> @{{latest_arrival.sex}}</span>
                </p>
                <p class="description">
                  <span><b>Tipo:</b></span><span style="text-transform:uppercase"> @{{latest_arrival.type}}</span>
                </p>
              </div>
                <div class="author">
                  <a href="#">
                    <img v-if="!photo" class="avatar border-gray" src="../img/descarga.png" >
                    <img v-else class="avatar border-gray" src="../img/face-6.jpg">
                    <input type="checkbox" name="" v-model="photo">
                  </a>
                </div>
            </div>
            <div class="card-body" v-else>
              <p class="text-center" style="font-size:15px;">Ingrese una cédula valida</p>
          </div>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="card" style="height: 450px;overflow-y: auto;" >
          <div class="card-header">

            <input type="text" name="" class="form-control" id="search" @keyup="getFilterByRowTable()" placeholder="Buscar por cedula de identidad, nombre, apellido, tipo y/o hora">

          </div>
          <div class="card-body">
            <div class="table-responsive table-full-width" v-if="!loadingArrival">
              <table class="table table-hover" id="table">
                <thead>
                  <tr>
                      <th> C.I </th>
                      <th> Nombre </th>
                      <th> Apellido </th>
                      <th> Sexo </th>
                      <th> Tipo </th>
                      <th> Hora </th>
                    </tr>
                  </thead>
                  <tbody  >
                    <tr v-for="arrival in arrivals" v-if="arrivals.length > 0">
                      <td style="font-size:12px;">@{{arrival.ic}}</td>
                      <td style="font-size:12px;text-transform:uppercase">@{{arrival.first_name}}</td>
                      <td style="font-size:12px;text-transform:uppercase">@{{arrival.last_name}}</td>
                      <td style="font-size:12px;text-transform:uppercase">@{{arrival.sex}}</td>
                      <td style="font-size:12px;text-transform:uppercase">@{{arrival.type}}</td>
                      <td style="font-size:12px;">@{{momentTime(arrival.created_at)}}</td>
                    </tr>
                </tbody>
              </table>
            </div>
            <div v-else class="loader">
            </div>
          </div>
        </div>
      </div>
    </div><!--END ROW-->
  </div><!--END CONTAINER-FLUID-->
  <div v-else>
      <span style="font-size:15px;">No hay turno habilitado</span>
  </div>
  <div>

  </div>
</div><!--END CONTENT-->

<div class="modal fade" id="showGraphModal" role="dialog">
  <div class="modal-dialog" style="max-width: 80%;">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body" v-if="arrivals.length > 0">
        <div>
          <span style="font-size:20px;"><b style="color: #87CB16;">Gráficas</b></span>
        </div>
        <br/>
        <div class="row">
          <div class="col-sm-6">
            <div class="card" >
              <div class="card-header">
                <span>Gráfica por tipos de comensales</span>
              </div>
              <div class="card-body">
                <pie-chart :data="chartData"></pie-chart>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card" >
              <div class="card-header">
                <span>Gráfica por sexo</span>
              </div>
              <div class="card-body">
                <pie-chart :data="chartDataBySex"></pie-chart>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div v-else class="modal-body">
        <span style="font-size:15px;">No hay registro de llegadas</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection

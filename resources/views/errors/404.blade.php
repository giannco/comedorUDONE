<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />

</head>
<body>
	<div class="content">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div>
					<h1>Error 404</h1>
					<h4> pagina no encontrada</h4>
				</div>
				<img src="../img/images.png">

				<div>
					<a class="btn btn-success" href="{{route('home')}}"role="button">Regresar</a>
				</div>
			</div>
			<div class="col-md-4"></div>
    	</div>
	</div>

</body>
<!-- Scripts -->

<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>

</html>

<form action="#" id="formPurchase" v-cloak>
	<div class="row">
		<div class="col-sm-8">
			<div class="card">
				<div class="card-header">
					<div v-if="!loadingStockForPurchase">
						<label for="pruducta">Producto</label>
						<v-select
							class="style-chooser"
							v-model="qry_product"
							:options="options_stock_for_purchase"
							@input="addItem"
						>
						</v-select>
					</div>
					<div v-else>
						<span>Cargando...</span>
					</div>
				</div>
				<div class="card-body">
					<div  style="height: 250px;overflow-y: auto;">
						<table class="table table-hover" id="table">
								<thead>
									<tr>
										<th> #Item </th>
										<th> Producto </th>
										<th> Um</th>
										<th> Precio </th>
										<th> Cantidad </th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(s, index) in stock">
										<td style="font-size:12px;">@{{index + 1}}</td>
										<td style="font-size:12px;">@{{s.name}}</td>
										<td style="font-size:12px;">@{{s.measurement}}</td>
										<td style="font-size:12px;"><input v-model="s.purchase_price" type="text" @keypress="onlyNumbrs($event)"/></td>
										<td style="font-size:12px;"><input v-model="s.total_quantity" type="text" @keypress="onlyNumbrs($event)"/></td>
										<td style="font-size:12px;"><a href="#" data-toggle="tooltip" title="Editar" @click.prevent="deleteItem(s)" >Eliminar</a></td>
									</tr>
								</tbody>
						</table>
					</div>
				</div>
				<div class="card-footer">
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="card">
				<div class="card-header ">
				</div>
				<div class="card-body">
					<label>Fecha de compra</label>
					<input  type="date" name="purcahse_date" id="purcahse_date" class="form-control" v-model="purchase.purchase_date">
					<br>
					<div v-if="options_proveedor.length > 0">
						<label for="provider">Proveedor</label>
						<v-select
							class="style-chooser"
							v-model="qry_provider"
							:options="options_proveedor"
							@input=""
						>
						</v-select>
					</div>
				</div>
				<div class="card-footer">
				</div>
			</div>
		</div>
	</div>
</form>

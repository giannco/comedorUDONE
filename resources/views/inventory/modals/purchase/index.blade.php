<div class="modal fade" id="showPurchaseModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
        <a href="#"  class="btn btn-success" data-toggle="modal" data-target="#createPurchaseModal" v-on:click="cleanModal();">Registrar compra</a>
      </div>
      <div class="modal-body" v-if="!loadingPurchase">
        <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Compras</b></span>
        </div>
        <br/>
        <div>
          <input type="text" name="findPurchase" class="form-control" v-model="purchase.search" placeholder="Buscar por fecha de compra d/m/a, proveedor o estatus" v-on:keyup.enter="searchData(purchase.search,getPurchases,'loadingPurchase')" style="font-size:12px;text-transform:uppercase">
        </div>
        <br>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> Id de factura </th>
              <th> Fecha de compra</th>
              <th> Proveedor </th>
              <th> Estatus </th>
              <th> Operacion</th>
            </tr>
          </thead>
          <tbody  v-if="purchases.length > 0" >
            <tr v-for="p in purchases">
              <td style="font-size:12px;text-transform:uppercase">@{{p.id}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{momentTime(p.purchase_date)}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{p.provider_name}}</td>
              <td  :class="p.status == 'aprobada' ? 'text-success'  : (p.status == 'en proceso' ? 'text-warning' : 'text-danger')" style="font-size:12px;text-transform:uppercase" >@{{p.status}}</td>
              <td><a @click.prevent = "showDataModal(p,'#editPurchaseModal','purchase')" href="#" data-toggle="tooltip" title="Ver"><i class="fa fa-eye" style="font-size:25px;color: #008bff;"></i></a> </td>
            </tr>
          </tbody>
        </table>
        <div v-if="paginationPurchase.last_page > 0">
          <paginate
                    :page-count="paginationPurchase.last_page"
                    :page-range="3"
                    :click-handler="setPagePurchase"
                    :prev-text="'ant'"
                    :next-text="'sig'"
                    :container-class="'pagination'"
                    :page-class = "'page-item'"
                    :page-link-class = "'page-link'"
                    :prev-class = "'page-item'"
                    :prev-link-class = "'page-link'"
                    :next-class = "'page-item'"
                    :next-link-class = "'page-link'"
                    :initial-page="paginationPurchase.current_page - 1">
          </paginate>
        </div>
      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

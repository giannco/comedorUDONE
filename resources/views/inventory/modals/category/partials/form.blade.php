<form action="#" id="formCategory">
    <div class="row">
        <div class="col-md-6">
            <label for="name">Nombre</label>
            <input required="required" v-model ="category.name" type="text" name="rif" class="form-control" maxlength="20" onkeypress ="return ((event.keyCode == 9 || event.keyCode == 8 || event.charCode == 32 || ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))))" style="text-transform:uppercase">
        </div>
        <div class="col-md-6">
            <label for="address">Descripcion</label>
            <input v-model ="category.description" type="text" name="address" class="form-control" maxlength="50" onkeypress ="return ((event.keyCode == 9 || event.keyCode == 8 || event.charCode == 32 || ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122)) || (event.charCode >= 48 && event.charCode <= 57)))" style="text-transform:uppercase">
        </div>
    </div>
</form>

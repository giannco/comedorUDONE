<div class="modal fade" id="showCategoryModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
        <a href="#"  class="btn btn-success" data-toggle="modal" data-target="#createCategoryModal" v-on:click="cleanModal();">Registrar categoria</a>
      </div>
      <div class="modal-body" v-if="!loadingCategory">
        <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Categorías</b></span>
        </div>
        <br/>
        <input type="text" name="findCategory" class="form-control" v-model="category.search" placeholder="Buscar categoria por nombre" v-on:keyup.enter="searchData(category.search,getCategories,'loadingCategory')" style="font-size:12px;text-transform:uppercase">
        <br/>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> Nombre </th>
              <th> Descripcion </th>
              <th> Operación</th>
            </tr>
          </thead>
          <tbody v-if="categories.length > 0">
            <tr v-for="c in categories">
              <td style="font-size:12px;text-transform:uppercase">@{{c.name}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{c.description}}</td>
              <td><a @click.prevent = "showDataModal(c,'#editCategoryModal','category')" href="#" data-toggle="tooltip" title="Editar"><i class="fa fa-pencil-square-o" style="font-size:25px;color: #008bff;"></i></a> <a v-if="c.deleted_at == null" @click.prevent = "deleteCategory(c.id)" href="#" data-toggle="tooltip" title="Desactivar" style="margin-left: 25px;"><i  class="fa fa-check-square-o" style="font-size:25px;color:#87CB16"></i></a><a v-else @click.prevent = "activateCategory(c.id)" href="#"  data-toggle="tooltip" title="Activar" style="margin-left: 25px;"><i  class="fa fa-square-o" style="font-size:25px;color:#87CB16"></i></a></td>

              <td></td>
            </tr>
          </tbody>
        </table>
        <div v-if="paginationCategory.last_page > 0">
          <paginate
                    :page-count="paginationCategory.last_page"
                    :page-range="3"
                    :click-handler="setPageCategory"
                    :prev-text="'ant'"
                    :next-text="'sig'"
                    :container-class="'pagination'"
                    :page-class = "'page-item'"
                    :page-link-class = "'page-link'"
                    :prev-class = "'page-item'"
                    :prev-link-class = "'page-link'"
                    :next-class = "'page-item'"
                    :next-link-class = "'page-link'"
                    :initial-page="paginationCategory.current_page - 1">
          </paginate>
        </div>
      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

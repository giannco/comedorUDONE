<div class="modal fade" id="showWasteModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
        <a href="#"  class="btn btn-success" data-toggle="modal" data-target="#createWasteModal" v-on:click="cleanModal();">Registrar desperdicio</a>
      </div>
      <div class="modal-body" v-if="!loadingWaste">
        <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Desperdicios</b></span>
        </div>
        <br/>
        <div>
          <input type="text" name="findWaste" class="form-control" v-model="waste.search" placeholder="Buscar" v-on:keyup.enter="searchData(waste.search,getWaste,'loadingWaste')" style="font-size:12px;text-transform:uppercase">
        </div>
        <br/>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> id </th>
              <th> razón </th>
              <th> estatus </th>
              <th> operaciónn </th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="w in wastes">
              <td style="font-size:12px;text-transform:uppercase">@{{w.id}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{w.reason}}</td>
              <td :class="w.status == 'aprobada' ? 'text-success'  : (w.status == 'en proceso' ? 'text-warning' : 'text-danger')" style="font-size:12px;text-transform:uppercase">@{{w.status}}</td>
              <td><a @click.prevent = "showDataModal(w,'#editWasteModal','waste')" href="#" data-toggle="tooltip" title="Ver"><i class="fa fa-eye" style="font-size:25px;color: #008bff;"></i></a> </td>


            </tr>
          </tbody>
        </table>
        <div v-if="paginationWaste.last_page > 0">
          <paginate
                    :page-count="paginationWaste.last_page"
                    :page-range="3"
                    :click-handler="setPageSale"
                    :prev-text="'ant'"
                    :next-text="'sig'"
                    :container-class="'pagination'"
                    :page-class = "'page-item'"
                    :page-link-class = "'page-link'"
                    :prev-class = "'page-item'"
                    :prev-link-class = "'page-link'"
                    :next-class = "'page-item'"
                    :next-link-class = "'page-link'"
                    :initial-page="paginationWaste.current_page - 1">
          </paginate>
        </div>
      </div>
      <div v-else class="loader">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar
        </button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editWasteModal" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-6">
            <span style="font-size:20px;"><b style="color: #87CB16;">Desperdicios</b> / Detalle</span>
          </div>
          <div class="col-sm-6">
            <span class="text-muted"><b> Fecha de desperdicio: </b></span><span>@{{momentTime(waste.waste_date)}}</span>
          </div>
        </div>

        <br/>
        <div class="row">
          <div class="col-sm-6">
            <span class="text-muted"><b> ID compra : </b></span><span>#@{{waste.id}}</span> <br/>
          </div>
          <div class="col-sm-6">
            <span class="text-muted"><b> Razón: </b></span><span>@{{waste.reason}}</span>
          </div>
		</div>
        <br>
        <div class="card">
          <div class="card-header">
          </div>
          <div class="card-body">
          <span style="font-size:15px;"><b style="color: #87CB16;">Productos</b></span>
            <br>
            <div  style="height: 250px;overflow-y: auto;">
              <table class="table table-hover" id="table">
                <thead>
                  <tr>
                    <th> #Item </th>
                    <th> Producto </th>
                     <th> Um</th>
                    <th> Cantidad </th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="(w, index) in waste.products">
                    <td style="font-size:12px;">@{{index + 1}}</td>
                    <td style="font-size:12px;">@{{w.name}}</td>
                    <td style="font-size:12px;">@{{w.measurement}}</td>
                    <td style="font-size:12px;">@{{w.total_quantity}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
				<div class="card-footer">
				</div>
			</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button @click.prevent="updateWaste(waste.id,'cancelada')" type="button" class="btn btn-danger pull-right" style="">Cancelar</button>
        <button @click.prevent="updateWaste(waste.id,'aprobada')" type="button" class="btn btn-success pull-right" style="">Aprobar</button>
      </div>
    </div>
  </div>
</div>

<form action="#" id="formWaste" v-cloak>
	<div class="row">
		<div class="col-sm-8">
			<div class="card">
				<div class="card-header">
					<div v-if="options_stock_for_purchase.length > 0">
						<label for="pruducta">Producto</label>
						<v-select
							class="style-chooser"
							v-model="qry_product"
							:options="options_stock_for_purchase"
							@input="addItem"
						>
						</v-select>
					</div>
					<div v-else>
						<span>Debe añadir productos</span>
					</div>
				</div>
				<div class="card-body">
					<div  style="height: 250px;overflow-y: auto;">
						<table class="table table-hover" id="table">
								<thead>
									<tr>
										<th> #Item </th>
										<th> Producto </th>
										<th> Um</th>
										<th> Cantidad </th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(s, index) in stock">
										<td style="font-size:12px;">@{{index + 1}}</td>
										<td style="font-size:12px;">@{{s.name}}</td>
										<td style="font-size:12px;">@{{s.measurement}}</td>
										<td style="font-size:12px;"><input v-model="s.total_quantity" type="text" @keypress="onlyNumbrs($event)"/></td>
										<td style="font-size:12px;"><a href="#" data-toggle="tooltip" title="Editar" @click.prevent="deleteItem(s)" >Eliminar</a></td>
									</tr>
								</tbody>
						</table>
					</div>
				</div>
				<div class="card-footer">
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="card">
				<div class="card-header ">
				</div>
				<div class="card-body">
					<label>Fecha de desperdicio</label>
					<input  type="date" name="waste_date" id="waste_date" class="form-control" v-model="waste.waste_date">
					<br>
					<div>
						<label for="reason">Razón</label>
						<v-select
							class="style-chooser"
							v-model="qry_reason"
							:options="[{value:'dañado',label:'dañado'},{value:'merma',label:'merma'},{value:'vencido',label:'vencido'}]"
							@input=""
						>
						</v-select>
					</div>
				</div>
				<div class="card-footer">
				</div>
			</div>
		</div>
	</div>
</form>

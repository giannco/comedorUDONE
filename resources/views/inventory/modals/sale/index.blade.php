<div class="modal fade" id="showSaleModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;
        </a>
        <a href="#"  class="btn btn-success" data-toggle="modal" data-target="#createSaleModal" v-on:click="cleanModal();">Registrar salida</a>
      </div>
      <div class="modal-body" v-if="!loadingSale">
        <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Salidas</b></span>
        </div>
        <br/>
        <div>
          <input type="text" name="findSale" class="form-control" v-model="sale.search" placeholder="Buscar por fecha de venta d/m/a, campus o estatus" v-on:keyup.enter="searchData(sale.search,getSales,'loadingSale')" style="font-size:12px;text-transform:uppercase">
        </div>
        <br/>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> Id de venta </th>
              <th> Fecha de venta </th>
              <th> Campus </th>
              <th> Estatus </th>
              <th> Operacion </th>
            </tr>
          </thead>
          <tbody v-if="sales.length > 0">
            <tr v-for="s in sales">
              <td style="font-size:12px;text-transform:uppercase">@{{s.id}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{momentTime(s.sale_date)}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{s.campus_name}}</td>
              <td :class="s.status == 'aprobada' ? 'text-success'  : (s.status == 'en proceso' ? 'text-warning' : 'text-danger')" style="font-size:12px;text-transform:uppercase">@{{s.status}}</td>
              <td><a @click.prevent = "showDataModal(s,'#editSaleModal','sale')" href="#" data-toggle="tooltip" title="Ver"><i class="fa fa-eye" style="font-size:25px;color: #008bff;"></i></a> </td>
            </tr>
          </tbody>
        </table>
        <div v-if="paginationSale.last_page > 0">
          <paginate
                    :page-count="paginationSale.last_page"
                    :page-range="3"
                    :click-handler="setPageSale"
                    :prev-text="'ant'"
                    :next-text="'sig'"
                    :container-class="'pagination'"
                    :page-class = "'page-item'"
                    :page-link-class = "'page-link'"
                    :prev-class = "'page-item'"
                    :prev-link-class = "'page-link'"
                    :next-class = "'page-item'"
                    :next-link-class = "'page-link'"
                    :initial-page="paginationSale.current_page - 1">
          </paginate>
        </div>
      </div>
      <div v-else class="loader">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar
        </button>
      </div>
    </div>
  </div>
</div>

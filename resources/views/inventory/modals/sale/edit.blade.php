<div class="modal fade" id="editSaleModal" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-6">
            <span style="font-size:20px;"><b style="color: #87CB16;">Salidas</b> / Detalle</span>
          </div>
          <div class="col-sm-6">
            <span class="text-muted"><b> Fecha de venta: </b></span><span>@{{momentTime(sale.sale_date)}}</span>
          </div>
        </div>
        <br/>
        <div class="row">
          <div class="col-sm-6">
            <span class="text-muted"><b> ID venta: </b></span><span>#@{{sale.id}}</span> <br/>
          </div>
          <div class="col-sm-6">
            <span class="text-muted"><b> Campus: </b></span><span>@{{sale.campus_name}}</span>
          </div>
        </div>
        <br>
        <div class="card">
          <div class="card-header">
          </div>
          <div class="card-body">
            <span style="font-size:15px;"><b style="color: #87CB16;">Productos</b></span>
            <br>
            <div  style="height: 250px;overflow-y: auto;">
              <table class="table table-hover" id="table">
                <thead>
                  <tr>
                    <th> #Item </th>
                    <th> Producto </th>
                    <th> Medida</th>
                    <th> Precio </th>
                    <th> Cantidad </th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="(s, index) in sale.products">
                    <td style="font-size:12px;">@{{index + 1}}</td>
                    <td style="font-size:12px;">@{{s.name}}</td>
                    <td style="font-size:12px;">@{{s.measurement}}</td>
                    <td style="font-size:12px;">@{{s.sale_price}}</td>
                    <td style="font-size:12px;">@{{s.total_quantity}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
				<div class="card-footer">
				</div>
			</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button @click.prevent="updateSale(sale.id,'cancelada')" type="button" class="btn btn-danger pull-right" style="">Cancelar</button>
        <button @click.prevent="updateSale(sale.id,'aprobada')" type="button" class="btn btn-success pull-right" style="">Aprobar</button>
      </div>
    </div>
  </div>
</div>

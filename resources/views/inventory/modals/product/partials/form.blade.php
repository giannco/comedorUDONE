<form action="#" id="formProduct">
    
    <div class="row">
        <div class="col-md-6">
            <label for="name">Nombre</label>
            <input required="required" v-model ="product.name" type="text" name="name" class="form-control" maxlength="20" @keypress="onlyAlpha($event)" style="text-transform:uppercase">
        </div>
        <div class="col-md-6">
            <label for="category">Categoria</label>
            <select id="category" class="form-control" v-model="product.category_id" required="required" style="text-transform:uppercase">
                <option v-for="category in categories" :value="category.id">@{{category.name}}</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
	        <label for="code">Codigo</label>
            <input required="required" v-model ="product.code" type="text" name="code" class="form-control" maxlength="20" @keypress="onlyAlphaNumeric($event)" style="text-transform:uppercase">
        </div>
        <div class="col-md-6">
            <label>Unidad de medida</label>
            <select name="measurement" v-model="product.measurement" class="form-control">
                <option value="ud">ud.</option>
                <option value="kg">KG</option>
                <option value="l">L</option>
	        </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="description">Stock minimo</label>
            <input v-model ="product.min_total_amount" type="text" name="description" class="form-control" maxlength="7" @keypress="onlyNumbrs($event)">
        </div>
        <div class="col-md-6">
            <label for="stock">Stock</label>
            <input v-model ="product.stock" required="required" type="text" name ="stock" class="form-control" maxlength="7" @keypress="onlyNumbrs($event)">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="purchase_price">Precio compra</label>
            <input v-model ="product.purchase_price"  required="required" type="text" name="purchase_price" class="form-control" @keypress="onlyDouble($event,product.purchase_price)">
        </div>
        <div class="col-md-6">
            <label>Precio venta</label>
            <input v-model ="product.sale_price" required="required" type="text" name="sale_price" class="form-control" @keypress="onlyDouble($event,product.sale_price)">
        </div>
    </div>
    <br>
</form>

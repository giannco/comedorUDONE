<div class="modal fade" id="showProductModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
        <a href="#"  class="btn btn-success" data-toggle="modal" data-target="#createProductModal" v-on:click="cleanModal();">Registrar producto</a>
      </div>
      <div class="modal-body" v-if="!loadingProduct">
        <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Productos</b></span>
        </div>
        <br/>

        <input type="text" name="findProduct" class="form-control" v-model="product.search" placeholder="Buscar producto por code, nombre o categoria" v-on:keyup.enter="searchData(product.search,getProducts,'loadingProduct')" style="font-size:12px;text-transform:uppercase">
        <br>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> Codigo </th>
              <th> Nombre</th>
              <th> Stock minimo </th>
              <th> Stock </th>
              <th> Operación</th>
            </tr>
          </thead>
          <tbody v-if="products.length > 0">
            <tr v-for="p in products">
              <td style="font-size:12px;text-transform:uppercase">@{{p.code}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{p.name}}</td>
              <td :class="p.stock < p.min_total_amount ? 'bg-warning'  : 'bg-white'" style="font-size:12px;text-transform:uppercase">@{{p.min_total_amount}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{p.stock}} </td>
              <td><a @click.prevent = "showDataModal(p,'#editProductModal','product')" href="#" data-toggle="tooltip" title="Editar"><i class="fa fa-pencil-square-o" style="font-size:25px;color: #008bff;"></i></a> <a v-if="p.deleted_at == null" @click.prevent = "deleteProduct(p.id)" href="#" data-toggle="tooltip" title="Desactivar" style="margin-left: 25px;"><i  class="fa fa-check-square-o" style="font-size:25px;color:#87CB16"></i></a><a v-else @click.prevent = "activateProduct(p.id)" href="#"  data-toggle="tooltip" title="Activar" style="margin-left: 25px;"><i  class="fa fa-square-o" style="font-size:25px;color:#87CB16"></i></a></td>
            </tr>
          </tbody>
        </table>
        <div v-if="paginationProduct.last_page > 0">
          <paginate
                    :page-count="paginationProduct.last_page"
                    :page-range="3"
                    :click-handler="setPageProduct"
                    :prev-text="'ant'"
                    :next-text="'sig'"
                    :container-class="'pagination'"
                    :page-class = "'page-item'"
                    :page-link-class = "'page-link'"
                    :prev-class = "'page-item'"
                    :prev-link-class = "'page-link'"
                    :next-class = "'page-item'"
                    :next-link-class = "'page-link'"
                    :initial-page="paginationProduct.current_page - 1">
          </paginate>
        </div>
      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

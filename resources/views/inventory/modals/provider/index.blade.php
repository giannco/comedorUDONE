<div class="modal fade" id="showProviderModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
        <a href="#"  class="btn btn-success" data-toggle="modal" data-target="#createProviderModal" v-on:click="cleanModal();">Registrar proveedor</a>
      </div>

      <div class="modal-body" v-if="!loadingProvider">
        <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Proveedores</b></span>
        </div>
        <br/>
        <input type="text" name="findProvider" class="form-control" v-model="provider.search" placeholder="Buscar proveedor por RIF o razón social" v-on:keyup.enter="searchData(provider.search,getProviders,'loadingProvider')" style="font-size:12px;text-transform:uppercase">
        <br/>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> Rif </th>
              <th> Razón Social </th>
              <th> Direccion </th>
              <th> Telefono</th>
              <th> Operación</th>
            </tr>
          </thead>
          <tbody v-if="providers.length > 0"  >
            <tr v-for="p in providers">
              <td style="font-size:12px;text-transform:uppercase">@{{p.rif}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{p.name}}</td>
              <td style="font-size:12px;">@{{p.address}}</td>
              <td style="font-size:12px;">@{{p.phone}}</td>
              <td><a @click.prevent = "showDataModal(p,'#editProviderModal','provider')" href="#" data-toggle="tooltip" title="Editar"><i class="fa fa-pencil-square-o" style="font-size:25px;color: #008bff;"></i></a> <a v-if="p.deleted_at == null" @click.prevent = "deleteProvider(p.id)" href="#" data-toggle="tooltip" title="Desactivar" style="margin-left: 25px;"><i  class="fa fa-check-square-o" style="font-size:25px;color:#87CB16"></i></a><a v-else @click.prevent = "activateProvider(p.id)" href="#"  data-toggle="tooltip" title="Activar" style="margin-left: 25px;"><i  class="fa fa-square-o" style="font-size:25px;color:#87CB16"></i></a></td>
            </tr>
          </tbody>
        </table>
        <div v-if="paginationProvider.last_page > 0">
          <paginate
                    :page-count="paginationProvider.last_page"
                    :page-range="3"
                    :click-handler="setPageProvider"
                    :prev-text="'ant'"
                    :next-text="'sig'"
                    :container-class="'pagination'"
                    :page-class = "'page-item'"
                    :page-link-class = "'page-link'"
                    :prev-class = "'page-item'"
                    :prev-link-class = "'page-link'"
                    :next-class = "'page-item'"
                    :next-link-class = "'page-link'"
                    :initial-page="paginationProvider.current_page - 1">
          </paginate>
        </div>
      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<form action="#" id="formProvider">
	<div class="row">
		<div class="col-md-6">
			<label for="name">RIF</label>
			<div class="input-group">
				<span class="input-group-addon">
					<select v-model="provider.type" style="border:none;">
						<option value="j">J</option>
						<option value="g">G</option>
						<option value="v">V</option>
					</select>
				</span>
				<input required="required" v-model ="provider.rif" type="text" name="rif" class="form-control" maxlength="20" @keypress="onlyNumbrs($event)" style="text-transform:uppercase">
			</div>
		</div>
		<div class="col-md-6">
			<label for="name">Razón social</label>
			<input required="required" v-model ="provider.name" type="text" name="name" class="form-control" maxlength="20" @keypress="onlyAlphaNumeric($event)" style="text-transform:uppercase">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">
			<label for="name">Teléfono</label>
			<input required="required" v-model ="provider.phone" type="text" name="phone" class="form-control" maxlength="11" @keypress="onlyNumbrs($event)" style="text-transform:uppercase">
		</div>
		<div class="col-md-6">
			<label for="address">Dirección</label>
			<input v-model ="provider.address" type="text" name="address" class="form-control" maxlength="50" @keypress="onlyAlphaNumeric($event)" style="text-transform:uppercase">
		</div>
	</div>
</form>

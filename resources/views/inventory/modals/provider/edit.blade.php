<div class="modal fade" id="editProviderModal" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body">
        <div>
          <span style="font-size:20px;"><b style="color: #87CB16;">Proveedores</b> / Editar</span>
        </div>
        <br/>
        @include('inventory.modals.provider.partials.form') 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button @click.prevent="updateProvider()" type="button" class="btn btn-primary pull-right" style="">Guardar</button>
      </div>
    </div>
  </div>
</div>
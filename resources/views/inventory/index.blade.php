@extends('layouts.app')
@section('css')
<style>

  .loader {
    border: 16px solid #f3f3f3;
    /* Light grey */
    border-top: 16px solid #3498db;
    /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
    margin-left: 44%;
  }
  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }

  .style-chooser .vs__search::placeholder,
  .style-chooser .vs__dropdown-toggle,
  .style-chooser .vs__dropdown-menu {
    background: #dfe5fb;
    border: none;
    color: #394066;
    text-transform: lowercase;
    font-variant: small-caps;
  }

  .style-chooser .vs__clear,
  .style-chooser .vs__open-indicator {
    fill: #394066;
  }

</style>
@endsection
@section('js')
<script src="js/vuejs-paginate.js">
</script>
<script>
  //Configuracion del lenguaje de la funcion momentTime
  moment.updateLocale('es', {
    months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
    monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
    weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
    weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
    weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
  });
</script>
<script>
  //$('#myModal').on('hidden.bs.modal', function () {});

</script>
<script>
  Vue.component('v-select', VueSelect.VueSelect);
  var vm = new Vue({
    el: '.content',
    data: {
      providers: [],
      provider:{
        'id' : '',
        'rif' : '',
        'name' : '',
        'address' : '',
        'phone' : '',
        'search':'',
        'type':'',
      },
      paginationProvider : {},
      loadingProvider : false,

      categories: [],
      category:{
        'id' : '',
        'name' : '',
        'description' : '',
        'search':'',
        'paginate':1,
      },
      paginationCategory: {},
      loadingCategory : false,
      products: [],
      product:{
        'id' : '',
        'code' : '',
        'name' : '',
        'measurement' : 'ud',
        'sale_price' : '',
        'purchase_price' :'',
        'total_quantity' : '',
        'min_total_amount' : '',
        'category_id' : '',
        'search':'',
        'paginate':1,
      },
      paginationProduct: {},
      loadingProduct : false,
      purchases: [],
      purchase:{
        'id' : '',
        'price_unit' : 0,
        'total_cost' : 0,
        'provider_name' : '',
        'search': '',
        'purchase_date' : '',
      },
      paginationPurchase: {},
      loadingPurchase : false,

      sales: [],
      sale:{
        'id' : '',
        'measurement' : 'ud',
        'units' : 0,
        'price_unit' : 0,
        'purchased_units' : 0,
        'total_cost' : 0,
        'description' : '',
        'product' : {
          'label':'',
          'value':''
        },
        'campus_id':'',
        'search': '',
        'sale_date' : '',
      },
      paginationSale: {},
      loadingSale: false,

      wastes: [],
      waste:{
        'search': '',
        'sale_date' : '',
      },
      paginationWaste: {},
      loadingWaste: false,


      current_product:{
        'id':'',
        'created_at':'',
        'units':'0'
      },
      qry_product: '',
      qry_provider: '',
      qry_reason: '',
      stock: [],
      options_stock_for_sale:[],
      options_stock_for_purchase:[],
      options_proveedor:[],
      campus: [],
      options_campus: [],
      qry_campus: '',
      pagination: {},
      errors : [],
      dataModal:[],
      loading : true,
      loadingStockForPurchase : true,
      loadingStockForSale : true,
    },
    created: function () {
      this.getProviders()
      this.getCategories()
      this.getProducts()
      this.getPurchases()
      this.getSales()
      //this.getStock()
      this.getStockForSale()
      this.getStockForPurchase()
      this.getCampus()
      this.getWaste()
    },
    mount: function(){
    },
    methods: {
      //-------------UTIL-------------
      momentTime: function(time) {
        return moment(time).format('DD/MM/YYYY')
      },
      onlyNumbrs: function(event){
        if (event.charCode < 48 || event.charCode > 57 || event.keyCode == 9 || event.keyCode == 8 || event.charCode == 32){
          return event.preventDefault()
        }
      },
      onlyAlphaNumeric: function(event){
        if ((event.charCode != 209 && event.charCode != 241) && (event.charCode < 65 || event.charCode > 90) && (event.charCode < 97 || event.charCode > 122) && (event.charCode < 48 || event.charCode > 57)){
          return event.preventDefault()//Cancela
        }
      },
      onlyAlpha: function(event){
        if ((event.charCode != 209 && event.charCode != 241) && (event.charCode < 65 || event.charCode > 90) && (event.charCode < 97 || event.charCode > 122)){
          return event.preventDefault()//Cancela
        }
      },
      onlyDouble: function(event,str){

        if ((event.charCode != 46 || str.indexOf('.') != -1) && (event.charCode < 48 || event.charCode > 57)) {
          return  event.preventDefault()
        }
      },

      cleanModal: function(id,object){

        this.provider = {
          'id' : '',
          'rif' : '',
          'name' : '',
          'address' : '',
          'phone' : '',
          'search':'',
          'type':'',
        }

        this.category = {
          'id' : '',
          'name' : '',
          'description' : '',
          'search':'',
          'paginate':1,
        }

        this.stock = []

        this.product = {
          'id' : '',
          'code' : '',
          'name' : '',
          'measurement' : 'ud',
          'sale_price' : '',
          'purchase_price' :'',
          'total_quantity' : '',
          'min_total_amount' : '',
          'category_id' : '',
          'search':'',
          'paginate':1,
        }

        this.sale = {
          'id' : '',
          'measurement' : 'ud',
          'units' : 0,
          'price_unit' : 0,
          'purchased_units' : 0,
          'total_cost' : 0,
          'description' : '',
          'product' : {
            'label':'',
            'value':''
          },
          'campus_id':'',
          'search': '',
          'sale_date' : '',
        }

        this.purchase = {
          'id' : '',
          'price_unit' : 0,
          'total_cost' : 0,
          'provider_name' : '',
          'search': '',
          'purchase_date' : '',
        }

        this.waste = {
          'id': '',
          'search': '',
          'sale_date' : '',
        }

      },

      /**
        @objName : string,
        @obj : object,
        @uri : string,
        @page : int,
        @search : string
      */
      getData: function(objName,obj,uri,page,search,paginateName,loadingName){

        if (page != "" && page != undefined){
          uri = uri + "?page=" + page
        }
        if (uri.indexOf("?") != -1){
          uri = uri + "&paginate=" + obj.paginate
        }else{
          uri = uri + "?paginate=" + obj.paginate
        }
        if (search != "" && search != undefined){
          uri = uri + "&search=" + search
        }

        axios({
          method: 'get',
          url: uri,
        })
        .then(function (response) {
          //success
          Vue.set(vm,objName,response.data.data)
          Vue.set(vm,paginateName,response.data.meta)

          //Carga select de proveedores
          if (objName == "providers"){
            vm.options_proveedor = [];

            vm.providers.forEach(function(currentValue, index, array){
              vm.options_proveedor.push({"label":currentValue.name,"value":currentValue.id})
            })
          }
        })
          .catch(function (error) {
          //error
          console.log('error',error)
        })
          .then(function () {
          //completado

            Vue.set(vm,loadingName, false)
        })
      },

      /**
        @uri : string,
        @obj : object,
        @params : object,
        @id : string,
        @method : string,
      */
      storeOrUpdateData: function(uri,params,obj,id,method,msg){//storeOrUpdateData

        axios({
          method: method,
          url: uri ,
          data:params
        })
        .then(function (response) {
          //success
          $.notify(msg,{
            position:'top center',className:'success',arrowShow: false})

          $(id).modal('hide')
        })
        .catch(function (error) {
          //error
          //console.log('error',error)
          let msg = ''
          //Muestra los msj de error dependiendo de la propiedad obtenida

          msg = Object.values(error.response.data.errors)[0][0]

          $.notify(msg,{
            position:'top center',className:'error',arrowShow: false})
        })
        .then(function () {
          //completado
          //Vue.set(obj,'loading', false)

        })
      },

      /**
        @search : string,
        @getData : callback function
        @loadingName : string
      */
      searchData: function(search,getData,loadingName){
        let _vm = this

        Vue.set(vm,loadingName, true)

        getData("",1,search)

      },

      deleteOrActivateData:function(uri,id,method,msg){

        let _vm = this
        let  url = uri

        axios({
          method: method,
          url: url ,
        })
        .then(function (response) {

          $.notify(msg,{
          position:'top center',className:'success',arrowShow: false})
          })
          .catch(function (error) {
            let msg = ''

            console.log('error')

            })
            .then(function () {

              })
      },
      activateData: function(){
      },
      showDataModal: function(data,id,objName){

        let dataObj = {}

        //se hace para obtener los set y get del objeto original
        let keys = Object.keys(data)
        let values = Object.values(data)

        //asigna los key y values al objeto dataObj
        keys.forEach(function(currentValue, index, array) {
          dataObj[currentValue] = values[index]
        });

        Vue.set(vm,objName,dataObj)
        $(id).modal()
      },


      //-----------Proveedores-----------------------
      clearProvider: function(){
        vm.provider = {
          'id' : '',
          'rif' : '',
          'name' : '',
          'address' : '',
          'phone' : '',
          'type':'',
        }
      },
      getProviders: function(data,page,search) {
        let _vm = this

        let uri = "{{route('providers.index')}}"

        _vm.loadingProvider = true

        _vm.getData('providers',this.provider,uri,page,search,'paginationProvider','loadingProvider')
      },
      storeProvider: function() {
        let _vm = this

        let uri = "{{route('providers.store')}}"

        const params = {
          rif : this.provider.rif,
          name : this.provider.name,
          address : this.provider.address,
          phone : this.provider.phone,
          type: this.provider.type,
        }

        _vm.loadingProvider = true

        _vm.storeOrUpdateData(uri,params,this.provider,'#createProviderModal','post','Proveedor agregado correctamente')

        _vm.getProviders()
      },
      updateProvider: function(){

        let _vm = this

        let uri = "{{route('providers.update',['_id'])}}".replace('_id',_vm.provider.id)

        const params = {
          id: this.provider.id,
          rif : this.provider.rif,
          name : this.provider.name,
          address : this.provider.address,
          phone : this.provider.phone,
          type: this.provider.type,
        }

        _vm.storeOrUpdateData(uri,params,this.provider,'#editProviderModal','put','Proveedor actualizado correctamente')

        _vm.getProviders()
      },
      deleteProvider: function(id){

        let _vm = this

        let uri = "{{route('providers.delete','_id')}}".replace('_id',id)

        _vm.loadingProvider = true

        _vm.deleteOrActivateData(uri,id,'delete','Proveedor deshabilitado con exito')

        _vm.getProviders()

      },
      activateProvider: function(id){

        let _vm = this

        let uri = "{{route('providers.activate','_id')}}".replace('_id',id)

        _vm.loadingProvider = true

        _vm.deleteOrActivateData(uri,id,'put','Proveedor habilitado con exito')

        _vm.getProviders()

      },
      setPageProvider:function(page){
        let _vm = this

        _vm.loadingProvider = true

        _vm.getProviders(null,page,null)

      },

      //-----------Categorias-----------------------
      getCategories: function(data,page,search) {
        let _vm = this

        let uri = "{{route('categories.index')}}"

        _vm.loadingCategory = true

        _vm.getData('categories',this.category,uri,page,search,'paginationCategory','loadingCategory')

      },
      storeCategory: function() {
        let _vm = this

        let uri = "{{route('categories.store')}}"

        const params = {
          name : this.category.name,
          description : this.category.description,
        }

        _vm.loadingCategory = true

        _vm.storeOrUpdateData(uri,params,this.category,'#createCategoryModal','post','Categoria agregada correctamente')

        _vm.getCategories()

      },
      updateCategory: function(){

        let _vm = this

        let uri = "{{route('categories.update',['_id'])}}".replace('_id',_vm.category.id)

        const params = {
          id: this.category.id,
          name : this.category.name,
          description : this.category.description,
        }

        _vm.storeOrUpdateData(uri,params,this.category,'#editCategoryModal','put','Categoria actualizada correctamente')

        _vm.getCategories()
      },
      deleteCategory: function(id){

        let _vm = this

        let uri = "{{route('categories.delete','_id')}}".replace('_id',id)

        _vm.loadingCategory = true

        _vm.deleteOrActivateData(uri,id,'delete','Categoria deshabilitada con exito')

        _vm.getCategories()

      },
      activateCategory: function(id){

        let _vm = this

        let uri = "{{route('categories.activate','_id')}}".replace('_id',id)

        _vm.loadingCategory = true

        _vm.deleteOrActivateData(uri,id,'put','Categoria habilitada con exito')

        _vm.getCategories()

      },
      setPageCategory:function(page){

        let _vm = this

        _vm.loadingCategory = true

        _vm.getCategories(null,page,null)
      },

      //-----------Products-----------------------
      getProducts: function(data,page,search) {
        let _vm = this

        let uri = "{{route('products.index')}}"

        _vm.loadingProduct = true

        _vm.getData('products',this.product,uri,page,search,'paginationProduct','loadingProduct')

        _vm.getStockForSale()
        _vm.getStockForPurchase()

      },
      storeProduct: function() {
        let _vm = this

        let uri = "{{route('products.store')}}"

        const params = {
          name: this.product.name,
          code : this.product.code,
          min_total_amount : this.product.min_total_amount,
          category_id : this.product.category_id,
          measurement : this.product.measurement,
          sale_price : this.product.sale_price,
          purchase_price : this.product.purchase_price,
          total_quantity : this.product.total_quantity,
        }

        _vm.loadingProduct = true

        _vm.storeOrUpdateData(uri,params,this.product,'#createProductModal','post','Producto agregado correctamente')

        _vm.getProducts()

      },
      updateProduct: function(){

        let _vm = this

        let uri = "{{route('products.update',['_id'])}}".replace('_id',_vm.product.id)

        const params = {
          name: this.product.name,
          code : this.product.code,
          min_total_amount : this.product.min_total_amount,
          category_id : this.product.category_id,
          measurement : this.product.measurement,
          sale_price : this.product.sale_price,
          purchase_price : this.product.purchase_price,
          total_quantity : this.product.stock,
        }

        _vm.storeOrUpdateData(uri,params,this.product,'#editProductModal','put','Producto actualizado correctamente')

        _vm.getProducts()

      },
      deleteProduct: function(id){

        let _vm = this

        let uri = "{{route('products.delete','_id')}}".replace('_id',id)

        _vm.loadingProduct = true

        _vm.deleteOrActivateData(uri,id,'delete','Producto deshabilitado con exito')

        _vm.getProducts()


      },
      activateProduct: function(id){

        let _vm = this

        let uri = "{{route('products.activate','_id')}}".replace('_id',id)

        _vm.loadingProduct = true

        _vm.deleteOrActivateData(uri,id,'put','Producto habilitado con exito')

        _vm.getProducts()

      },
      setPageProduct:function(page){
        let _vm = this

        _vm.loadingProduct = true

        _vm.getProducts(null,page,null)
      },

      //-----------Compras-------------
      addItem: function() {

        var query = ""
        var result = [{'total_quantity' : 0}]
        let _vm = this


        if (this.qry_product !== null){

          query = this.qry_product.value

          this.products.forEach(function(product){

            if (product.id == query){
              result[0].id =  product.id
              result[0].code = product.code
              result[0].name = product.name
              result[0].measurement = product.measurement
              result[0].purchase_price = product.purchase_price
              result[0].sale_price = product.sale_price
              result[0].total_quantity = 1

              _vm.stock.push(result[0])
            }

          })

          //result = this.products.filter(product => product.id == query)

          //result[0].total_quantity = 1

          //this.stock.push(result[0])

        }

      },
      deleteItem: function(item){

        for( var i = 0; i < this.stock.length; i++){
          if (this.stock[i].id === item.id) {
            this.stock.splice(i, 1);
          }
        }

        //this.stock.splice( this.stock.indexOf(item.id), 1 );
      },

      clearPurchaseOrSale: function(){

          vm.stock = []

      },

      getPurchases: function(data,page,search) {
        let _vm = this

        let uri = "{{route('purchases.index')}}"

        _vm.loadingPurchase = true

        _vm.getData('purchases',this.purchase,uri,page,search,'paginationPurchase','loadingPurchase')

      },
      storePurchase: function() {
        let _vm = this

        let uri = "{{route('purchases.store')}}"

        const params = {
          products : this.stock,
          provider_id: this.qry_provider.value,
          purchase_date: this.purchase.purchase_date,
        }

        _vm.loadingPurchase = true

        _vm.storeOrUpdateData(uri,params,this.purchase,'#createPurchaseModal','post','Compra agregada correctamente')

        //Inicializa el select de stock
        //_vm.options_stock = []
        _vm.options_stock_for_purchase = []

        //_vm.getStock()
        _vm.getStockForPurchase()
        _vm.getPurchases()
      },
      updatePurchase: function(id,status){

        let _vm = this

        let uri = "{{route('purchases.update',['_id'])}}".replace('_id',_vm.purchase.id)

        const params = {
          status: status,
        }

        _vm.loadingPurchase = true

        _vm.storeOrUpdateData(uri,params,this.purchase,'#editPurchaseModal','put','Compra actualizada correctamente')

        //_vm.getStock()
        _vm.getStockForPurchase()
        _vm.getStockForSale()
        _vm.getPurchases()

      },
      deletePurchase: function(){
      },
      activatePurchase: function(id){
      },
      setPagePurchase:function(page){
        let _vm = this

        _vm.loadingPurchase = true

        _vm.getPurchases(null,page,null)
      },

      getStockForPurchase:function(data,page){

        let _vm = this
        let uri = "{{route('products.index')}}"

        _vm.loadingStockForPurchase = true

        this.options_stock_for_purchase = []

        _vm.getStock(data,page,uri,this.options_stock_for_purchase,'loadingStockForPurchase')

      },

      //-----------Salidas-ventas-------------
      getSales: function(data,page,search) {
        let _vm = this

        let uri = "{{route('sales.index')}}"

        _vm.loadingSale = true

        _vm.getData('sales',this.sale,uri,page,search,'paginationSale','loadingSale')

      },
      storeSale: function() {

        let _vm = this

        let uri = "{{route('sales.store')}}"

        const params = {
          products : this.stock,
          campus_id : this.qry_campus.value,
          sale_date : this.sale.sale_date,
        }

        _vm.loadingSale = true

        _vm.storeOrUpdateData(uri,params,this.sale,'#createSaleModal','post','Venta agregada correctamente')

        //_vm.getStock()
        _vm.getStockForSale()
        _vm.getSales()

      },
      updateSale: function(id,status){

        let _vm = this

        let uri = "{{route('sales.update',['_id'])}}".replace('_id',_vm.sale.id)

        const params = {
          status: status,
        }

        _vm.loadingSale = true

        _vm.storeOrUpdateData(uri,params,this.sale,'#editSaleModal','put','Venta actualizada correctamente')

        //_vm.getStock()
        _vm.getStockForSale()
        _vm.getStockForPurchase()
        _vm.getSales()
      },
      deleteSale: function(){
      },
      activateSale: function(id){
      },
      showSaleModal: function(){
      },
      setPageSale:function(page){
        let _vm = this

        _vm.loadingSale = true

        _vm.getSales(null,page,null)
      },

      getStockForSale: function(data,page){
        let _vm = this

        let uri = "{{route('products.index')}}" + '?sale=1'

        _vm.loadingStockForSale = true

        this.options_stock_for_sale = []

        _vm.getStock(data,page,uri,this.options_stock_for_sale,'loadingStockForSale')

      },

      //-----------Desperdicios----------

      getWaste: function(data,page,search) {
        let _vm = this

        let uri = "{{route('waste.index')}}"

        _vm.loadingWaste = true

        _vm.getData('wastes',this.waste,uri,page,search,'paginationWaste','loadingWaste')

      },

      storeWaste: function() {

        let _vm = this

        let uri = "{{route('waste.store')}}"

        const params = {
          products : this.stock,
          reason : this.qry_reason.value,
          waste_date : this.waste.waste_date,
        }

        _vm.loadingWaste = true

        _vm.storeOrUpdateData(uri,params,this.waste,'#createWasteModal','post','Derperdicio agregado correctamente')

        //_vm.getStock()
        _vm.getStockForSale()
        _vm.getWaste()

        },
        updateWaste: function(id,status){

          let _vm = this

          let uri = "{{route('waste.update',['_id'])}}".replace('_id',_vm.waste.id)

          const params = {
            status: status,
          }

          _vm.loadingWaste = true

          _vm.storeOrUpdateData(uri,params,this.waste,'#editWasteModal','put','Desperdicio actualizado correctamente')


          _vm.getWaste()

        },


      //-----------Stock-------------
      getStock: function(data,page,uri,options_stock,loading) {

        axios({
          method: 'get',
          url: uri,
        })
          .then(function (response) {
          //success

          response.data.data.forEach(function(currentValue, index, array){

            let label = ""

            label = currentValue.name

            if (currentValue != null ){
              options_stock[index] = {

                "label": label.toUpperCase() ,"value":currentValue.id
              }

            }

          })

        })
          .catch(function (error) {
          //error
          console.log('error',error)
        })
          .then(function () {
            //completado
            Vue.set(vm,loading, false)
        })
      },
      getCampus: function(){
        let _vm = this

        axios({
          method: 'get',
          url: "{{route('campus.index')}}" ,
        })
          .then(function (response) {
          //success
            //console.log(response.data)
            _vm.campus = response.data

            _vm.campus.forEach(function(currentValue, index, array){
              if (currentValue != null ){
                vm.options_campus[index] = {
                  "label": currentValue.name.toUpperCase() ,"value":currentValue.id
                }
              }
            })
          })
          .catch(function (error) {
            //error
            console.log('error',error)
          })
          .then(function () {
            //completado
          })
        },

    },//End methods
    components: {
      paginate: VuejsPaginate
    },
    computed: {
      // a computed getter
      getCostoTotal: function () {
        // `this` points to the vm instance
        return parseFloat(this.purchase.units) * parseFloat(this.purchase.price_unit)
      },
      getMomentTime: function(){
        return moment(this.current_product.created_at).format('LLLL')
      }
    },
  });
</script>
@endsection
@section('content')
<div class="content"  v-cloak>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="m-b-md" style="align-items: center; display: flex;justify-content: center;">
          <span style="color: #636b6f;  font-family: 'Raleway', sans-serif;font-weight: 100;font-size: 42px;">Inventario
          </span>
          <br>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      @if(auth()->user()->can('providers.index'))
      <a href="#" data-toggle="modal" data-target="#showProviderModal" v-on:click="cleanModal();getProviders()">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-group" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Proveedores
            </span>
          </center>
        </div>
      </a>
      @endif
      @if(auth()->user()->can('categories.index'))
      <a href="#" data-toggle="modal" data-target="#showCategoryModal"  v-on:click="cleanModal();category.paginate=1; getCategories()">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-cutlery" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Categorías
            </span>
          </center>
        </div>
      </a>
      @endif
      @if(auth()->user()->can('products.index'))
      <a href="#" data-toggle="modal" data-target="#showProductModal" v-on:click="cleanModal();product.paginate=1; category.paginate=0; getProducts() ; getCategories()">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-cutlery" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Productos
            </span>
          </center>
        </div>
      </a>
      @endif
      @if(auth()->user()->can('purchases.index'))
      <a href="#" data-toggle="modal" data-target="#showPurchaseModal" v-on:click="cleanModal();product.paginate=0; getPurchases(); getProducts()">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-cutlery" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Compras
            </span>
          </center>
        </div>
      </a>
      @endif
      @if(auth()->user()->can('sales.index'))
      <a href="#" data-toggle="modal" data-target="#showSaleModal"  v-on:click="cleanModal();getSales()">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-cutlery" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Salidas
            </span>
          </center>
        </div>
      </a>
      @endif
      @if(auth()->user()->can('waste.index'))
      <a href="#" data-toggle="modal" data-target="#showWasteModal"  v-on:click="cleanModal();getWaste()">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-cutlery" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Desperdicios
            </span>
          </center>
        </div>
      </a>
      @endif
      @include('inventory.modals.provider.index')
      @include('inventory.modals.provider.create')
      @include('inventory.modals.provider.edit')

      @include('inventory.modals.category.index')
      @include('inventory.modals.category.create')
      @include('inventory.modals.category.edit')

      @include('inventory.modals.product.index')
      @include('inventory.modals.product.create')
      @include('inventory.modals.product.edit')

      @include('inventory.modals.purchase.index')
      @include('inventory.modals.purchase.create')
      @include('inventory.modals.purchase.edit')

      @include('inventory.modals.sale.index')
      @include('inventory.modals.sale.create')
      @include('inventory.modals.sale.edit')

      @include('inventory.modals.waste.index')
      @include('inventory.modals.waste.create')
      @include('inventory.modals.waste.edit')

    </div>
    <!--END ROW-->
  </div>
  <!--END CONTAINER-FLUID-->
</div>
@endsection

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">

    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
   <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />-->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" />
    <!-- CSS Files -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/light-bootstrap-dashboard.css?v=2.0.1')}}" rel="stylesheet" />
     <link href="{{asset('css/vue-select.css')}}" rel="stylesheet" />
    <style>
        [v-cloak] {
            display: none
        }
        .link-box{
            background: linear-gradient(to bottom, #5686b7 0%, #4a6cea 100%);
            width: 250px;
            border: 10px solid #FFFF;
            padding: 25px;
            margin: 25px;
        }
        .box-icon{
        }
        .link-box:hover {
            /*filter: saturate(180%);*/
            -webkit-transform:scale(1.3);
            transform:scale(1.3);
            box-shadow: 5px grey;
        }
        .close {
            font-size: 1.5rem
        }
        .modal-header {
            border-bottom : none;
        }
        a:focus {
            outline: none;
        }
        .pagination {
            justify-content: center;
        }
    </style>
    @yield('css')

</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="blue" data-image="{{asset('img/sidebar-5.jpg')}}">
        <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"
            Tip 2: you can also add an image using data-image tag
        -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="#" class="simple-text">
                        <img style="position: relative; max-width: auto; max-height: 100px; min-width: auto; min-height: auto; width: auto; height: auto; top: 50px!important; transform: translateY(-50%)!important; margin: auto!important" src="../img/logo.png" alt="">
                    </a>
                </div>
                <ul class="nav">
                    @if(auth()->user()->can('home'))
                    <li class="nav-item" >
                        <a class="nav-link" href="{{route('home')}}">
                             <i class="fa fa-id-card-o" aria-hidden="true"></i>
                            <p>Home</p>
                        </a>
                    </li>
                    @endif
                    @if(auth()->user()->can('control-access'))
                    <li>
                        <a class="nav-link" href="{{route('control-access')}}">
                            <i class="fa fa-barcode" aria-hidden="true"></i>
                            <p>Control de acceso</p>
                        </a>
                    </li>
                    @endif
                    @if(auth()->user()->can('inventory'))
                    <li>
                        <a class="nav-link" href="{{route('inventory')}}">
                            <i class="fa fa-cubes" aria-hidden="true"></i>
                            <p>Inventario</p>
                        </a>
                    </li>
                    @endif
                    @if(auth()->user()->can('reports'))
                    <li>
                        <a class="nav-link" href="{{route('reports')}}">
                            <i class="fa fa-print" aria-hidden="true"></i>
                            <p>Reportes</p>
                        </a>
                    </li>
                    @endif
                    @role('admin')
                    <li>
                        <a class="nav-link" href="{{route('configuration')}}">
                            <i class="fa fa-gears" aria-hidden="true"></i>
                            <p>Configuración</p>
                        </a>
                    </li>
                    @endrole
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <a class="navbar-brand" href="#"></a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">

                        <ul style="margin-right: 60px;" class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a  class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <span class="no-icon">{{Auth::user()->name}}</span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <!--<a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="divider"></div>-->

                                    <a class="dropdown-item" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <span class="no-icon d-lg-block">&nbsp;Cerrar Sesion</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="content"  v-cloak>
                @isset($error)
                        You are not allowed to access this resource!
                @endisset
                @yield('content')
            </div>
            <footer class="footer">
                <div class="container">
                    <nav>
                        <p class="copyright text-center">
                            ©
                            <script>
                                document.write(new Date().getFullYear())

                            </script>
                            <a href="#"><strong>Created by:</strong> Gian Franco Marcano & Alcides Marcano </a>
                        </p>
                    </nav>
                </div>
            </footer>
        </div>
    </div>
</body>

<!-- Scripts -->
<script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{asset('js/tether.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/light-bootstrap-dashboard.js?v=2.0.1')}}" type="text/javascript"></script>
<!--Libreria para manejo de tiempo-->
<script src="{{asset('js/moment.js')}}"></script>
<!-- -->
<!--Libreria para alertas-->
<script src="{{asset('js/notify.js')}}"></script>
<!-- -->
<!--Libreria vuejs -->
<script src="{{asset('js/vue.js')}}"></script>

<script src="{{asset('js/vue-select.js')}}"></script>

<!---->
<!--Librerias para las graficas con vuejs -->
<script src="{{asset('js/chart.bundle.js')}}"></script>
<script src="{{asset('js/chartkick.js')}}"></script>
<script src="{{asset('js/vue-chartkick.js')}}"></script>
<!-- -->
<!--Mantener activa la ruta del sidebar-->
<script type="text/javascript">
    $(document).ready(function () {

        var url = window.location;

        $('ul a').filter(function() {

            return this.href == url;

        }).parent().addClass('active');

    // for treeview
        $('ul.treeview-menu a').filter(function() {
            return this.href == url;

        }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
    });
</script>
<!---->
@yield('js')

</html>

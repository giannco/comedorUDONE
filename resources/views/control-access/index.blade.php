@extends('layouts.app')
@section('css')
<style>

  .loader {
    border: 16px solid #f3f3f3;
    /* Light grey */
    border-top: 16px solid #3498db;
    /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
    margin-left: 35%;
  }

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }

  .custom-file-label {
    top: 0;
    right: 0;
    left: 0;
    z-index: 1;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    width: 100%;
}

</style>
@endsection
@section('js')
<script src="js/vuejs-paginate.js">
</script>
<script>
  /*Deshabilita los 3 pts suspensivos en el paginator*/
  //$(".page-item.disabled").css({"display":"none"});
  var vm =  new Vue({
    el: '.content',
    data: {
      turns:[],
      turn : {
        'id' : '',
        'name' : '',
        'description':'',
        'start_hour':'',
        'end_hour':'',
        'search': ''
      },
      careers:[],
      career:{
        'id':''
      },
      comensales:[],
      comensal : {
        'ic':'',
        'first_name' : '',
        'last_name':'',
        'photo':'',
        'type_id' : '',
        'sex':'',
        'career_id':'',
        'search': ''

      },
      loadingComensal : true,
      loadingTurn : true,
      paginationTurn: {},
      users:[],
      types:[],
      type:{
        'loading':true
      },
      select_type_id :'',
      pagination: {},
      errors : [],
      dataModal:[],
      msg:'',
      file:'',
      loading : true
  },

  created: function () {
    setTimeout(function () {
      vm.getTurns()
      vm.getComensals()
      vm.getComensalType()
      vm.getCareers()
    }
               ,1000)
  }
  ,
    methods: {
      //-----------Archivos-----------------------
      submitFile: function(){
        let _vm = this
        _vm.loading = true
        let formData = new FormData();
        formData.append('file', this.file);
        formData.append('type_id',this.select_type_id);
        axios.post( '/comensales/import',formData,{
          headers: {
            'Content-Type': 'multipart/form-data'
          },
        })
          .then(function(response){
            $.notify('Comensales importado con exito',{
            position:'top center',className:'success',arrowShow: false})

            console.log('SUCCESS!!');
        })
          .catch(function(error){

            msg = Object.values(error.response.data.errors)[0][0]

            $.notify(msg,{
            position:'top center',className:'error',arrowShow: false})

            console.log('FAILURE!!');
        })
          .then(function () {
          //completado
          _vm.loading = false

        })
      },

      handleFileUpload:function(event){
          //console.log("evento aqu",event.target.files[0])
          this.file = event.target.files[0]

			    document.getElementById("filename").innerHTML = event.target.files[0].name
      },

      getData: function(objName,obj,uri,page,search,paginateName,loadingName){

        if (page != "" && page != undefined){
          uri = uri + "?page=" + page
        }

        if (uri.indexOf("?") != -1){
          uri = uri + "&paginate=" + obj.paginate
        }else{
          uri = uri + "?paginate=" + obj.paginate
        }

        if (search != "" && search != undefined){
          uri = uri + "&search=" + search
        }

        axios({
          method: 'get',
          url: uri,
        })
        .then(function (response) {
          //success
          Vue.set(vm,objName,response.data.data)
          Vue.set(vm,paginateName,response.data.meta)

        })
          .catch(function (error) {
          //error
          console.log('error',error)
        })
          .then(function () {
          //completado

            Vue.set(vm,loadingName, false)
        })
      },

      searchData: function(search,getData,loadingName){
        let _vm = this

        Vue.set(vm,loadingName, true)

        getData("",1,search)

      },

      showDataModal: function(data,id,objName){

        let dataObj = {}

        //se hace para obtener los set y get del objeto original
        let keys = Object.keys(data)
        let values = Object.values(data)

        //asigna los key y values al objeto dataObj
        keys.forEach(function(currentValue, index, array) {
          dataObj[currentValue] = values[index]
        });

        Vue.set(vm,objName,dataObj)
        $(id).modal()
      },

      storeOrUpdateData: function(uri,params,obj,id,method,msg){
        axios({
          method: method,
          url: uri ,
          data:params
        })
        .then(function (response) {
          //success
          $.notify(msg,{
            position:'top center',className:'success',arrowShow: false
          })
          $(id).modal('hide')
        })
        .catch(function (error) {
          //error

          let msg = ''
          //Muestra los msj de error dependiendo de la propiedad obtenida

          msg = Object.values(error.response.data.errors)[0][0]

          $.notify(msg,{
              position:'top center',className:'error',arrowShow: false})
          })
          .then(function () {
            //completado
            //Vue.set(obj,'loading', false)
          })
      },



        //-----------Comensales-----------------------
        getComensals: function(data,page,search) {
          let _vm = this
          let uri = "{{route('comensales.index')}}"

          //page != "" && page != undefined && search != "" && search != undefined
          if (!!page && !!search){
            uri = uri + "?page=1" + "&search=" + search
          }else{
            if (!!page){
              uri = uri + "?page=" + page
            }
            if (!!search){
              uri = uri + "&search=" + search
            }
          }
          //vm.loading = true
          _vm.loadingComensal = true

          _vm.pagination = {}

          axios({
            method: 'get',
            url: uri,
          }
               )
            .then(function (response) {
            //success
            _vm.comensales = response.data.data
            _vm.pagination = response.data.meta
          }
                 )
            .catch(function (error) {
            //error
            console.log('error',error)
          }
                  )
            .then(function () {
            //completado
            //_vm.loading = false
            _vm.loadingComensal = false
          }
                 )
        },
        submitComensal: function() {
          let _vm = this
          let url = "{{route('comensales.store')}}"
          this.errors = []

          //vm.loading = true
          _vm.loadingComensal = true

          axios({
            method: 'post',
            url: url ,
            data:{
              ic : vm.comensal.ic,
              first_name : vm.comensal.first_name,
              last_name : vm.comensal.last_name,
              photo : vm.comensal.photo,
              type_id : vm.comensal.type_id,
              sex : vm.comensal.sex,
              career_id: vm.career.id
            }
          }
               )
            .then(function (response) {
            //success


            vm.getComensals()
            vm.clearComensal()
            $('#createComensalModal').modal('hide');
            $.notify('Comensal registrado con exito',{
              position:'top center',className:'success',arrowShow: false
            })
          })
            .catch(function (error) {
            let msg = ''
            //Muestra los msj de error dependiendo de la propiedad obtenida

            msg = Object.values(error.response.data.errors)[0][0]

            $.notify(msg,{
              position:'top center',className:'error',arrowShow: false})
          })
            .then(function () {
            //completado
            // _vm.loading = false
            _vm.loadingComensal = false
          })
        }
      ,
        updateComensal: function() {
          let _vm = this
          let url = "{{route('comensales.update',['_id'])}}".replace('_id',_vm.comensal.ic)

          //vm.loading = true
          _vm.loadingComensal = true

          axios({
            method: 'put',
            url: url ,
            data:{
              ic : vm.comensal.ic,
              first_name : vm.comensal.first_name,
              last_name : vm.comensal.last_name,
              photo : vm.comensal.photo,
              type_id : vm.comensal.type_id,
              sex : vm.comensal.sex,
              career_id: vm.career.id
            }
          }
               )
            .then(function (response) {
            //success

            _vm.getComensals()


            $.notify('Comensal modificado con exito',{
              position:'top center',className:'success',arrowShow: false})

            $('#editComensalModal').modal('hide')

          }
                 )
            .catch(function (error) {
            let msg = ''
            //$.notify(msg,{position:'top center',className:'error',arrowShow: false})
            }
                  )
            .then(function () {
            //completado
            // _vm.loading = false
            _vm.loadingComensal = false
          })
        }
      ,
        showComensalModal: function(data) {
          let _vm = this

          _vm.comensal.ic = data.ic
          _vm.comensal.first_name = data.first_name
          _vm.comensal.last_name = data.last_name
          _vm.comensal.photo = data.photo
          _vm.comensal.sex = data.sex
          _vm.comensal.type_id = data.type_id
          _vm.comensal.career_id = data.career_id

          //_vm.comensal = data
          _vm.career.id = data.career_id


          $("#editComensalModal").modal()
        }
      ,
        deleteComensal:function(id){

          let _vm = this
          let url = "{{route('comensales.delete','_id')}}".replace('_id',id)
          //vm.loading = true
          _vm.loadingComensal = true
          axios({
            method: 'delete',
            url: url ,
          }
               )
            .then(function (response) {
            //success

            _vm.getComensals()
            $.notify('Comensal deshabilitado con exito',{
              position:'top center',className:'success',arrowShow: false})

          }
                 )
            .catch(function (error) {
            let msg = ''
            //$.notify(msg,{position:'top center',className:'error',arrowShow: false})
            }
                  )
            .then(function () {
            //completado
            //_vm.loading = false
            _vm.loadingComensal = false
          }
                 )
        }
      ,
        activateComensal:function(id){

          let _vm = this
          let  url = "{{route('comensales.activate','_id')}}".replace('_id',id)
          //vm.loading = true
          _vm.loadingComensal = true
          axios({
            method: 'put',
            url: url ,
          }
               )
            .then(function (response) {
            //success
            _vm.getComensals()
            $.notify('Comensal habilitado con exito',{
              position:'top center',className:'success',arrowShow: false}
                    )
          }
                 )
            .catch(function (error) {
            let msg = ''
            //$.notify(msg,{position:'top center',className:'error',arrowShow: false})
            }
                  )
            .then(function () {
            //completado
            // _vm.loading = false
            _vm.loadingComensal = false
          }
                 )
        }
      ,
        searchCOmensal:function(){
          //console.log("comensal",this.comensal.search)
          let _vm = this
          //vm.loading = true
          _vm.loadingComensal = true
          _vm.getComensals("",1,_vm.comensal.search)
        }
      ,
        clearComensal: function(){
          vm.comensal = {
            'ic':'',
            'first_name' : '',
            'last_name':'',
            'photo':'',
            'type_id' : '',
            'sex':'',
            'career':''
          }

        }
      ,
        setPage:function(page){
          let _vm = this
          _vm.loadingComensal = true
          _vm.getComensals(null,page,_vm.comensal.search)
        }
      ,
        //-----------Turno-----------------------
        //Limpia el modal
        clearTurn: function() {
          vm.turn = {
            'id' : '',
            'name' : '',
            'description':'',
            'start_hour':'',
            'end_hour':''
          }
        },
        submitTurn : function() {
          let _vm = this

          let uri = "{{route('turns.store')}}"

          const params = {
            name : this.turn.name,
            description : this.turn.description,
            start_hour : this.turn.start_hour,
            end_hour : this.turn.end_hour,
          }

          _vm.loadingTurn = true

          _vm.storeOrUpdateData(uri,params,this.turn,'#createTurnModal','post','Turno registrado con exito')

          _vm.getTurns()

      },

      getTurns: function(data,page,search) {
        let _vm = this

        let uri = "{{route('turns.index')}}"

        _vm.loadingTurn = true

        _vm.getData('turns',this.turn,uri,page,search,'paginationTurn','loadingTurn')

      },

      setPageTurn:function(page){

        let _vm = this

        _vm.loadingTurn = true

        _vm.getTurns(null,page,null)
      },

      showTurnModal: function(data) {
          let _vm = this
          _vm.turn = data
          console.log("data",data)
          $("#editTurnModal").modal()
      },

      updateTurn: function(){

        let _vm = this

        let uri = "{{route('turns.update',['_id'])}}".replace('_id',_vm.turn.id)

        const params = {
          id: this.turn.id,
          name : this.turn.name,
          description : this.turn.description,
          start_hour : this.turn.start_hour,
          end_hour : this.turn.end_hour,

        }

        _vm.storeOrUpdateData(uri,params,this.turn,'#editTurnModal','put','Turno modificado con exito')

        _vm.getTurns()
      },


        deleteTurn:function(id){
          let _vm = this
          vm.loading = true
          $.ajax({
            url: "{{route('turns.delete','_id')}}".replace('_id',id),
            method: 'delete',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            ,
            success: function(data) {
              vm.getTurns()
              $.notify('Turno deshabilitado con exito',{
                position:'top center',className:'success',arrowShow: false}
                      )
              //document.location.reload()
            }
            ,
            error: function(jqXHR) {
              console.log('error')
            }
            ,
            complete: function() {
              // _vm.loading = false
            }
            ,
          }
                )
        }
      ,
        activateTurn:function(id){
          let _vm = this
          vm.loading = true
          $.ajax({
            url: "{{route('turns.activate','_id')}}".replace('_id',id),
            method: 'put',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            ,
            success: function(data) {
              vm.getTurns()
              $.notify('Turno habilitado con exito',{
                position:'top center',className:'success',arrowShow: false}
                      )
              //document.location.reload()
            }
            ,
            error: function(jqXHR) {
              console.log('error')
            }
            ,
            complete: function() {
              // _vm.loading = false
            }
            ,
          }
                )
        }
      ,
        //-----------Tipo de Comensales-----------------------
        getComensalType:function(id){
          let _vm = this

          vm.type.loading = true

          $.ajax({
            url: "{{route('comensal-type.index')}}",
            method: 'get',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            ,
            success: function(data) {
              _vm.types = []
              data.data.forEach(function(type) {
                _vm.types.push({
                  label:type.name,
                  value:type.id,
                  deleted_at:type.deleted_at,
                  description:type.description
                }
                              )

              }
                               )
            }
            ,
            error: function(jqXHR) {
              console.log('error')
            }
            ,
            complete: function() {

              _vm.type.loading = false
            }
            ,
          }
                )
        }
      ,
        deleteType:function(id){
          let _vm = this
          //vm.loading = true
          vm.type.loading = true

          $.ajax({
            url: "{{route('comensal-type.delete','_id')}}".replace('_id',id),
            method: 'delete',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            ,
            success: function(data) {
              vm.getComensalType()
              $.notify('Tipo de comensal deshabilitado con exito',{
                position:'top center',className:'success',arrowShow: false}
                      )
              //document.location.reload()
            }
            ,
            error: function(jqXHR) {
              console.log('error')
            }
            ,
            complete: function() {
              // _vm.loading = false
              _vm.type.loading = false
            }
            ,
          }
                )
        }
      ,
        activateType:function(id){
          let _vm = this
          //vm.loading = true
          vm.type.loading = true
          $.ajax({
            url: "{{route('comensal-type.activate','_id')}}".replace('_id',id),
            method: 'put',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            ,
            success: function(data) {
              vm.getComensalType()
              $.notify('Tipo de comensal habilitado con exito',{
                position:'top center',className:'success',arrowShow: false}
                      )
              //document.location.reload()
            }
            ,
            error: function(jqXHR) {
              $.notify('Ups error al tratar de habilitar tipo de comensal',{
                position:'top center',className:'error',arrowShow: false}
                      )
              console.log('error')
            }
            ,
            complete: function() {
              // _vm.loading = false
              _vm.type.loading = false
            }
            ,
          }
                )
        }
      ,
        //-----------Carreras-----------------------
        getCareers: function(data) {
          let _vm = this
          $.ajax({
            url: "{{route('careers.index')}}",
            data: data,
            method: 'get',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
              'Authorization':'',
            }
            ,
            success: function(data) {
              data.forEach(function(career) {
                _vm.careers.push({
                  label:career.name,value:career.id}
                                )
              }
                          )
            }
            ,
            error: function(error) {
              console.log('error')
            }
            ,
            complete: function() {
              _vm.loading = false
            }
            ,
          }
                )
        }
      ,
        //-----------Utils-----------------------
        momentTime: function(time) {
          return moment(time).format('h:mm:ss A')
        }
    }
  ,
    components: {
      paginate: VuejsPaginate
    }
  }
  );
</script>
@endsection
@section('content')
<div class="content"  v-cloak>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="m-b-md" style="align-items: center; display: flex;justify-content: center;">
          <span style="color: #636b6f;  font-family: 'Raleway', sans-serif;font-weight: 100;font-size: 42px;">Control de acceso
          </span>
          <br>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      @if(auth()->user()->can('comensales.index'))
      <a href="#" data-toggle="modal" data-target="#showComensalModal">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-group" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Comensales
            </span>
          </center>
        </div>
      </a>
      @endif
      @if(auth()->user()->can('comensal-type.index'))
      <a href="#" data-toggle="modal" data-target="#showTypeModal">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-user" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase; font-size: 14px">Tipos de comensales
            </span>
          </center>
        </div>
      </a>
      @endif
      @if(auth()->user()->can('turns.index'))
      <a href="#" data-toggle="modal" data-target="#showTurnModal">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-cutlery" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Turnos
            </span>
          </center>
        </div>
      </a>
      @endif
      @if(auth()->user()->can('comensales.import'))
      <a href="#" data-toggle="modal" data-target="#importComensalModal">
        <div class="link-box">
          <center>
            <i style="font-size:30px;color:white;opacity: .86" class="fa fa-database" aria-hidden="true">
            </i>
          </center>
          <center>
            <span style="color: white; opacity: .86;font-weight: 600;text-transform:uppercase;">Cargar Data
            </span>
          </center>
        </div>
      </a>
      @endif
      @include('control-access.modals.comensal.index')
      @include('control-access.modals.comensal.create')
      @include('control-access.modals.comensal.edit')
      @include('control-access.modals.turn.index')
      @include('control-access.modals.turn.create')
      @include('control-access.modals.turn.edit')
      @include('control-access.modals.excel.index')
      @include('control-access.modals.types.index')
    </div>
    <!--END ROW-->
  </div>
  <!--END CONTAINER-FLUID-->
</div>
@endsection

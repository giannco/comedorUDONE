<div class="modal fade" id="createComensalModal" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a @click.prevent = "clearComensal()" href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body">
        <div>
          <span style="font-size:20px;"><b style="color: #87CB16;">Comensal</b> / Crear</span>
        </div>
        <br>
        @include('control-access.modals.comensal.partials.form')
      </div>
      <div >

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button @click.prevent="submitComensal()" type="button" class="btn btn-primary pull-right">Guardar</button>
      </div>
    </div>
  </div>
</div>

 <!-- VER COMENSALES MODAL -->
<div class="modal fade" id="showComensalModal" style="overflow-y: scroll!important" role="dialog">
    <div class="modal-dialog" style="max-width: 60%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <a href="#" class="close" data-dismiss="modal">&times;</a>
          <a href="#" class="btn btn-success" data-toggle="modal" data-target="#createComensalModal">Registrar comensal</a>
        </div>
        <div class="modal-body" v-if="!loadingComensal">
          <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Comensal</b></span>
          </div>
          <br>
          <div>
            <input type="text" name="findComensal" class="form-control" v-model="comensal.search" placeholder="Buscar comensal por cédula de identidad, nombre y/o apellido" v-on:keyup.enter="searchCOmensal()">
          </div>
          <br>
          <table class="table table-bordered" id="table">
            <thead>
              <tr>
                  <th> c.i </th>
                  <th> nombre </th>
                  <th> apellido </th>
                  <th> tipo </th>
                  <th> operación</th>
                </tr>
              </thead>
              <tbody v-if="comensales.length > 0" >
                <tr v-for="comensal in comensales">
                  <td style="font-size:12px;">@{{comensal.ic}}</td>
                  <td style="font-size:12px;text-transform:uppercase">@{{comensal.first_name}}</td>
                  <td style="font-size:12px;text-transform:uppercase">@{{comensal.last_name}}</td>
                  <td style="font-size:12px;text-transform:uppercase">@{{comensal.type}}</td>
                  <td><a @click.prevent = "showComensalModal(comensal)" href="#" data-toggle="tooltip" title="Editar"><i class="fa fa-pencil-square-o" style="font-size:25px;color: #008bff;"></i></a> <a v-if="comensal.deleted_at == null" @click.prevent = "deleteComensal(comensal.ic)" href="#" data-toggle="tooltip" title="Desactivar" style="margin-left: 25px;"><i  class="fa fa-check-square-o" style="font-size:25px;color:#87CB16"></i></a><a v-else @click.prevent = "activateComensal(comensal.ic)" href="#"  data-toggle="tooltip" title="Activar" style="margin-left: 25px;"><i  class="fa fa-square-o" style="font-size:25px;color:#87CB16"></i></a></td>
                </tr>
            </tbody>
          </table>
          <div v-if="pagination.last_page > 0">
              <paginate
                  :page-count="pagination.last_page"
                  :page-range="3"
                  :click-handler="setPage"
                  :prev-text="'ant'"
                  :next-text="'sig'"
                  :container-class="'pagination'"
                  :page-class = "'page-item'"
                  :page-link-class = "'page-link'"
                  :prev-class = "'page-item'"
                  :prev-link-class = "'page-link'"
                  :next-class = "'page-item'"
                  :next-link-class = "'page-link'"
                  :initial-page="pagination.current_page -1">
              </paginate>
            </div>
        </div>
        <div v-else class="loader">

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>

    </div>
</div>

 <!-- EDITAR COMENSAL MODAL -->

<form>
    <div class="row">
        <div class="col-md-6">
            <label>Tipo</label>
            <select required="required" v-model="comensal.type_id" name="type" class="form-control" style="text-transform:uppercase" >
                <option v-for="type in types" :value="type.value">@{{type.label.toUpperCase()}}</option>
            </select>
        </div>
        <div class="col-md-6">
            <label for="ic">Cédula</label>
            <input required="required" v-model="comensal.ic" type="text" name="ic" class="form-control" maxlength="15" onkeypress = "return ((event.keyCode == 9 || event.keyCode == 8 || (event.charCode >= 48 && event.charCode <= 57)))" >
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <label for="first_name">Nombre</label>
            <input required="required" v-model ="comensal.first_name" type="text" name="first_name" class="form-control"  maxlength="20" onkeypress ="return ((event.keyCode == 9 || event.keyCode == 8 || event.charCode == 32 || ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))))" style="text-transform:uppercase">
        </div>
        <div class="col-md-6">
            <label for="last_name">Apellido</label>
            <input required="required" v-model ="comensal.last_name" type="text" name="last_name" class="form-control" maxlength="20" onkeypress = "return ((event.keyCode == 9 || event.keyCode == 8 || event.charCode == 32 || ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))))" style="text-transform:uppercase">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <label for="sex">Sexo</label>
            <select required="required" v-model="comensal.sex" name="sex" class="form-control" style="text-transform:uppercase">
                <option value="m">Masculino</option>
                <option value="f">Femenino</option>
            </select>
        </div>
        <div class="col-md-6" v-if="comensal.type_id == 1">
            <label for="career">Carrera</label>
            <select id="career" class="form-control" v-model="career.id" required="required" style="text-transform:uppercase">
                <option v-for="career in careers" :value="career.value">@{{career.label}}</option>
            </select>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <label for="photo">Foto</label>
            <input v-model="comensal.photo" type="text" name="photo" class="form-control" disabled="disabled">
        </div>
    </div>
</form>

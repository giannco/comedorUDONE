<div class="modal fade" id="showTypeModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
    	<div class="modal-header" style="border-bottom:none">
        	<a href="#" class="close" data-dismiss="modal">&times;</a>

      	</div>
    	<div class="modal-body" v-if="!type.loading">
          	<div>
            	<span style="font-size:20px;"><b style="color: #87CB16;">Tipos de Comensales</b></span>
        	</div>
			<br>
	        <table class="table table-bordered" id="table">
	          <thead>
	            <tr>
	              <th> nombre </th>
	              <th> descripción </th>
	              <th> operación</th>
	            </tr>
	          </thead>
	          <tbody v-if="types.length > 0 ">
	            <tr v-for="type in types">
	              <td style="font-size:12px;text-transform:uppercase">@{{type.label}}</td>
	              <td style="font-size:12px;text-transform:uppercase">@{{type.description}}</td>
	              <td><a v-if="type.deleted_at == null" @click.prevent = "deleteType(type.value)" href="#" data-toggle="tooltip" title="Desactivar" style="margin-left: 25px;"><i  class="fa fa-check-square-o" style="font-size:25px;color:#87CB16"></i></a><a v-else @click.prevent = "activateType(type.value)" href="#"  data-toggle="tooltip" title="Activar" style="margin-left: 25px;"><i  class="fa fa-square-o" style="font-size:25px;color:#87CB16"></i></a></td>
	            </tr>
	          </tbody>
	        </table>
      	</div>
      	<div v-else class="loader">

      	</div>
      	<div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      	</div>
    </div>
  </div>
</div>

<div class="modal fade" id="importComensalModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body" v-if="!loading">
          <div>
            <span style="font-size:20px;"><b style="color: #87CB16;">Importar comensales</b></span>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
            <label>Archivo</label>
              <div class="custom-file">
                <label id="filename"  for="file" class="custom-file-label">Seleccionar archivo</label>
                <input type="file" style="visibility: hidden !important;" id="file" ref="files" name="file" accept=".xlsx" v-on:change="handleFileUpload($event)">
              </div>
            </div>
            <div class="col-md-6">
              <label for="types">Tipo de comensal</label>
              <select class="form-control" style="text-transform:uppercase"  id="type" name="type" v-model="select_type_id" required>
                <option v-for="type in types" :value="type.value">@{{type.label.toUpperCase()}}</option>
              </select>
            </div>
          </div>
          <br>

      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <input type="submit" name="enviar" value="Importar" class="btn btn-primary pull-right" v-on:click="submitFile()">
      </div>

    </div>
  </div>
</div>

<form action="#" id="formTurn">
   	<div class="row">
	   <div class="col-md-6">
			<label for="name">Nombre</label>
			<input required="required" v-model ="turn.name" type="text" name="name" class="form-control" maxlength="20" onkeypress ="return ((event.keyCode == 9 || event.keyCode == 8 || event.charCode == 32 || ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))))" style="text-transform:uppercase">
	   </div>
	   <div class="col-md-6">
			<label for="description">Descripción</label>
			<input v-model ="turn.description" type="text" name="description" class="form-control" maxlength="50" onkeypress ="return ((event.keyCode == 9 || event.keyCode == 8 || event.charCode == 32 || ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))))" style="text-transform:uppercase">
	   </div>
   	</div>
   	<br>
   	<div class="row">
	   <div class="col-md-6">
			<label for="start_hour">Hora inicio</label>
			<input required="required" v-model="turn.start_hour" type="time" name="start_hour" class="form-control">
	   </div>
	   <div class="col-md-6">
			<label for="end_hour">Hora fin</label>
			<input required="required" v-model="turn.end_hour" type="time" name="end_hour" class="form-control">
	   </div>
   	</div>
</form>

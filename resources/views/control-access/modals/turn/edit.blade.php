<div class="modal fade" id="editTurnModal" role="dialog">
  <div class="modal-dialog">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
      </div>
      <div class="modal-body" v-if="!loadingTurn">
         <div>
          <span style="font-size:20px;"><b style="color: #87CB16;">Turno</b> / Editar</span>
        </div>
        <br>
        @include('control-access.modals.turn.partials.form')

      </div>
       <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button @click.prevent="updateTurn(); loading=true" type="button" class="btn btn-primary pull-right">Guardar</button>
      </div>
    </div>
  </div>
</div>

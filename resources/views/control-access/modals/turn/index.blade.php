<div class="modal fade" id="showTurnModal" style="overflow-y: scroll!important" role="dialog">
  <div class="modal-dialog" style="max-width: 40%;">

      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:none">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
        <a href="#" @click.prevent="clearTurn()" class="btn btn-success" data-toggle="modal" data-target="#createTurnModal">Crear turno</a>
      </div>
      <div class="modal-body" v-if="!loadingTurn">
        <div>
          <span style="font-size:20px;"><b style="color: #87CB16;">Turno</b></span>
        </div>
        <br>
        <input type="text" name="findTurn" class="form-control" v-model="turn.search" placeholder="Buscar turno por nombre" v-on:keyup.enter="searchData(turn.search,getTurns,'loadingTurn')" style="font-size:12px;text-transform:uppercase">
        <br/>
        <table class="table table-bordered" id="table">
          <thead>
            <tr>
              <th> nombre </th>
              <th> hora inicio </th>
              <th> hora fin</th>
              <th> descripción </th>
              <th> operación</th>
            </tr>
          </thead>
          <tbody v-if="turns.length > 0" >
            <tr v-for="turn in turns">
              <td style="font-size:12px;text-transform:uppercase">@{{turn.name}}</td>
              <td style="font-size:12px;">@{{turn.start_hour}}</td>
              <td style="font-size:12px;">@{{turn.end_hour}}</td>
              <td style="font-size:12px;text-transform:uppercase">@{{turn.description}}</td>
              <td><a @click.prevent = "showDataModal(turn,'#editTurnModal','turn')" href="#" data-toggle="tooltip" title="Editar"><i class="fa fa-pencil-square-o" style="font-size:25px;color: #008bff"></i></a> <a v-if="turn.deleted_at == null" @click.prevent = "deleteTurn(turn.id)" href="#" data-toggle="tooltip" title="Desactivar" style="margin-left: 25px;"><i  class="fa fa-check-square-o" style="font-size:25px;color:#87CB16"></i></a><a v-else @click.prevent = "activateTurn(turn.id)" href="#"  data-toggle="tooltip" title="Activar" style="margin-left: 25px;"><i  class="fa fa-square-o" style="font-size:25px;color:#87CB16"></i></a></td>
            </tr>
          </tbody>
        </table>
        <div v-if="paginationTurn.last_page > 0">
          <paginate
                    :page-count="paginationTurn.last_page"
                    :page-range="3"
                    :click-handler="setPageTurn"
                    :prev-text="'ant'"
                    :next-text="'sig'"
                    :container-class="'pagination'"
                    :page-class = "'page-item'"
                    :page-link-class = "'page-link'"
                    :prev-class = "'page-item'"
                    :prev-link-class = "'page-link'"
                    :next-class = "'page-item'"
                    :next-link-class = "'page-link'"
                    :initial-page="paginationTurn.current_page - 1">
          </paginate>
        </div>
      </div>
      <div v-else class="loader">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Comedor Udone</title>

        <!-- Fonts -->


        <link rel="stylesheet" href="../css/font-awesome.min.css" />

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">{{Auth::user()->name}}</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                    @endif
                </div>
            @endif

            <div class="content">
                <div class="m-b-md"  style="border-radius: 50px;width:686px;height:423px;background: linear-gradient(to bottom, #132d48 0%, #1F77D0 100%);">

                    <img style="position: relative; max-width: auto; max-height: 250px; min-width: auto; min-height: auto; width: auto; height: auto; top: 50%!important; transform: translateY(-50%)!important; margin: auto!important"" src="../img/logo.png" alt="">
                </div>
                <div class="title m-b-md">
                    Comedor UDONE
                </div>

                <div class="links">
                    <a href="{{ route('control-access') }}"><i class="fa fa-id-card-o" aria-hidden="true"></i> Control de Acceso</a>
                    <a href="{{route('inventory')}}"><i class="fa fa-cubes" aria-hidden="true"></i> Inventario</a>
                    <a href="{{route('reports')}}"><i class="fa fa-print" aria-hidden="true"></i> Reportes</a>
                    <a href="{{route('configuration')}}"><i class="fa fa-gears" aria-hidden="true"> </i> Configuración</a>

                </div>
            </div>
        </div>
    </body>

</html>

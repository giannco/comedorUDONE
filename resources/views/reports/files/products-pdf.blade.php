<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Comensal</title>
        <style>
            body{
                text-transform: uppercase;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <P align="center">universidad de oriente</P>
            <P align="center">núcleo de nueva esparta</P>

            @if ($num == 4)
                <p align="center"> reporte de stock</p>
            @endif
            @if ($num == 5)
                <p align="center"> reporte de entradas</p>
            @endif
            @if ($num == 6)
                <p align="center"> reporte de salidas</p>
            @endif
            @if ($num == 7)
                <p align="center"> reporte de  desperdicios</p>
            @endif


            <p align="right"><span><b>fecha de emisión: </b>{{$dateNow}}</span></p>
            <br/>

            <p align="center">detallado por productos </p>

            <table style="width: 100%">
                <thead>
                   <tr align="center">
                        <th>nombre del producto</th>
                        <th>Cantidad total</th>
                   </tr>
                </thead>
                <tbody>
                @foreach($products['data'] as $product))
                    <tr align="center">
                       <td>{{$product['name']}}</td>
                       @if ($num == 4)
                            <td>{{$product['total_quantity']}}</td>
                        @else
                        <td>{{$product['total_quantity_sum']}}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
            <br/>
        </div>
    </body>
</html>

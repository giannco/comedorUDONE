<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Comensal</title>
        <style>
            body{
                text-transform: uppercase;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <P align="center">universidad de oriente</P>
            <P align="center">núcleo nueva esparta</P>
            <p align="center">reporte general</p>
            <p align="right"><span><b>fecha de emisión: </b>{{$dateNow}}</span></p>
            <br/>

            @foreach($arrivals['meta']['types'] as $type)

            <span><b>total de {{$type['name']}} de sexo femenino: </b></span> <span>{{$type['total_f']}}</span> <br /><br />
            <span><b>total de {{$type['name']}} de sexo masculino: </b></span> <span>{{$type['total_m']}}</span> <br /><br />
            <span><b>total de {{$type['name']}}: </b></span> <span>{{$type['total']}}</span> <br /><br />
            <hr/>
            @endforeach

            <span><b>total de comensales de sexo femenino: </b></span> <span>{{$arrivals['meta']['general']['total_f']}}</span><br /><br />
            <span><b>total de comensales de sexo masculino: </b></span> <span>{{$arrivals['meta']['general']['total_m']}}</span><br /><br />
            <span><b>total de comensales: </b></span><span>{{$arrivals['meta']['general']['total']}}</span>

        </div>
    </body>
</html>

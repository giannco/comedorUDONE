<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Comensal</title>
        <style>
            body{
                text-transform: uppercase;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <P align="center">universidad de oriente</P>
            <P align="center">núcleo de nueva esparta</P>
            <p align="center">reporte de estudiantes por carreras</p>
            <p align="right"><span><b>fecha de emisión: </b>{{$dateNow}}</span></p>
            <br/>

            @foreach($arrivals['meta']['careers'] as $career)
            @if($career['name'] !== NULL)
            <p align="center"><b>listado de {{$career['name']}}</b></p>
            <table style="width: 100%">
                <thead>
                   <tr align="center">
                        <th>cedula</th>
                        <th>nombre</th>
                        <th>apellido</th>
                        <th>turno</th>
                        <th>fecha y hora</th>
                   </tr>
                </thead>
                <tbody>
                @foreach($arrivals['data'] as $arrival)
                @if($arrival['career'] == $career['name'])

                    <tr align="center">
                       <td>{{$arrival['ic']}}</td>
                       <td>{{$arrival['first_name']}}</td>
                       <td>{{$arrival['last_name']}}</td>
                       <td>{{$arrival['turn_name']}}</td>
                       <td>{{$arrival['created_at']}}</td>
                    </tr>
                @endif
                @endforeach
                </tbody>
            </table>

            <br/>
            <br/>
            <div>
                <center><span>total de {{$career['name']}}: {{$career['total']}}</span></center>
            </div>
            @endif
            <br>
            <br>
            @endforeach
        </div>
    </body>
</html>

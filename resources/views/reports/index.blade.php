@extends('layouts.app')
@section('css')
<style>

  .titleGraph {
    color: #636b6f;
    font-family: 'Raleway' sans-serif;
    font-weight: 80;
    font-size: 30px;
  }

  .centertitleGraph {
    align-items: center;
    display: flex;
    justify-content: center;
  }

</style>
@endsection
@section('js')
<script>
     var vm =  new Vue({
            el: '.content',
            data: {
                report : {
                    'start_date': '',
                    'end_date' : '',
                    'type':'',
                    'turn_id':'',
                    'career_id':'',
                    'sex':'',
                    'career':'',
                    'type_report':''

                },
                errors : [],
                columnChart: [],
                checked: true,
                loading : true,
                graphName: '',
            },

            created: function () {

              this.graphName = 'Gráfica general'

            },
             mounted: function(){

              //Fechas por defectos
              this.report.start_date =  new Date().toISOString().slice(0,10)
              this.report.end_date =  new Date().toISOString().slice(0,10)
              this.report.type_report = 0
            },
            methods: {

                submit: function() {

                  let _vm = this

                  vm.loading = true

                  let url = "{{route('reports.pdf')}}"

                  const params = {
                    type: vm.report.type,
                    start_date : vm.report.start_date,
                    end_date : vm.report.end_date,
                    turn_id : vm.report.turn_id,
                    type_report : vm.report.type_report,
                    sex : vm.report.sex,
                    career : vm.report.career,

                  }

                  axios({
                    method: 'post',
                    url: url ,
                    responseType: 'blob',
                    data:params
                  })
                  .then(function (response) {

                     const url = window.URL.createObjectURL(new Blob([response.data]));
                     const link = document.createElement('a');
                     link.href = url;
                     link.setAttribute('download', 'file.pdf'); //or any other extension
                     document.body.appendChild(link);
                     link.click();

                  })
                  .catch(function (error) {
                    //error
                    console.log('error',error)

                  })
                  .then(function () {
                    //completado
                    _vm.loading = false

                    //_vm.columnChart = []
                    //_vm.getDataForGraph()

                  })
                },

                getDataForGraph: function() {

                  let _vm = this
                  let url = "{{route('reports.pdf')}}"

                  const params = {
                    type: vm.report.type,
                    start_date : vm.report.start_date,
                    end_date : vm.report.end_date,
                    turn_id : vm.report.turn_id,
                    type_report : vm.report.type_report,
                    sex : vm.report.sex,
                    career : vm.report.career,
                    option:2,

                  }

                  axios({
                    method: 'post',
                    url: url ,
                    responseType: 'json',
                    data:params,
                  })
                  .then(function (response) {

                      if (vm.report.type_report == '0'){

                          let general = response.data.meta.general
                          vm.graphName = 'Gráfica general'

                          vm.columnChart.push({name:'General',data:{'Total':general.total,'Sexo Masculino':general.total_m,'Sexo Femenino':general.total_f}})
                      }

                      if (vm.report.type_report == '1'){


                        let careers = response.data.meta.careers

                        vm.graphName = 'Gráfica por carreras'

                        for (const index in careers) {

                          var name = careers[index]['name']
                          var total = careers[index]['total']
                          var total_m = careers[index]['total_m']
                          var total_f = careers[index]['total_f']

                          if (name != null){
                            vm.columnChart.push({name:name,data:{'Total':total,'Sexo Masculino':total_m,'Sexo Femenino':total_f}})
                          }

                        }
                        /*careers.forEach(function(career){
                          if (career.name != null){
                              vm.columnChart.push({name:career.name,data:{'Total':career.total,'Sexo Masculino':career.total_m,'Sexo Femenino':career.total_f}})
                          }
                        })*/
                      }

                      if (vm.report.type_report == '2'){

                        let types = response.data.meta.types
                        vm.graphName = 'Gráfica por tipo de comensales'

                        for (const index in types) {

                          var name = types[index]['name']
                          var total = types[index]['total']
                          var total_m = types[index]['total_m']
                          var total_f = types[index]['total_f']

                          vm.columnChart.push({name:name,data:{'Total':total,'Sexo Masculino':total_m,'Sexo Femenino':total_f}})
                        }

                        /*types.forEach(function(type){
                          vm.columnChart.push({name:type.name,data:{'Total':type.total,'Sexo Masculino':type.total_m,'Sexo Femenino':type.total_f}})
                        })*/
                      }

                      if (vm.report.type_report == '3'){

                        let turns = response.data.meta.turns
                        vm.graphName = 'Gráfica por turnos'

                        for (const index in turns) {

                          var name = turns[index]['name']
                          var total = turns[index]['total']
                          var total_m = turns[index]['total_m']
                          var total_f = turns[index]['total_f']

                          vm.columnChart.push({name:name,data:{'Total':total,'Sexo Masculino':total_m,'Sexo Femenino':total_f}})
                        }

                        /*turns.forEach(function(turn){
                          vm.columnChart.push({name:turn.name,data:{'Total':turn.total,'Sexo Masculino':turn.total_m,'Sexo Femenino':turn.total_f}})
                        })*/
                      }

                      if (vm.report.type_report == '4'){

                        let products = response.data
                        vm.graphName = 'Gráfica por productos'

                        products.data.forEach(function(product){
                          vm.columnChart.push({name:product.name,data:{'Stock':product.total_quantity}})
                        })
                      }

                      if (vm.report.type_report == '5' || vm.report.type_report == '6' || vm.report.type_report == '7'){

                        let products = response.data
                        vm.graphName = vm.report.type_report == '5' ? 'Gráfica por entradas' : vm.report.type_report == '6' ? 'Gráfica por salidas' : 'Gráfica por desperdicios'

                        products.data.forEach(function(product){
                          vm.columnChart.push({name:product.name,data:{'Total':product.total_quantity_sum}})
                        })
                      }


                  })
                  .catch(function (error) {
                    //error
                      console.log('error',error)
                      let msg = ''
                      //Muestra los msj de error dependiendo de la propiedad obtenida

                      msg = Object.values(error.response.data.errors)[0][0]

                      $.notify(msg,{
                        position:'top center',className:'error',arrowShow: false})
                      })
                  .then(function () {
                    //completado
                     _vm.loading = false


                  })
                },
              momentTime: function(time) {
                  return moment(time).format('h:mm:ss A')
              },
              changeGraphBySelect:function(){

                let _vm = this

                _vm.columnChart = []
                _vm.getDataForGraph()
              }
            },

        });
</script>
@endsection

@section('content')
<div class="content" v-cloak>
  <div class="row">
    <div class="col-md-12">
        <div class="m-b-md" style="align-items: center; display: flex;justify-content: center;">
            <span style="color: #636b6f;  font-family: 'Raleway', sans-serif;font-weight: 100;font-size: 42px;">Reporte de comensales</span><br>
        </div>
    </div>
  </div>
  <br/>
  <div class="row">

    <div class="col-md-3">
      <label for="start_date">Fecha de inicio</label>
      <input v-model="report.start_date" type="date" name="start_date" id="start_date" class="form-control" @change="changeGraphBySelect()">
    </div>

    <div class="col-md-3">
      <label for="end_date">Fecha de culminación</label>
      <input v-model="report.end_date" type="date" name="end_date" id="end_date" class="form-control" @change="changeGraphBySelect()">
    </div>

    <div class="col-md-4">
      <label for="type_report" >Tipo de reporte</label>
      <select id="type_report" class="form-control"  v-model="report.type_report" required="required" @change="changeGraphBySelect()">
        <option value="0">General</option>
        <option value="1">Detallado por carrera</option>
        <option value="2">Detallado por tipo de comensal</option>
        <option value="3">Detallado por turno</option>
        <option value="4">Detallado por producto</option>
        <option value="5">Detallado por entrada</option>
        <option value="6">Detallado por salida</option>
        <option value="7">Detallado por desperdicio</option>
      </select>
    </div>

    <div class="col-md-2">
      <button @click.prevent="submit()" type="button" class="btn btn-primary" style="margin-top: 29px;">Generar pdf</button>
    </div>
  </div>

  <br/>

  <div class="row">
    <div class="col-md-12" v-if="checked">
      <div class="m-b-md centertitleGraph">
        <span class="titleGraph centertitleGraph" >@{{graphName}}</span>
      </div>
      <column-chart :data="columnChart" :download="true" legend="bottom" ></column-chart>
    </div>
  </div>

  <br/>

</div>

@endsection

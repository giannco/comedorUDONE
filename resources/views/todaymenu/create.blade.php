@extends('layouts.app')
@section('css')
<style>
  [v-cloak] {display: none}

</style>
@endsection
@section('js')
<script>

</script>
<script>
     var vm =  new Vue({
            el: '.content',
            data: {
                menu : {
                    'description':'',
                    'photo':'',
                    'turn_id' : '',
                },
                turns : [],
                turn : '',
                errors : [],
                loading : true
            },

            created: function () {
                  setTimeout(function() {
                        vm.turnsearch()
                  },1000)

            },
            methods: {

                submit: function() {

                  let fake = {

                    description : vm.menu.description,
                    turn_id : vm.menu.turn_id,
                  }
                    this.errors = []
                    let _vm = this
                        $.ajax({
                            url: "{{route('today-menu.store')}}",
                            data: JSON.stringify(fake),
                            method: 'post',
                            dataType: 'json',
                            contentType: 'application/json',
                            headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },

                            success: function(data) {

                             document.location.reload()

                            },
                            error: function(error) {
                                console.log(error)
                                console.log('error')
                            },
                            complete: function() {
                               // _vm.processing = false
                            },
                        })

                },
                turnsearch: function(data) {
                    let _vm = this

                    $.ajax({
                        url: "{{route('turns.index',['query_search' => 'all'])}}",
                        data: data,
                        method: 'get',
                        dataType: 'json',
                        contentType: 'application/json',
                        headers: {
                            'Authorization':'',
                        },
                        success: function(data) {

                          data.forEach(function(turn) {
                            _vm.turns.push({label:turn.name,value:turn.id})

                          })

                        },
                        error: function(error) {
                            console.log(error)
                            console.log('error')
                        },
                        complete: function() {
                            _vm.loading = false
                        },
                    })
                },

                momentTime: function(time) {
                    return moment(time).format('h:mm:ss A')
                },
            },

        });
</script>
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">

        <h3>Crear menu del dia</h3>
        <label>Turno del menu</label>
        <select required="required" v-model="menu.turn_id" name="type" class="form-control" >
            <option v-for="turn in turns" v-bind:value="turn.value">@{{turn.label}}</option>
        </select>
        <label for="photo">Imagen del menu</label>
        <input v-model="menu.photo" type="text" name="photo" class="form-control" disabled="disabled">
        <label for="description">Descripcion del menu</label>
        <textarea required="required" v-model="menu.description"  name="description" class="form-control"  maxlength="255" placeholder="Arroz con pollo ejemplo" onkeypress ="return ((event.keyCode == 8 || event.charCode == 32 || ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))))" style="min-height: 100px;"></textarea>
        <br>
        <button @click.prevent="submit()" type="button" class="btn btn-primary pull-right">Guardar</button>

    </div>
</div>
@endsection
